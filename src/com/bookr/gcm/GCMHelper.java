package com.bookr.gcm;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.bookr.App;
import com.bookr.utils.Constants;
import com.bookr.utils.SalonEmployee;
import com.bookr.utils.User;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

@SuppressWarnings("deprecation")
public class GCMHelper {

	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	private static final String KEY_GCM_PREFS = "GCM_PREFS";
	private static final String KEY_REGID = "KEY_REGID";
	private static final String KEY_USERID = "KEY_USERID";
	private static final String KEY_APP_VERSION = "KEY_APP_VERSION";
	private static final String TAG = "GCMHelper";

	@SuppressWarnings("unused")
	private static boolean isPlayServicesAvailable(final Context context) {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(context);
		return resultCode == ConnectionResult.SUCCESS;
	}

	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	private static int getAppVersion() {
		try {
			PackageInfo packageInfo = App.getContext().getPackageManager()
					.getPackageInfo(App.getContext().getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	private static void storeRegistrationId(final String regid) {
		SharedPreferences.Editor editor = App.getContext()
				.getSharedPreferences(KEY_GCM_PREFS, 0).edit();
		editor.putString(KEY_REGID, regid);
		editor.putInt(KEY_APP_VERSION, getAppVersion());
		if (User.getInstance() != null) {
			editor.putString(KEY_USERID, User.getInstance().getUserID());
		} else {
			editor.putString(KEY_USERID, SalonEmployee.getInstance().getEmpId());
		}
		editor.commit();
	}

	public static String getSavedRegistrationId() {
		return App.getContext().getSharedPreferences(KEY_GCM_PREFS, 0)
				.getString(KEY_REGID, "");
	}

	private static String getRegistrationId() {
		final SharedPreferences prefs = App.getContext().getSharedPreferences(
				KEY_GCM_PREFS, 0);
		final String regid = prefs.getString(KEY_REGID, "");

		if (regid.isEmpty()) {
			return "";
		}
		// Check if App was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// App version.
		int registeredVersion = prefs
				.getInt(KEY_APP_VERSION, Integer.MIN_VALUE);
		int currentVersion = getAppVersion();
		if (registeredVersion != currentVersion) {
			unregister();
			return "";
		}
		return regid;
	}

	/**
	 * Handle registrations. If already registered, returns.
	 */
	public static void registerIfNotAlreadyDone(final Activity context) {
		if (!checkPlayServices(context)) {
			return;
		}

		final String regid = getRegistrationId();
		Log.v("regid == ", regid);
		sendRegistrationIdToBackend(regid);
		if (regid.isEmpty()) {
			generateRegid();
		}
	}

	@SuppressWarnings("unused")
	private static void registerForGCM() {
		try {
			GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(App
					.getContext());
			final String regid = gcm.register(GCMConfig.SENDER_ID);
			Log.e("regid", regid);
			if (sendRegistrationIdToBackend(regid)) {
				// Persist the regID - no need to register again.
				storeRegistrationId(regid);
			} else {
				Log.e("GCMHelper", "Not registered on server!");
			}
		} catch (IOException ex) {
			// If there is an error, don't just keep trying to register.
			// Require the user to click a button again, or perform
			// exponential back-off.
		}
	}

	private static void generateRegid() {
		Log.d("GCMHelper", "getting devID....");
		new AsyncTask<Void, Void, String>() {

			@Override
			protected String doInBackground(Void... params) {
				try {
					GoogleCloudMessaging gcm = GoogleCloudMessaging
							.getInstance(App.getInstance());
					return gcm.register(GCMConfig.SENDER_ID);
				} catch (IOException e) {
					return null;
				}
			}

			protected void onPostExecute(String result) {
				if (result != null) {
					Log.e("regid is", result);
					sendRegistrationIdToBackend(result);
				} else {
					// try again
				}
			};

		}.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	private static boolean sendRegistrationIdToBackend(final String regid) {
		Log.v(TAG, "Registering on server");
		new AsyncTask<Void, Void, JSONObject>() {
			@Override
			protected JSONObject doInBackground(Void... params) {
				try {
					HttpParams param = new BasicHttpParams();
					HttpConnectionParams.setConnectionTimeout(param,
							Constants.TIMEOUT);
					HttpConnectionParams.setSoTimeout(param, Constants.TIMEOUT);
					DefaultHttpClient client = new DefaultHttpClient(param);
					HttpPost post = new HttpPost(Constants.REGISTER_DEVICE);
					List<NameValuePair> values = new ArrayList<NameValuePair>();
					if (User.getInstance() != null) {
						values.add(new BasicNameValuePair("user_id", User
								.getInstance().getUserID()));
						values.add(new BasicNameValuePair("member_type",
								"visitor"));
					} else {
						values.add(new BasicNameValuePair("user_id",
								SalonEmployee.getInstance().getEmpId()));
						values.add(new BasicNameValuePair("member_type",
								"employee"));
					}
					values.add(new BasicNameValuePair("udid", regid));
					values.add(new BasicNameValuePair("type", "android"));
					// TODO
					Log.v("values are ", values.toString());
					post.setEntity(new UrlEncodedFormEntity(values));

					String reply = EntityUtils.toString(client.execute(post)
							.getEntity());
					Log.v("reply", reply);
					JSONObject ob = new JSONObject(reply);
					return ob;
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected void onPostExecute(JSONObject result) {
				super.onPostExecute(result);
				try {
					if (result.getInt("responseCode") == 1) {
						storeRegistrationId(regid);
					} else {
						Log.e(TAG, "not stored on server");
						// TODO retry registration
					}
				} catch (Exception e) {
					e.printStackTrace();
					Log.e(TAG, "not stored on server");
				}
			}

		}.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		return false;
	}

	public static void unregister() {
		Log.v(TAG, "Unregistering regid");
		SharedPreferences prefs = App.getContext().getSharedPreferences(
				KEY_GCM_PREFS, 0);
		final String regid = prefs.getString(KEY_REGID, "");
		final String userid = prefs.getString(KEY_USERID, "");
		if (userid.isEmpty() || regid.isEmpty()) {
			prefs.edit().clear().commit();
			return;
		}
		new AsyncTask<Void, Void, JSONObject>() {
			@Override
			protected JSONObject doInBackground(Void... params) {
				try {
					HttpParams param = new BasicHttpParams();
					HttpConnectionParams.setConnectionTimeout(param,
							Constants.TIMEOUT);
					HttpConnectionParams.setSoTimeout(param, Constants.TIMEOUT);
					DefaultHttpClient client = new DefaultHttpClient(param);
					HttpPost post = new HttpPost(Constants.UNREGISTER_DEVICE);
					List<NameValuePair> values = new ArrayList<NameValuePair>();
					values.add(new BasicNameValuePair("udid", regid));
					// values.add(new BasicNameValuePair("user_id", userid));
					// values.add(new BasicNameValuePair("type", "android"));
					Log.v("values are UNREG==", values.toString());
					post.setEntity(new UrlEncodedFormEntity(values));
					String reply = EntityUtils.toString(client.execute(post)
							.getEntity());
					Log.v("reply", reply);
					JSONObject ob = new JSONObject(reply);
					return ob;
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected void onPostExecute(JSONObject result) {
				super.onPostExecute(result);
				try {
					if (result.getInt("responseCode") == 1) {
						App.getContext().getSharedPreferences(KEY_GCM_PREFS, 0)
								.edit().remove(KEY_REGID).remove(KEY_USERID)
								.commit();
						Log.v("Tag", "===" + result.getInt("responseCode"));
						Log.v(TAG, "Unregister success!");
					} else {
						Log.v(TAG, "Unregister Failed!");
						// TODO retry un-registration
					}
				} catch (Exception e) {
					e.printStackTrace();
					Log.e(TAG, "not stored on server");
				}
			}

		}.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	public static boolean checkPlayServices(Activity ctx) {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(ctx);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, ctx,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Log.e("GCMHelper", "This device is not supported.");
				Toast.makeText(ctx.getApplicationContext(),
						"This device is not supported.", Toast.LENGTH_LONG)
						.show();
				ctx.finish();
			}
			return false;
		}
		return true;
	}

}
