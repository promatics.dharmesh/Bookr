package com.bookr.gcm;

import java.util.Random;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.bookr.Dashboard;
import com.bookr.MyBookingActivity;
import com.bookr.MyOffersActivity;
import com.bookr.MyReviewsActivity;
import com.bookr.PromotedSalons;
import com.bookr.R;
import com.bookr.details.BookFrag;
import com.bookr.details.SaloonDetailActivity;
import com.bookr.employees.EmployeeTabs;
import com.bookr.employees.Receptionist;
import com.bookr.utils.Constants;
import com.bookr.utils.SalonEmployee;
import com.bookr.utils.User;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class GCMIntentService extends IntentService {

	private static final String TAG = GCMIntentService.class.getSimpleName();

	public GCMIntentService() {
		super("GCMIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		Log.d(TAG, "GCM Intent Received");
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		// The getMessageType() intent parameter must be the intent you received
		// in your BroadcastReceiver.
		String messageType = gcm.getMessageType(intent);
		if (extras != null && !extras.isEmpty()) {
			Log.v(TAG, extras.toString());
			try {
				if (messageType
						.equals(GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE)) {
					int type = 0;
					try {
						type = Integer.valueOf(extras.getString("type"));
					} catch (NumberFormatException e) {
						e.printStackTrace();
						type = 0;
					}
					switch (type) {
					case 1:
					case 2:
					case 3:
					case 4:
					case 5:
						if (User.getInstance() != null) {
							// show notification only if the user is logged in
							sendUserNotification(type, extras);
						}
						break;
					case 6:
					case 7:
						if (SalonEmployee.getInstance() != null
								&& SalonEmployee.getInstance()
										.getReceptionist().equals("0")) {
							// show notification only if the Employee is logged
							// in
							sendEmpNotificaton(type, extras);
							Log.v("Salon Employee", "Receptionaist");
						} else {
							// Unregister Device for this user...
							GCMHelper.unregister();
						}
						break;
					case 9:
					case 10:
						if (SalonEmployee.getInstance() != null
								&& SalonEmployee.getInstance()
										.getReceptionist().equals("1")) {
							sendReceptionistNoti(type, extras);
							Log.v("Receptionaist", "Receptionaist");
						} else {
							// Unregister Device for this user...
							GCMHelper.unregister();
						}
					default:
						break;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		GCMReceiver.completeWakefulIntent(intent);
	}

	private void sendReceptionistNoti(int type, Bundle extras) {
		String msg = extras.getString("message");
		String title = extras.getString("title");
		Intent intent = new Intent();
		intent.putExtras(extras);
		switch (type) {
		case 9:
			intent.setClass(this, Receptionist.class);
			intent.setAction(Constants.ACTION_NEW_BOOKING_RECETIONIST);
			break;
		case 10:
			intent.setClass(this, Receptionist.class);
			intent.setAction(Constants.ACTION_CANCEL_BOOKING_RECEPTIONIST);
			break;
		}
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_CLEAR_TASK
				| Intent.FLAG_ACTIVITY_NEW_TASK);
		PendingIntent pi = PendingIntent.getActivity(this,
				new Random().nextInt(), intent, PendingIntent.FLAG_ONE_SHOT);
		NotificationCompat.Builder builder = new NotificationCompat.Builder(
				this)
				.setSmallIcon(R.mipmap.icon)
				.setContentTitle(title)
				.setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
				.setContentText(msg)
				.setTicker(msg)
				.setSound(
						RingtoneManager
								.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
				.setAutoCancel(true);

		builder.setContentIntent(pi);
		NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		nm.notify(new Random().nextInt(), builder.build());

	}

	private void sendEmpNotificaton(int type, Bundle extras) {
		String msg = extras.getString("message");
		String title = extras.getString("title");
		Intent intent = new Intent();
		intent.putExtras(extras);
		switch (type) {
		case 6:
			intent.setClass(this, EmployeeTabs.class);
			intent.setAction(Constants.ACTION_NEW_BOOKING_EMPLOYEE);
			break;
		case 7:
			intent.setClass(this, EmployeeTabs.class);
			intent.setAction(Constants.ACTION_CANCEL_BOOKING_EMPLOYEE);
			break;
		}
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_CLEAR_TASK
				| Intent.FLAG_ACTIVITY_NEW_TASK);
		PendingIntent pi = PendingIntent.getActivity(this,
				new Random().nextInt(), intent, PendingIntent.FLAG_ONE_SHOT);
		NotificationCompat.Builder builder = new NotificationCompat.Builder(
				this)
				.setSmallIcon(R.mipmap.icon)
				.setContentTitle(title)
				.setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
				.setContentText(msg)
				.setTicker(msg)
				.setSound(
						RingtoneManager
								.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
				.setAutoCancel(true);

		builder.setContentIntent(pi);
		NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		nm.notify(new Random().nextInt(), builder.build());
	}

	private void sendUserNotification(int type, Bundle extras) {
		String msg = extras.getString("message");
		String title = extras.getString("title");
		Intent intent = new Intent();
		intent.putExtras(extras);
		switch (type) {
		case 1:
			intent.setClass(this, MyReviewsActivity.class);
			intent.setAction(Constants.ACTION_CONFIRMED);
			break;
		case 2:
			intent.setClass(this, MyBookingActivity.class);
			intent.setAction(Constants.ACTION_CANCELLED);
			break;
		case 3:
			String SalonId = extras.getString("saloon_id");
			BookFrag.searchFlag = true;
			intent.setClass(this, SaloonDetailActivity.class).putExtra(
					"salonId", SalonId);
			intent.setAction(Constants.ACTION_SALON_OFFER);
			break;
		case 4:
			intent.setClass(this, MyOffersActivity.class);
			intent.setAction(Constants.ACTION_ADMIN_OFFERS);
			break;
		case 5:
			intent.setClass(this, PromotedSalons.class);
			intent.setAction(Constants.ACTION_ADMIN_PROMOTED_SALON);
			break;
		default:
			intent.setClass(this, Dashboard.class);
			break;
		}

		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_CLEAR_TASK
				| Intent.FLAG_ACTIVITY_NEW_TASK);
		PendingIntent pi = PendingIntent.getActivity(this,
				new Random().nextInt(), intent, PendingIntent.FLAG_ONE_SHOT);
		NotificationCompat.Builder builder = new NotificationCompat.Builder(
				this)
				.setSmallIcon(R.mipmap.icon)
				.setContentTitle(title)
				.setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
				.setContentText(msg)
				.setTicker(msg)
				.setSound(
						RingtoneManager
								.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
				.setAutoCancel(true);

		builder.setContentIntent(pi);
		NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		nm.notify(new Random().nextInt(), builder.build());
	}

}
