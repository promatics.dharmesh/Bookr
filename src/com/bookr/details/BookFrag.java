package com.bookr.details;

import static com.bookr.utils.DataHolder.categories;
import static com.bookr.utils.DataHolder.categorySelected;
import static com.bookr.utils.DataHolder.selectedSaloon;
import static com.bookr.utils.DataHolder.services;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.bookr.R;
import com.bookr.adapters.CustomSpinnerAdapter;
import com.bookr.adapters.ServiceSlotAdapter;
import com.bookr.models.Category;
import com.bookr.models.Employee;
import com.bookr.models.Service;
import com.bookr.models.TimeSlot;
import com.bookr.utils.CallService;
import com.bookr.utils.CommonUtils;
import com.bookr.utils.Constants;
import com.bookr.utils.DataHolder;
import com.bookr.utils.DynamicHeight;
import com.bookr.utils.ServiceCallBack;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;

@SuppressWarnings("deprecation")
public class BookFrag extends Fragment implements OnClickListener,
		ServiceCallBack, OnItemClickListener {

	public static boolean searchFlag = false;
	AQuery aq;
	private SliderLayout imgSlider;
	private ListView listView;
	private TextView txtServiceName, txtDate, txtTime;
	private int Hour, Minute, Year, Month, Day;
	private boolean isTimeset = false, isDateset = false;
	private Employee[] arr;
	ArrayList<String> catNameList = new ArrayList<String>();
	ArrayList<String> svcNameList = new ArrayList<String>();
	Dialog listDialog, dialog;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (arr == null || arr.length == 0) {
			arr = DataHolder.selectedSaloon.getEmployees();
		}
	}

	@SuppressLint("SimpleDateFormat")
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		aq = new AQuery(getActivity());
		View v = inflater.inflate(R.layout.booking, container, false);
		listView = (ListView) v.findViewById(R.id.listAvailableTime);
		imgSlider = (SliderLayout) v.findViewById(R.id.slider);
		if (selectedSaloon != null && selectedSaloon.getImgs() != null)
			for (String Image : DataHolder.selectedSaloon.getImgs()) {
				DefaultSliderView sv = new DefaultSliderView(getActivity());
				sv.image(Image);
				imgSlider
						.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
				imgSlider.addSlider(sv);
			}
		imgSlider.setDuration(5000);
		imgSlider.getPagerIndicator().setVisibility(View.GONE);
		txtServiceName = (TextView) v.findViewById(R.id.txtServiceName);
		txtDate = (TextView) v.findViewById(R.id.txtDate);
		txtTime = (TextView) v.findViewById(R.id.txtTime);
		Calendar ca = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		txtDate.setText(format.format(ca.getTime()));
		isDateset = true;
		SimpleDateFormat format1 = new SimpleDateFormat("hh:mm a");
		txtTime.setText(format1.format(ca.getTime()));
		isTimeset = true;
		if (searchFlag == true) {
			txtServiceName.setText("Select Category");
		} else
			txtServiceName.setText(DataHolder.categorySelected.getName());
		txtDate.setOnClickListener(this);
		txtTime.setOnClickListener(this);
		txtServiceName.setOnClickListener(this);
		return v;
	}

	@Override
	public void onResume() {
		super.onResume();
		callService();
	}

	@SuppressLint("SimpleDateFormat")
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.txtDate:
			final Calendar c = Calendar.getInstance();
			Year = c.get(Calendar.YEAR);
			Month = c.get(Calendar.MONTH);
			Day = c.get(Calendar.DAY_OF_MONTH);
			DatePickerDialog dpd = new DatePickerDialog(getActivity(),
					new OnDateSetListener() {
						@Override
						public void onDateSet(DatePicker arg0, int Year,
								int Month, int Day) {
							txtDate.setText(Day + "-" + (Month + 1) + "-"
									+ Year);
							String date = txtDate.getText().toString();
							try {
								SimpleDateFormat sdf = new SimpleDateFormat(
										"dd-MM-yyyy");
								Date dateObj = sdf.parse(date);
								txtDate.setText(new SimpleDateFormat(
										"dd-MM-yyyy").format(dateObj));
							} catch (final Exception e) {
								e.printStackTrace();
							}
							isDateset = true;
							callService();
						}
					}, Year, Month, Day);
			long t = System.currentTimeMillis();
			dpd.getDatePicker().setMinDate(t - 1000);
			dpd.show();
			break;
		case R.id.txtTime:
			final Calendar c1 = Calendar.getInstance();
			Hour = c1.get(Calendar.HOUR_OF_DAY);
			Minute = c1.get(Calendar.MINUTE);
			TimePickerDialog tpd = new TimePickerDialog(getActivity(),
					new OnTimeSetListener() {
						@Override
						public void onTimeSet(TimePicker view, int hourOfDay,
								int minute) {
							txtTime.setText(hourOfDay + ":" + minute);
							String time = txtTime.getText().toString();
							Log.v("24Hr", time);
							try {
								SimpleDateFormat sdf = new SimpleDateFormat(
										"H:mm");
								Date dateObj = sdf.parse(time);
								txtTime.setText(new SimpleDateFormat("hh:mm a")
										.format(dateObj));
							} catch (final Exception e) {
								e.printStackTrace();
							}
							isTimeset = true;
							callService();
						}
					}, Hour, Minute, false);
			tpd.show();
			break;
		case R.id.txtServiceName:
			new CallService(this, getActivity(), Constants.REQ_GET_CAT, true,
					null).execute(Constants.GET_CATEGORIES);
			break;
		}
	}

	private void callService() {
		if (isDateset && isTimeset) {
			List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
			values.add(new BasicNameValuePair("saloon_id",
					DataHolder.selectedSaloon.getId()));
			if (searchFlag == false) {
				values.add(new BasicNameValuePair("service_name_id",
						DataHolder.selectedService.getId()));
			}
			values.add(new BasicNameValuePair("date", txtDate.getText()
					.toString()));
			values.add(new BasicNameValuePair("time", txtTime.getText()
					.toString()));
			Log.i("values are", values.toString());
			new CallService(this, getActivity(),
					Constants.REQ_TODAY_AVAILABLE_BY_EMP, true, values)
					.execute(Constants.TODAY_AVAILABLE_BY_EMP);
		}
	}

	@Override
	public void onServiceResponse(int requestCode, JSONObject data) {
		switch (requestCode) {
		case Constants.REQ_TODAY_AVAILABLE_BY_EMP:
			try {
				int code = data.getInt("responseCode");
				if (code == 1) {
					DataHolder.newdate = txtDate.getText().toString();
					JSONArray services = data.getJSONObject("Data")
							.getJSONArray("Employee");
					arr = new Employee[services.length()];
					for (int i = 0; i < services.length(); i++) {
						JSONObject detailsvc = services.getJSONObject(i);
						Employee temp = new Employee();
						temp.setEmpId(detailsvc.getString("id"));
						temp.setEmpName(detailsvc.getString("name"));
						temp.setEmpPhone(detailsvc.getString("phone_no"));
						temp.setEmpStartTime(detailsvc.getString("starttime"));
						temp.setEmpEndTime(detailsvc.getString("endtime"));
						JSONArray slotsarr = services.getJSONObject(i)
								.getJSONArray("Schedule");
						TimeSlot[] slots = new TimeSlot[slotsarr.length()];
						for (int j = 0; j < slotsarr.length(); j++) {
							JSONObject obj = slotsarr.getJSONObject(j);
							TimeSlot slot = new TimeSlot();
							slot.setSlotID(obj.getString("id"));
							slot.setStartTime(obj.getString("starttime"));
							slot.setEndTime(obj.getString("endtime"));
							// slot.setServiceID(obj.getString("service_id"));
							// slot.setServiceName(obj.getString("service_name"));
							// slot.setEmpName(obj.getString("emp_name"));
							slots[j] = slot;
						}
						temp.setSlots(slots);
						arr[i] = temp;
						DataHolder.selectedDate = txtDate.getText().toString();
					}
					listView.setAdapter(new ServiceSlotAdapter(getActivity(),
							arr));
					DynamicHeight.setListViewHeightBasedOnChildren(listView);
				}
			} catch (Exception e) {
				e.printStackTrace();
				Toast.makeText(getActivity(), "error Occured",
						Toast.LENGTH_LONG).show();
			}
			break;
		case Constants.REQ_GET_CAT:
			try {
				int code = data.getInt("responseCode");
				if (code == 1) {
					JSONArray arr = data.getJSONArray("Data");
					DataHolder.categories.clear();
					catNameList.clear();
					for (int i = 0; i < arr.length(); i++) {
						JSONObject servicecat = arr.getJSONObject(i)
								.getJSONObject("ServiceCategory");
						Category appsvc = new Category();
						appsvc.setId(servicecat.getString("id"));
						appsvc.setName(servicecat.getString("name"));
						catNameList.add(appsvc.getName());
						DataHolder.categories.add(appsvc);
					}
					Log.v("IMG", "total imgs:" + categories.size());
					showCatDialog();
				} else {
					CommonUtils.showDialog(getActivity(), "error");
				}
			} catch (Exception e) {
				e.printStackTrace();
				CommonUtils.showDialog(getActivity(),
						"Invalid response from server");
			}
			break;
		case Constants.REQ_GET_SERVICES:
			try {
				int code = data.getInt("responseCode");
				if (code == 1) {
					JSONArray servicearr = data.getJSONArray("Data");
					DataHolder.services.clear();
					svcNameList.clear();
					for (int i = 0; i < servicearr.length(); i++) {
						JSONObject service = servicearr.getJSONObject(i)
								.getJSONObject("ServiceName");
						Service catsvc = new Service();
						catsvc.setId(service.getString("id"));
						catsvc.setName(service.getString("name"));
						svcNameList.add(catsvc.getName());
						DataHolder.services.add(catsvc);
					}
					Log.v("svs", "total no=" + services.toString());
					showSvcDialog();
				} else {
					CommonUtils.showDialog(getActivity(),
							"Error occured!\nPlease try again.");
				}
			} catch (Exception e) {
				e.printStackTrace();
				CommonUtils.showDialog(getActivity(),
						"Invalid response from server.");
			}
			break;
		}
	}

	@SuppressLint("InflateParams")
	private void showSvcDialog() {
		dialog = new Dialog(getActivity());
		dialog.setTitle("Select Service");
		LayoutInflater li = (LayoutInflater) getActivity().getSystemService(
				Context.LAYOUT_INFLATER_SERVICE);
		View view = li.inflate(R.layout.svc_list_dialog, null, false);
		dialog.setContentView(view);
		dialog.setCancelable(true);
		ListView listsvc = (ListView) dialog.findViewById(R.id.listSVC);
		if (!svcNameList.isEmpty()) {
			listsvc.setAdapter(new CustomSpinnerAdapter(getActivity(),
					svcNameList));
		} else {
			CommonUtils.showDialog(getActivity(), "error");
		}
		listsvc.setOnItemClickListener(this);
		dialog.show();
	}

	@SuppressLint("InflateParams")
	private void showCatDialog() {
		listDialog = new Dialog(getActivity());
		listDialog.setTitle("Select Category");
		LayoutInflater li = (LayoutInflater) getActivity().getSystemService(
				Context.LAYOUT_INFLATER_SERVICE);
		View v = li.inflate(R.layout.employee_namelist, null, false);
		listDialog.setContentView(v);
		listDialog.setCancelable(true);
		ListView listEmp = (ListView) listDialog.findViewById(R.id.listemp);
		listEmp.setAdapter(new CustomSpinnerAdapter(getActivity(), catNameList));
		listEmp.setOnItemClickListener(this);
		listDialog.show();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		switch (parent.getId()) {
		case R.id.listemp:
			DataHolder.categorySelected = DataHolder.categories.get(position);
			listDialog.dismiss();
			txtServiceName.setText(DataHolder.categorySelected.getName());
			List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
			values.add(new BasicNameValuePair("cat_id", categorySelected
					.getId()));
			new CallService(this, getActivity(), Constants.REQ_GET_SERVICES,
					true, values).execute(Constants.GET_SERVICES);
			break;
		case R.id.listSVC:
			DataHolder.selectedService = services.get(position);
			dialog.dismiss();
			searchFlag = false;
			callService();
			break;
		}
	}
}
