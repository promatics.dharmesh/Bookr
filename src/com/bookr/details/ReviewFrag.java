package com.bookr.details;

import static com.bookr.utils.DataHolder.selectedSaloon;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.bookr.R;
import com.bookr.adapters.SalonReviewListAdapter;
import com.bookr.models.SalonReview;
import com.bookr.utils.CallService;
import com.bookr.utils.CommonUtils;
import com.bookr.utils.Constants;
import com.bookr.utils.DataHolder;
import com.bookr.utils.DynamicHeight;
import com.bookr.utils.ServiceCallBack;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;

@SuppressWarnings("deprecation")
public class ReviewFrag extends Fragment implements ServiceCallBack {
	ListView reviewList;
	AQuery aq;
	SliderLayout imgSlider;
	ArrayList<SalonReview> reviews = new ArrayList<SalonReview>();

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		aq = new AQuery(getActivity());
		ViewGroup header = (ViewGroup) inflater.inflate(R.layout.listheader,
				reviewList, false);
		header.setOnClickListener(null);
		View v = inflater.inflate(R.layout.reviewfrag, container, false);
		imgSlider = (SliderLayout) v.findViewById(R.id.slider);
		if (selectedSaloon != null && selectedSaloon.getImgs() != null)
			for (String Image : DataHolder.selectedSaloon.getImgs()) {
				DefaultSliderView sv = new DefaultSliderView(getActivity());
				sv.image(Image);
				imgSlider
						.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
				imgSlider.addSlider(sv);
			}
		imgSlider.setDuration(5000);
		imgSlider.getPagerIndicator().setVisibility(View.GONE);
		reviewList = (ListView) v.findViewById(R.id.ReviewList);
		List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
		values.add(new BasicNameValuePair("saloon_id",
				DataHolder.selectedSaloon.getId()));
		new CallService(this, getActivity(), Constants.REQ_SALON_REVIEW, true,
				values).execute(Constants.SALON_REVIEW);
		reviewList.setOnItemClickListener(null);
		reviewList.addHeaderView(header);
		// aq.id(imgSlider).image(DataHolder.selectedSaloon.getImgs().length);
		return v;
	}

	@Override
	public void onServiceResponse(int requestCode, JSONObject data) {
		switch (requestCode) {
		case Constants.REQ_SALON_REVIEW:
			try {
				int code = data.getInt("responseCode");
				if (code == 1) {
					JSONArray revArray = data.getJSONArray("Data");
					reviews.clear();
					for (int i = 0; i < revArray.length(); i++) {
						SalonReview reviewSalon = new SalonReview();
						JSONObject revObj = revArray.getJSONObject(i)
								.getJSONObject("Review");
						reviewSalon.setTitle(revObj.getString("title"));
						reviewSalon.setRating(revObj.getString("rating"));
						reviewSalon.setReview(revObj.getString("review"));
						JSONObject visiObj = revArray.getJSONObject(i)
								.getJSONObject("Visitor");
						reviewSalon.setUsername(visiObj.getString("name"));
						reviews.add(reviewSalon);
					}
					reviewList.setAdapter(new SalonReviewListAdapter(
							getActivity(), reviews));
					DynamicHeight.setListViewHeightBasedOnChildren(reviewList);
					if (reviews.size() == 0) {
						CommonUtils.showDialog(getActivity(),
								"No Review Available for this salon");
					}
				} else {
					CommonUtils.showDialog(getActivity(),
							data.getJSONObject("Message").getString("error"));
				}
			} catch (Exception e) {
				e.printStackTrace();
				Toast.makeText(getActivity(), "error Occured",
						Toast.LENGTH_LONG).show();
			}
			break;
		}
	}
}
