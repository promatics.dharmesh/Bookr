package com.bookr.details;

import static com.bookr.utils.DataHolder.selectedslot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bookr.Dashboard;
import com.bookr.MyBookingActivity;
import com.bookr.R;
import com.bookr.utils.CallService;
import com.bookr.utils.CommonUtils;
import com.bookr.utils.Constants;
import com.bookr.utils.DataHolder;
import com.bookr.utils.ServiceCallBack;
import com.bookr.utils.User;

@SuppressWarnings("deprecation")
@SuppressLint("SimpleDateFormat")
public class BookingReview extends Activity implements OnClickListener,
		ServiceCallBack {
	TextView txtTestService, txtSetDate, txtSetTime, txtSetPrice,
			txtTestDetail, txtSalonNme;;
	Button btnBookAppointment;
	ImageView imgCall;
	String number;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bookingreview);
		txtTestService = (TextView) findViewById(R.id.txtTestService);
		txtSetDate = (TextView) findViewById(R.id.txtSetDate);
		txtSetTime = (TextView) findViewById(R.id.txtSetTime);
		txtSalonNme = (TextView) findViewById(R.id.txtSalonNme);
		imgCall = (ImageView) findViewById(R.id.imgCall);
		// txtSetPrice=(TextView)findViewById(R.id.txtSetPrice);
		// txtTestDetail=(TextView)findViewById(R.id.txtTestDetail);
		btnBookAppointment = (Button) findViewById(R.id.btnBookAppointment);
		if (DataHolder.selectedService != null) {
			txtTestService.setText(DataHolder.selectedService.getName());
			System.out.println(txtTestService.getText());
		} else {
			txtTestService.setText(selectedslot.getEmpName());
			System.out.println(txtTestService.getText());
		}
		// if (selectedslot.getEmpName() != null) {
		// txtTestService.setText(selectedslot.getEmpName());
		// System.out.println(txtTestService.getText());
		// } else {
		// txtTestService.setText(DataHolder.selectedService.getName());
		// System.out.println(txtTestService.getText());
		// }
		txtSalonNme.setText(selectedslot.getSalonName());
		// txtTestDetail.setText(selectedslot.getDescription());
		number = "tel:" + DataHolder.empNo;
		imgCall.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_DIAL);
				intent.setData(Uri.parse(number));
				startActivity(intent);
			}
		});
		txtSetDate.setText(DataHolder.selectedDate);
		txtSetTime.setText(selectedslot.getStartTime().substring(0, 5) + "-"
				+ selectedslot.getEndTime().substring(0, 5));
		String time = txtSetTime.getText().toString();
		StringTokenizer tokens = new StringTokenizer(time, "-");
		String first = tokens.nextToken();
		String second = tokens.nextToken();
		System.out.println(first + second);
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
			Date dateObj = sdf.parse(first);
			Date dateObj1 = sdf.parse(second);
			txtSetTime.setText(new SimpleDateFormat("hh:mm a").format(dateObj)
					+ " - " + new SimpleDateFormat("hh:mm a").format(dateObj1));
		} catch (final Exception e) {
			e.printStackTrace();
		}
		// txtSetPrice.setText("KD" + selectedslot.getPrice() + " and up");
		btnBookAppointment.setOnClickListener(this);
	}

	@Override
	public void onBackPressed() {
		if (isTaskRoot()) {
			Intent i = new Intent(this, Dashboard.class);
			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
					| Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(i);
			finish();
		} else {
			DataHolder.selectedslot = null;
			super.onBackPressed();
		}
	}

	@Override
	public void onClick(View v) {
		List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
		values.add(new BasicNameValuePair("visitor_id", User.getInstance()
				.getUserID()));
		values.add(new BasicNameValuePair("saloon_id", selectedslot
				.getSalonId()));
		if (BookFrag.searchFlag == false) {
			values.add(new BasicNameValuePair("service_id",
					DataHolder.selectedService.getId()));
		}
		values.add(new BasicNameValuePair("slot_id", selectedslot.getSlotid()));
		values.add(new BasicNameValuePair("appointment_date", txtSetDate
				.getText().toString()));

		System.out.println(values);

		new CallService(this, this, Constants.REQ_GET_BOOKINGS, true, values)
				.execute(Constants.GET_BOOKINGS);
	}

	@Override
	public void onServiceResponse(int requestCode, JSONObject data) {
		switch (requestCode) {
		case Constants.REQ_GET_BOOKINGS:
			try {
				int code = data.getInt("responseCode");
				if (code == 1) {
					AlertDialog.Builder alert = new AlertDialog.Builder(
							new ContextThemeWrapper(this,
									android.R.style.Theme_Holo_Light));
					alert.setMessage("Your Appointment has been booked successfully");
					alert.setTitle("Appointment");
					alert.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									DataHolder.selectedslot = null;
									Intent i = new Intent(BookingReview.this,
											MyBookingActivity.class);
									startActivity(i);
									finish();
								}
							});
					alert.show();
				} else {
					CommonUtils.showDialog(this, data.getJSONObject("Message")
							.getString("error"));
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("error");
				CommonUtils.showDialog(this, "Error Occured:" + e.getMessage());
			}
			break;

		}
	}
}
