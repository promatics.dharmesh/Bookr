package com.bookr.details;

import static com.bookr.utils.DataHolder.selectedSaloon;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.bookr.R;
import com.bookr.models.Saloon;
import com.bookr.utils.CallService;
import com.bookr.utils.CommonUtils;
import com.bookr.utils.Constants;
import com.bookr.utils.DataHolder;
import com.bookr.utils.ServiceCallBack;
import com.bookr.utils.User;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;

@SuppressWarnings("deprecation")
public class DetailFrag extends Fragment implements OnClickListener,
		ServiceCallBack {
	TextView txtAddr, txtAbout, txtReview;
	RatingBar ratingBar;
	AQuery aq;
	SliderLayout imgSlider;
	TextView txtAddFav;
	String salonID, userID;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		aq = new AQuery(getActivity());
		View v = inflater.inflate(R.layout.detail_frag, container, false);
		imgSlider = (SliderLayout) v.findViewById(R.id.slider);
		txtAddFav = (TextView) v.findViewById(R.id.txtAddFav);
		txtAddr = (TextView) v.findViewById(R.id.txtAddress);
		txtAbout = (TextView) v.findViewById(R.id.txtAbout);
		txtReview = (TextView) v.findViewById(R.id.txtReviews);
		ratingBar = (RatingBar) v.findViewById(R.id.saloonRating);
		setData();
		return v;
	}

	// private void CallService() {
	// List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
	// values.add(new BasicNameValuePair("saloon_id", salonID));
	// if (User.getInstance() != null) {
	// values.add(new BasicNameValuePair("visitor_id", userID));
	// }
	// new CallService(this, getActivity(), Constants.REQ_GET_SALON, true,
	// values).execute(Constants.GET_SALOON);
	//
	// }

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.txtAddFav:
			if (User.getInstance() != null) {
				callService();
			} else {
				Toast.makeText(getActivity(),
						"You can favorite any salon after Login",
						Toast.LENGTH_LONG).show();
			}
			break;
		}
	}

	/**
	 * get the Salon's current favourite status and sets the TextView Drawable
	 * and Text Accordingly
	 * */
	public void refreshFavStatus() {
		if (selectedSaloon.isFavourite()) {
			// favourite
			txtAddFav.setCompoundDrawablesWithIntrinsicBounds(
					R.drawable.heart_full, 0, 0, 0);
			txtAddFav.setText(" Favorite");
		} else {
			// not fav
			txtAddFav.setCompoundDrawablesWithIntrinsicBounds(
					R.drawable.heart_empty, 0, 0, 0);
			txtAddFav.setText("Add to Favorite");
		}
	}

	private void callService() {
		List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
		values.add(new BasicNameValuePair("visitor_id", User.getInstance()
				.getUserID()));
		values.add(new BasicNameValuePair("saloon_id", selectedSaloon.getId()));
		new CallService(this, getActivity(), Constants.REQ_ADD_FAVOURITE, true,
				values).execute(Constants.ADD_FAVOURITE);
	}

	@Override
	public void onResume() {
		super.onResume();
		setData();
	}

	@Override
	public void onServiceResponse(int requestCode, JSONObject data) {
		switch (requestCode) {
		case Constants.REQ_GET_SALON:
			try {
				int code = data.getInt("responseCode");
				if (code == 1) {
					JSONObject salonDetail = data.getJSONObject("Data")
							.getJSONObject("User");
					selectedSaloon = new Saloon();
					selectedSaloon.setId(salonDetail.getString("id"));
					selectedSaloon.setBusiness_name(salonDetail
							.getString("business_name"));
					SaloonDetailActivity.title.setText(selectedSaloon
							.getBusiness_name());
					selectedSaloon.setAddress(salonDetail.getString("address"));
					selectedSaloon.setAbout_us(salonDetail
							.getString("about_us"));
					selectedSaloon.setCity_id(salonDetail.getString("city_id"));
					selectedSaloon.setLat(salonDetail.getString("lat"));
					selectedSaloon.setLng(salonDetail.getString("lng"));
					selectedSaloon.setReview(salonDetail.getString("review"));
					selectedSaloon.setRating(salonDetail.getString("rating"));
					if (salonDetail.has("favourite_status")) {
						selectedSaloon.setFavourite(salonDetail.getString(
								"favourite_status").equals("1"));
					}
					JSONArray imgArr = data.getJSONObject("Data").getJSONArray(
							"BusinessImage");
					if (imgArr != null && imgArr.length() != 0) {
						String[] imgs = new String[imgArr.length()];
						for (int j = 0; j < imgArr.length(); j++) {
							imgs[j] = imgArr.getJSONObject(j).getString("url");
							if (j == 0) {
								selectedSaloon.setImg(imgs[0]);
							}
							imgs[j] = imgArr.getJSONObject(j).getString("url");
						}
						selectedSaloon.setImgs(imgs);
					}
					DataHolder.saloons.add(selectedSaloon);
					setData();
				} else {
					CommonUtils.showDialog(getActivity(),
							"Error occured!\nPlease try again.");
				}
			} catch (Exception e) {
				e.printStackTrace();
				CommonUtils.showDialog(getActivity(),
						"Invalid response from server.");
			}
			break;
		case Constants.REQ_ADD_FAVOURITE:
			try {
				int code = data.getInt("responseCode");
				if (code == 1) {
					boolean status = data.getInt("status") == 1;
					selectedSaloon.setFavourite(status);
					if (status) {
						// favourite
						txtAddFav.setCompoundDrawablesWithIntrinsicBounds(
								R.drawable.heart_full, 0, 0, 0);
						txtAddFav.setText(" Favorite");
					} else {
						// not fav
						txtAddFav.setCompoundDrawablesWithIntrinsicBounds(
								R.drawable.heart_empty, 0, 0, 0);
						txtAddFav.setText("Add to Favorite");
					}
				} else {
					CommonUtils.showDialog(getActivity(),
							"Error occured!\nPlease try again.");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		}
	}

	private void setData() {

		if (selectedSaloon != null && selectedSaloon.getImgs() != null)
			for (String Image : DataHolder.selectedSaloon.getImgs()) {
				DefaultSliderView sv = new DefaultSliderView(getActivity());
				sv.image(Image);
				imgSlider
						.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
				imgSlider.addSlider(sv);
			}
		imgSlider.setDuration(5000);
		imgSlider.getPagerIndicator().setVisibility(View.GONE);

		if (selectedSaloon != null) {
			txtAddr.setText(selectedSaloon.getAddress());
			txtAbout.setText(selectedSaloon.getAbout_us());
			txtReview.setText(selectedSaloon.getReview() + " Reviews");
			float ratings = Float.valueOf(selectedSaloon.getRating());
			ratingBar.setRating(ratings);
			txtAddFav.setOnClickListener(this);
			refreshFavStatus();
		}

	}
}
