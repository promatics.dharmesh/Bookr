package com.bookr.details;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bookr.R;
import com.bookr.utils.DataHolder;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapFrag extends Fragment implements OnMapReadyCallback {

	private GoogleMap googleMap;
	private Double latitude, longitude;
	private SupportMapFragment mapFragment;
	private View v;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		try {
			if (v != null) {
				ViewGroup parent = (ViewGroup) v.getParent();
				if (parent != null)
					parent.removeView(v);
			}
			try {
				v = inflater.inflate(R.layout.map_frag, container, false);
			} catch (InflateException e) {
				return v;
			}
			latitude = Double.parseDouble(DataHolder.selectedSaloon.getLat());
			longitude = Double.parseDouble(DataHolder.selectedSaloon.getLng());
			mapFragment = (SupportMapFragment) getChildFragmentManager()
					.findFragmentById(R.id.myMap);
			mapFragment.getMapAsync(this);
			return v;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private void setUpMap() {
		googleMap.setMyLocationEnabled(true);
		googleMap.getUiSettings().setZoomControlsEnabled(true);
		googleMap.addMarker(new MarkerOptions()
				.position(new LatLng(latitude, longitude))
				.title(DataHolder.selectedSaloon.getBusiness_name())
				.snippet(DataHolder.selectedSaloon.getAddress()));
		googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
				latitude, longitude), 12.0f));
	}

	@Override
	public void onMapReady(GoogleMap arg0) {
		Log.v("map", "GoogleMap is ready");
		if (googleMap == null)
			googleMap = mapFragment.getMap();
		if (googleMap != null)
			setUpMap();

	}
}
