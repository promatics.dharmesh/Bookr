package com.bookr.details;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.bookr.R;
import com.bookr.adapters.SalonOfferListAdapter;
import com.bookr.models.SalonOffers;
import com.bookr.utils.CallService;
import com.bookr.utils.CommonUtils;
import com.bookr.utils.Constants;
import com.bookr.utils.DataHolder;
import com.bookr.utils.ServiceCallBack;
import com.bookr.utils.User;

@SuppressWarnings("deprecation")
public class OffersFrag extends Fragment implements ServiceCallBack {
	ListView offerList;
	ArrayList<SalonOffers> offers = new ArrayList<SalonOffers>();

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.offer_frag, container, false);
		offerList = (ListView) v.findViewById(R.id.listsalonOffer);
		List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
		values.add(new BasicNameValuePair("saloon_id",
				DataHolder.selectedSaloon.getId()));
		if (User.getInstance() != null) {
			values.add(new BasicNameValuePair("visitor_id", User.getInstance()
					.getUserID()));
		}
		new CallService(this, getActivity(), Constants.REQ_GET_SALON_OFFER,
				true, values).execute(Constants.GET_SALON_OFFER);
		return v;
	}

	@Override
	public void onServiceResponse(int requestCode, JSONObject data) {
		switch (requestCode) {
		case Constants.REQ_GET_SALON_OFFER:
			try {
				int code = data.getInt("responseCode");
				if (code == 1) {
					JSONArray offerArr = data.getJSONArray("Data");
					offers.clear();
					for (int i = 0; i < offerArr.length(); i++) {
						SalonOffers offr = new SalonOffers();
						JSONObject obj = offerArr.getJSONObject(i)
								.getJSONObject("Promotion");
						offr.setTitle(obj.getString("title"));
						offr.setDescription(obj.getString("description"));
						offr.setExpDate(obj.getString("expiry_date"));
						offers.add(offr);
					}
					Log.v("offers", "Total offer" + offers.size());
					offerList.setAdapter(new SalonOfferListAdapter(
							getActivity(), offers));
					if (offers.size() == 0) {
						CommonUtils.showDialog(getActivity(),
								"No Offer Available for this salon");
					}
				} else {
					CommonUtils.showDialog(getActivity(),
							data.getJSONObject("Message").getString("error"));
				}
			} catch (Exception e) {
				e.printStackTrace();
				Toast.makeText(getActivity(), "error Occured",
						Toast.LENGTH_LONG).show();
			}
			break;
		}
	}
}
