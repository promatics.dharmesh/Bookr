package com.bookr.details;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bookr.AboutBookMe;
import com.bookr.ContactAdmin;
import com.bookr.Dashboard;
import com.bookr.LoginView;
import com.bookr.MyBookingActivity;
import com.bookr.MyFavouriteActivity;
import com.bookr.MyOffersActivity;
import com.bookr.MyProfile;
import com.bookr.MyReviewsActivity;
import com.bookr.MyRewardsActivity;
import com.bookr.PromotedSalons;
import com.bookr.R;
import com.bookr.SearchSalon;
import com.bookr.Splash;
import com.bookr.gcm.GCMHelper;
import com.bookr.utils.DataHolder;
import com.bookr.utils.ServiceCallBack;
import com.bookr.utils.User;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

public class SaloonDetailActivity extends FragmentActivity implements
		OnClickListener, ServiceCallBack {

	FragmentTabHost host;
	ImageView imgDrawer;
	public static TextView title, selectLocaton, searchServices, searchSalon,
			promotedSalon, myProfile, myBookings, myReviews, myFavourites,
			myRewards, myOffers, logout, facebook, twitter, instagram, youtube,
			contactAdmin, aboutBookme, name, txtNewSerchSvc, txtNewSerchName,
			txtNewProm, txtLogin, txtAboutBookr, txtContact;
	SlidingMenu menu, newMenu;
	public String userID, SalonID;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.saloon_detail);
		if (User.getInstance() != null) {
			initDrawer();
		} else {
			newDrawer();
		}
		imgDrawer = (ImageView) findViewById(R.id.imgDrawer);
		imgDrawer.setOnClickListener(this);
		title = (TextView) findViewById(R.id.title);
		initTabs();
	}

	private void newDrawer() {
		newMenu = new SlidingMenu(this);
		newMenu.setMode(SlidingMenu.RIGHT);
		newMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
		newMenu.setBehindOffset(100);
		newMenu.setShadowDrawable(R.drawable.shadow);
		newMenu.setShadowWidthRes(R.dimen.shadow_width);
		newMenu.setFadeDegree(0.35f);
		newMenu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
		newMenu.setMenu(R.layout.newdrawer);
		txtNewSerchSvc = (TextView) newMenu
				.findViewById(R.id.txt_searchservices);
		txtNewSerchName = (TextView) newMenu
				.findViewById(R.id.txt_serachsalons);
		txtNewProm = (TextView) newMenu.findViewById(R.id.txt_promotedsalons);
		txtLogin = (TextView) newMenu.findViewById(R.id.txt_login);
		txtContact = (TextView) newMenu.findViewById(R.id.txtContactAdmin);
		txtAboutBookr = (TextView) newMenu.findViewById(R.id.txtAboutBookr);

		txtNewSerchSvc.setOnClickListener(this);
		txtNewSerchName.setOnClickListener(this);
		txtNewProm.setOnClickListener(this);
		txtLogin.setOnClickListener(this);
		txtContact.setOnClickListener(this);
		txtAboutBookr.setOnClickListener(this);
	}

	@Override
	public void onBackPressed() {
		if (User.getInstance() != null) {
			if (isTaskRoot()) {
				Intent i = new Intent(this, Dashboard.class);
				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
						| Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(i);
				finish();

			} else if (menu.isMenuShowing()) {
				menu.toggle(true);
			} else {
				super.onBackPressed();
			}
		} else
			super.onBackPressed();
	}

	private void initDrawer() {
		menu = new SlidingMenu(this);
		menu.setMode(SlidingMenu.RIGHT);
		menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
		menu.setBehindOffset(100);
		menu.setShadowDrawable(R.drawable.shadow);
		menu.setShadowWidthRes(R.dimen.shadow_width);
		menu.setFadeDegree(0.35f);
		menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
		menu.setMenu(R.layout.drawer);

		searchServices = (TextView) menu.findViewById(R.id.txt_searchServices);
		searchSalon = (TextView) menu.findViewById(R.id.txt_serachSalons);
		promotedSalon = (TextView) menu.findViewById(R.id.txt_promotedSalons);
		myProfile = (TextView) menu.findViewById(R.id.txt_myProfile);
		myBookings = (TextView) menu.findViewById(R.id.txt_myBooking);
		myReviews = (TextView) menu.findViewById(R.id.txt_myReviews);
		myFavourites = (TextView) menu.findViewById(R.id.txtMyfavourites);
		myRewards = (TextView) menu.findViewById(R.id.txtMyrewards);
		myOffers = (TextView) menu.findViewById(R.id.txtMyOffers);
		logout = (TextView) menu.findViewById(R.id.txt_logout);
		// facebook = (TextView) menu.findViewById(R.id.txt_fb);
		// twitter = (TextView) menu.findViewById(R.id.txt_twitter);
		// instagram = (TextView) menu.findViewById(R.id.txt_instagram);
		contactAdmin = (TextView) menu.findViewById(R.id.txt_contactAdmin);
		aboutBookme = (TextView) menu.findViewById(R.id.txt_aboutBookme);
		name = (TextView) menu.findViewById(R.id.txt_username);
		if (User.getInstance().getName() != null) {
			name.setText(User.getInstance().getName());
		}
		searchServices.setOnClickListener(this);
		searchSalon.setOnClickListener(this);
		promotedSalon.setOnClickListener(this);
		myProfile.setOnClickListener(this);
		myBookings.setOnClickListener(this);
		myReviews.setOnClickListener(this);
		myFavourites.setOnClickListener(this);
		myRewards.setOnClickListener(this);
		myOffers.setOnClickListener(this);
		logout.setOnClickListener(this);
		// facebook.setOnClickListener(this);
		// twitter.setOnClickListener(this);
		// instagram.setOnClickListener(this);
		contactAdmin.setOnClickListener(this);
		aboutBookme.setOnClickListener(this);
	}

	@SuppressLint("InflateParams")
	private View indicator(String txt) {
		View v = getLayoutInflater().inflate(R.layout.tab_indicator, null,
				false);
		((TextView) v.findViewById(R.id.txtTab)).setText(txt);
		return v;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.imgDrawer:
			if (User.getInstance() != null) {
				menu.toggle();
			} else {
				newMenu.toggle();
			}
			break;
		case R.id.txt_searchServices:
			Intent services = new Intent(this, Dashboard.class);
			startActivity(services);
			menu.toggle();
			break;
		case R.id.txt_serachSalons:
			Intent searchSalon = new Intent(this, SearchSalon.class);
			startActivity(searchSalon);
			menu.toggle();
			break;
		case R.id.txt_promotedSalons:
			Intent promoted = new Intent(this, PromotedSalons.class);
			startActivity(promoted);
			menu.toggle();
			break;
		case R.id.txt_myProfile:
			Intent intent = new Intent(this, MyProfile.class);
			startActivity(intent);
			menu.toggle();
			break;
		case R.id.txt_myBooking:
			Intent i = new Intent(this, MyBookingActivity.class);
			startActivity(i);
			menu.toggle();
			break;
		case R.id.txt_myReviews:
			Intent review = new Intent(this, MyReviewsActivity.class);
			startActivity(review);
			menu.toggle();
			break;
		case R.id.txtMyfavourites:
			Intent MyFav = new Intent(this, MyFavouriteActivity.class);
			startActivity(MyFav);
			menu.toggle();
			break;
		case R.id.txtMyrewards:
			Intent rewards = new Intent(this, MyRewardsActivity.class);
			startActivity(rewards);
			menu.toggle();
			break;
		case R.id.txtMyOffers:
			Intent offers = new Intent(this, MyOffersActivity.class);
			startActivity(offers);
			menu.toggle();
			break;
		case R.id.txt_logout:
			menu.toggle();
			if (!GCMHelper.getSavedRegistrationId().equals("")) {
				GCMHelper.unregister();
			}
			User.getInstance().logout();
			Intent intentLog = new Intent(this, Splash.class);
			startActivity(intentLog);
			Toast.makeText(this, "You have successfully logout",
					Toast.LENGTH_SHORT).show();
			finish();
			break;
		// case R.id.txt_fb:
		// Toast.makeText(this, "This module is pending", Toast.LENGTH_SHORT)
		// .show();
		// break;
		// case R.id.txt_twitter:
		// Toast.makeText(this, "This module is pending", Toast.LENGTH_SHORT)
		// .show();
		// break;
		// case R.id.txt_instagram:
		// Toast.makeText(this, "This module is pending", Toast.LENGTH_SHORT)
		// .show();
		// break;
		case R.id.txt_contactAdmin:
			menu.toggle();
			Intent email = new Intent(this, ContactAdmin.class);
			startActivity(email);
			break;
		case R.id.txt_aboutBookme:
			Intent about = new Intent(this, AboutBookMe.class);
			startActivity(about);
			menu.toggle();
			break;
		case R.id.txt_searchservices:
			Intent service = new Intent(this, Dashboard.class);
			startActivity(service);
			newMenu.toggle();
			break;
		case R.id.txt_serachsalons:
			Intent salon = new Intent(this, SearchSalon.class);
			startActivity(salon);
			newMenu.toggle();
			break;
		case R.id.txt_promotedsalons:
			Intent promo = new Intent(this, PromotedSalons.class);
			startActivity(promo);
			newMenu.toggle();
			break;
		case R.id.txt_login:
			Intent login = new Intent(this, LoginView.class);
			startActivity(login);
			break;
		case R.id.txtContactAdmin:
			newMenu.toggle();
			Intent emails = new Intent(this, ContactAdmin.class);
			startActivity(emails);
			break;
		case R.id.txtAboutBookr:
			Intent abouts = new Intent(this, AboutBookMe.class);
			startActivity(abouts);
			newMenu.toggle();
			break;
		}
	}

	@Override
	public void onServiceResponse(int requestCode, JSONObject data) {
		for (Fragment frag : getSupportFragmentManager().getFragments()) {
			if (frag instanceof DetailFrag) {
				((DetailFrag) frag).refreshFavStatus();
			}
		}
	}

	private void initTabs() {
		host = (FragmentTabHost) findViewById(android.R.id.tabhost);
		host.setup(this, getSupportFragmentManager(), R.id.realtabcontent);
		host.addTab(host.newTabSpec("BOOK").setIndicator(indicator("BOOK")),
				BookFrag.class, null);
		host.addTab(host.newTabSpec("INFO").setIndicator(indicator("INFO")),
				DetailFrag.class, null);
		host.addTab(host.newTabSpec("MAP").setIndicator(indicator("MAP")),
				MapFrag.class, null);
		host.addTab(
				host.newTabSpec("REVIEW").setIndicator(indicator("REVIEW")),
				ReviewFrag.class, null);
		host.addTab(
				host.newTabSpec("OFFERS").setIndicator(indicator("OFFERS")),
				OffersFrag.class, null);
		if (DataHolder.selectedSaloon != null) {
			title.setText(DataHolder.selectedSaloon.getBusiness_name());
		}
		if (DataHolder.salonList == true) {
			host.setCurrentTab(0);
			DataHolder.salonList = false;
		} else if (DataHolder.offerList == true) {
			host.setCurrentTab(4);
			DataHolder.offerList = false;
		} else {
			host.setCurrentTab(1);
		}
	}
}
