package com.bookr;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bookr.gcm.GCMHelper;
import com.bookr.utils.CallService;
import com.bookr.utils.CommonUtils;
import com.bookr.utils.Constants;
import com.bookr.utils.ServiceCallBack;
import com.bookr.utils.User;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

@SuppressWarnings("deprecation")
@SuppressLint("SimpleDateFormat")
public class MyProfile extends Activity implements OnClickListener,
		ServiceCallBack {

	EditText etName, etUname, etPhone;
	TextView txtEmail, txtDob, txtName, txtEmails, txtPhone;
	Button submit, changePass;
	int Year, Month, Day;
	TextView searchServices, searchSalon, promotedSalon, myProfile, myBookings,
			myReviews, myFavourites, myrewards, myOffers, logout, facebook,
			twitter, instagram, youtube, contactAdmin, aboutBookme, name;
	SlidingMenu menu;
	ImageView imgdrawer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.visitor_profile);
		initDrawer();
		imgdrawer = (ImageView) findViewById(R.id.imgDrawer);
		imgdrawer.setOnClickListener(this);
		etName = (EditText) findViewById(R.id.etName);
		// etUname = (EditText) findViewById(R.id.txtUsername);
		etPhone = (EditText) findViewById(R.id.txtPhone);
		// txtDob = (TextView) findViewById(R.id.txtdob);
		txtEmail = (TextView) findViewById(R.id.txtEmail);
		submit = (Button) findViewById(R.id.btnUpdate);
		changePass = (Button) findViewById(R.id.btnChangePass);
		txtEmails = (TextView) findViewById(R.id.txtemail);
		txtName = (TextView) findViewById(R.id.txtname);
		txtPhone = (TextView) findViewById(R.id.txtphone);
		// if (!User.getInstance().getDob().equals("0000-00-00")) {
		// String dob = User.getInstance().getDob();
		// try {
		// SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		// Date dateObj = sdf.parse(dob);
		// txtDob.setText(new SimpleDateFormat("dd-MM-yyyy")
		// .format(dateObj));
		// } catch (final Exception e) {
		// e.printStackTrace();
		// }
		// }
		if (!User.getInstance().getName().equals("")) {
			etName.setText(User.getInstance().getName());
		}
		txtEmail.setText(User.getInstance().getEmail());
		txtEmails.setText(User.getInstance().getEmail());
		// etUname.setText(User.getInstance().getUsername());
		etPhone.setText(User.getInstance().getPhone());
		txtPhone.setText(User.getInstance().getPhone());
		txtName.setText(User.getInstance().getName());
		// txtDob.setOnClickListener(this);
		submit.setOnClickListener(this);
		changePass.setOnClickListener(this);
	}

	private void initDrawer() {
		menu = new SlidingMenu(this);
		menu.setMode(SlidingMenu.RIGHT);
		menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
		menu.setBehindOffset(100);
		menu.setShadowDrawable(R.drawable.shadow);
		menu.setShadowWidthRes(R.dimen.shadow_width);
		menu.setFadeDegree(0.35f);
		menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
		menu.setMenu(R.layout.drawer);

		searchServices = (TextView) menu.findViewById(R.id.txt_searchServices);
		searchSalon = (TextView) menu.findViewById(R.id.txt_serachSalons);
		promotedSalon = (TextView) menu.findViewById(R.id.txt_promotedSalons);
		myProfile = (TextView) menu.findViewById(R.id.txt_myProfile);
		myBookings = (TextView) menu.findViewById(R.id.txt_myBooking);
		myReviews = (TextView) menu.findViewById(R.id.txt_myReviews);
		myFavourites = (TextView) menu.findViewById(R.id.txtMyfavourites);
		myrewards = (TextView) menu.findViewById(R.id.txtMyrewards);
		myOffers = (TextView) menu.findViewById(R.id.txtMyOffers);
		logout = (TextView) menu.findViewById(R.id.txt_logout);
		// facebook = (TextView) menu.findViewById(R.id.txt_fb);
		// twitter = (TextView) menu.findViewById(R.id.txt_twitter);
		// instagram = (TextView) menu.findViewById(R.id.txt_instagram);
		contactAdmin = (TextView) menu.findViewById(R.id.txt_contactAdmin);
		aboutBookme = (TextView) menu.findViewById(R.id.txt_aboutBookme);
		name = (TextView) menu.findViewById(R.id.txt_username);
		if (User.getInstance().getName() != null) {
			name.setText(User.getInstance().getName());
		}
		searchServices.setOnClickListener(this);
		searchSalon.setOnClickListener(this);
		promotedSalon.setOnClickListener(this);
		myProfile.setOnClickListener(this);
		myBookings.setOnClickListener(this);
		myReviews.setOnClickListener(this);
		myFavourites.setOnClickListener(this);
		myrewards.setOnClickListener(this);
		myOffers.setOnClickListener(this);
		logout.setOnClickListener(this);
		// facebook.setOnClickListener(this);
		// twitter.setOnClickListener(this);
		// instagram.setOnClickListener(this);
		contactAdmin.setOnClickListener(this);
		aboutBookme.setOnClickListener(this);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.slide_down);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.imgDrawer:
			menu.toggle();
			break;
		case R.id.txt_searchServices:
			Intent services = new Intent(this, Dashboard.class);
			startActivity(services);
			break;
		case R.id.txt_serachSalons:
			Intent searchSalon = new Intent(this, SearchSalon.class);
			startActivity(searchSalon);
			menu.toggle();
			break;
		case R.id.txt_promotedSalons:
			Intent promoted = new Intent(this, PromotedSalons.class);
			startActivity(promoted);
			menu.toggle();
			break;
		case R.id.txt_myProfile:
			// Intent intent = new Intent(this, MyProfile.class);
			// startActivity(intent);
			menu.toggle();
			break;
		case R.id.txt_myBooking:
			Intent i = new Intent(this, MyBookingActivity.class);
			startActivity(i);
			menu.toggle();
			break;
		case R.id.txt_myReviews:
			Intent review = new Intent(this, MyReviewsActivity.class);
			startActivity(review);
			menu.toggle();
			break;
		case R.id.txtMyfavourites:
			Intent MyFav = new Intent(this, MyFavouriteActivity.class);
			startActivity(MyFav);
			menu.toggle();
			break;
		case R.id.txtMyrewards:
			Intent rewards = new Intent(this, MyRewardsActivity.class);
			startActivity(rewards);
			menu.toggle();
			break;
		case R.id.txtMyOffers:
			Intent offers = new Intent(this, MyOffersActivity.class);
			startActivity(offers);
			menu.toggle();
			break;
		case R.id.txt_logout:
			menu.toggle();
			if (!GCMHelper.getSavedRegistrationId().equals("")) {
				GCMHelper.unregister();
			}
			User.getInstance().logout();
			Intent intentLog = new Intent(this, Splash.class);
			intentLog.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_CLEAR_TASK
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intentLog);
			Toast.makeText(this, "You have successfully logged out.",
					Toast.LENGTH_SHORT).show();
			finish();
			break;
		// case R.id.txt_fb:
		// Toast.makeText(this, "This module is pending", Toast.LENGTH_SHORT)
		// .show();
		// break;
		// case R.id.txt_twitter:
		// Toast.makeText(this, "This module is pending", Toast.LENGTH_SHORT)
		// .show();
		// break;
		// case R.id.txt_instagram:
		// Toast.makeText(this, "This module is pending", Toast.LENGTH_SHORT)
		// .show();
		// break;
		case R.id.txt_contactAdmin:
			menu.toggle();
			Intent email = new Intent(this, ContactAdmin.class);
			startActivity(email);
			break;
		case R.id.txt_aboutBookme:
			Intent about = new Intent(this, AboutBookMe.class);
			startActivity(about);
			menu.toggle();
			break;

		// case R.id.txtdob:
		// final Calendar c = Calendar.getInstance();
		// Year = c.get(Calendar.YEAR);
		// Month = c.get(Calendar.MONTH);
		// Day = c.get(Calendar.DAY_OF_MONTH);
		// DatePickerDialog dpd = new DatePickerDialog(this,
		// new OnDateSetListener() {
		// @SuppressLint("SimpleDateFormat")
		// @Override
		// public void onDateSet(DatePicker arg0, int Year,
		// int Month, int Day) {
		// txtDob.setText(Day + "-" + (Month + 1) + "-" + Year);
		// String date = txtDob.getText().toString();
		// try {
		// SimpleDateFormat sdf = new SimpleDateFormat(
		// "dd-MM-yyyy");
		// Date dateObj = sdf.parse(date);
		// txtDob.setText(new SimpleDateFormat(
		// "dd-MM-yyyy").format(dateObj));
		// } catch (final Exception e) {
		// e.printStackTrace();
		// }
		// }
		// }, Year, Month, Day);
		// long t = System.currentTimeMillis();
		// dpd.getDatePicker().setMaxDate(t - 1000);
		// dpd.show();
		// break;

		case R.id.btnUpdate:

			if (etName.getText().toString().isEmpty()) {
				AlertDialog.Builder alert = new Builder(this);
				alert.setMessage("Enter your name");
				alert.setTitle("Alert");
				alert.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
							}
						});
				alert.show();
				return;
			}
			// if (etUname.getText().toString().length() < 4) {
			// AlertDialog.Builder alert = new Builder(this);
			// alert.setMessage("Enter your User name (Min 4)");
			// alert.setTitle("Alert");
			// alert.setPositiveButton("OK",
			// new DialogInterface.OnClickListener() {
			// @Override
			// public void onClick(DialogInterface dialog,
			// int which) {
			// }
			// });
			// alert.show();
			// return;
			// }
			// if (txtDob.getText().toString().equals("")
			// || txtDob.getText().toString()
			// .equalsIgnoreCase("Date of Birth")) {
			// AlertDialog.Builder alert = new Builder(this);
			// alert.setMessage("Enter your Date of Birth");
			// alert.setTitle("Alert");
			// alert.setPositiveButton("OK",
			// new DialogInterface.OnClickListener() {
			// @Override
			// public void onClick(DialogInterface dialog,
			// int which) {
			// }
			// });
			// alert.show();
			// return;
			// }
			if (!etPhone.getText().toString().matches(Patterns.PHONE.pattern())
					&& !etPhone.getText().toString().equals("")) {
				AlertDialog.Builder alert = new Builder(this);
				alert.setMessage("Enter valid Phone number");
				alert.setTitle("Alert");
				alert.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
							}
						});
				alert.show();
				return;
			}
			try {
				InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
						0);
			} catch (Exception e) {
			}
			List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
			values.add(new BasicNameValuePair("id", User.getInstance()
					.getUserID()));
			values.add(new BasicNameValuePair("name", etName.getText()
					.toString()));
			// values.add(new BasicNameValuePair("username", etUname.getText()
			// .toString()));
			values.add(new BasicNameValuePair("phone_no", etPhone.getText()
					.toString()));
			// values.add(new BasicNameValuePair("dob", txtDob.getText()
			// .toString()));
			new CallService(this, this, Constants.REQ_UPDATE_PROFILE, true,
					values).execute(Constants.UPDATE_PROFILE);
			break;

		case R.id.btnChangePass:
			final Dialog dialog = new Dialog(this);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.dialog_change_password);
			final EditText currentPass = (EditText) dialog
					.findViewById(R.id.et_currentPass);
			final EditText newPass = (EditText) dialog
					.findViewById(R.id.et_newPass);
			final EditText cnfPass = (EditText) dialog
					.findViewById(R.id.et_confirmPass);
			final Button sumbitPass = (Button) dialog
					.findViewById(R.id.btn_pass_submit);
			final Button cancel = (Button) dialog
					.findViewById(R.id.btn_pass_cancel);
			sumbitPass.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (!currentPass.getText().toString()
							.equals(User.getInstance().getPassword())) {
						currentPass.setError("Enter your current password");
						return;
					}
					if (newPass.getText().toString().equals("")) {
						newPass.setError("Enter your new Password");
						return;
					}
					if (!cnfPass.getText().toString()
							.equals(newPass.getText().toString())) {
						cnfPass.setError("Confirm password should be same as new password");
						return;
					}
					try {
						InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(getCurrentFocus()
								.getWindowToken(), 0);
					} catch (Exception e) {
					}
					List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
					values.add(new BasicNameValuePair("id", User.getInstance()
							.getUserID()));
					values.add(new BasicNameValuePair("new_password", newPass
							.getText().toString()));
					new CallService(MyProfile.this, MyProfile.this,
							Constants.REQ_CHANGE_PASSWORD, true, values)
							.execute(Constants.CHANGE_PASSWORD);
					dialog.dismiss();
				}
			});
			cancel.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});
			dialog.show();
			break;
		}
	}

	@Override
	public void onServiceResponse(int requestCode, JSONObject data) {
		switch (requestCode) {
		case Constants.REQ_UPDATE_PROFILE:
			try {
				int code = data.getInt("responseCode");
				if (code == 1) {
					// String newd = txtDob.getText().toString();
					// try {
					// SimpleDateFormat sdf = new SimpleDateFormat(
					// "dd-MM-yyyy");
					// Date dateObj = sdf.parse(newd);
					// txtDob.setText(new SimpleDateFormat("yyyy-MM-dd")
					// .format(dateObj));
					// } catch (final Exception e) {
					// e.printStackTrace();
					// }
					User usr = User.getInstance();
					User.storeUser(usr.getUserID(),
							etName.getText().toString(), usr.getUsername(),
							etPhone.getText().toString(), usr.getDob(),
							usr.getPassword(), usr.getLocationId(),
							usr.getCityId(), usr.getEmail(), usr.getUserType());

					AlertDialog.Builder alert = new Builder(this);
					alert.setMessage("Your profile has been successfully updated");
					alert.setTitle("Thanks");
					alert.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									Intent i = new Intent(MyProfile.this,
											Dashboard.class);
									startActivity(i);
								}
							});
					alert.show();
				} else {
					CommonUtils.showDialog(this, data.getJSONObject("Message")
							.getString("error"));
				}
			} catch (Exception e) {
				e.printStackTrace();
				CommonUtils.showDialog(this, "Error Occurred:" + e.getMessage());
			}
			break;
		case Constants.REQ_CHANGE_PASSWORD:
			try {
				int code = data.getInt("responseCode");
				if (code == 1) {
					Toast.makeText(this,
							"Your password has been changed successfully",
							Toast.LENGTH_LONG).show();
				}

			} catch (Exception e) {
				e.printStackTrace();
				CommonUtils.showDialog(this, "Error Occurred:" + e.getMessage());
			}
		}
	}
}
