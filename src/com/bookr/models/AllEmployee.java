package com.bookr.models;

public class AllEmployee {
	private String empId, empName, emapEmail, empPhone, empSalonId, empAddress;

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmapEmail() {
		return emapEmail;
	}

	public void setEmapEmail(String emapEmail) {
		this.emapEmail = emapEmail;
	}

	public String getEmpPhone() {
		return empPhone;
	}

	public void setEmpPhone(String empPhone) {
		this.empPhone = empPhone;
	}

	public String getEmpSalonId() {
		return empSalonId;
	}

	public void setEmpSalonId(String empSalonId) {
		this.empSalonId = empSalonId;
	}

	public String getEmpAddress() {
		return empAddress;
	}

	public void setEmpAddress(String empAddress) {
		this.empAddress = empAddress;
	}

}
