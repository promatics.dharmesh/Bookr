package com.bookr.models;

public class ServiceSlot {

	String price, description, service_name, service_name_id, duration,
			service_id, saloonId;
	TimeSlot[] slots;

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getService_id() {
		return service_id;
	}

	public void setService_id(String service_id) {
		this.service_id = service_id;
	}

	public String getSaloonId() {
		return saloonId;
	}

	public void setSaloonId(String saloonId) {
		this.saloonId = saloonId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getService_name() {
		return service_name;
	}

	public void setService_name(String service_name) {
		this.service_name = service_name;
	}

	public String getService_name_id() {
		return service_name_id;
	}

	public void setService_name_id(String service_name_id) {
		this.service_name_id = service_name_id;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public TimeSlot[] getSlots() {
		return slots;
	}

	public void setSlots(TimeSlot[] slots) {
		this.slots = slots;
	}
}
