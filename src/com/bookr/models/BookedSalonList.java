package com.bookr.models;

public class BookedSalonList {
 String salonId, salonName, bookingId;

public String getSalonId() {
	return salonId;
}

public void setSalonId(String salonId) {
	this.salonId = salonId;
}

public String getSalonName() {
	return salonName;
}

public void setSalonName(String salonName) {
	this.salonName = salonName;
}

public String getBookingId() {
	return bookingId;
}

public void setBookingId(String bookingId) {
	this.bookingId = bookingId;
}
 
}
