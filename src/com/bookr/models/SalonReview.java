package com.bookr.models;

public class SalonReview {
	String reviews, title, username, rating;

	public String getReview() {
		return reviews;
	}

	public void setReview(String review) {
		this.reviews = review;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String ratings) {
		this.rating = ratings;
	}

}
