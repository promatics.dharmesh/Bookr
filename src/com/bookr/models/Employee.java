package com.bookr.models;

public class Employee {

	String empId, empName, empPhone, empStartTime, empEndTime, empSalon;
	TimeSlot[] slots;

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getEmpSalon() {
		return empSalon;
	}

	public void setEmpSalon(String empSalon) {
		this.empSalon = empSalon;
	}

	public String getEmpPhone() {
		return empPhone;
	}

	public void setEmpPhone(String empPhone) {
		this.empPhone = empPhone;
	}

	public String getEmpStartTime() {
		return empStartTime;
	}

	public void setEmpStartTime(String empStartTime) {
		this.empStartTime = empStartTime;
	}

	public String getEmpEndTime() {
		return empEndTime;
	}

	public void setEmpEndTime(String empEndTime) {
		this.empEndTime = empEndTime;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public TimeSlot[] getSlots() {
		return slots;
	}

	public void setSlots(TimeSlot[] slots) {
		this.slots = slots;
	}
}
