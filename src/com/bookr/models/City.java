package com.bookr.models;

public class City {
	String id, loaction_id, cityname, status;

	public City() {
		super();
	}

	public City(String id, String loaction_id, String cityname) {
		super();
		this.id = id;
		this.loaction_id = loaction_id;
		this.cityname = cityname;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLoaction_id() {
		return loaction_id;
	}

	public void setLoaction_id(String loaction_id) {
		this.loaction_id = loaction_id;
	}

	public String getCityname() {
		return cityname;
	}

	public void setCityname(String cityname) {
		this.cityname = cityname;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return id + ":" + cityname;

	}

}
