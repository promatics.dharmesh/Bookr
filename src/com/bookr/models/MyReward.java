package com.bookr.models;

public class MyReward {
	String reward_id, reward_point, reward_price;

	public String getReward_id() {
		return reward_id;
	}

	public void setReward_id(String reward_id) {
		this.reward_id = reward_id;
	}

	public String getReward_point() {
		return reward_point;
	}

	public void setReward_point(String reward_point) {
		this.reward_point = reward_point;
	}

	public String getReward_price() {
		return reward_price;
	}

	public void setReward_price(String reward_price) {
		this.reward_price = reward_price;
	}
	
}
