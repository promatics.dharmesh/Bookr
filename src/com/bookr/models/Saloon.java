package com.bookr.models;

public class Saloon implements Comparable<Saloon> {
	String id, name, business_name, address, about_us, location_id, city_id,
			img, lat, lng, review, rating, distance, promo;

	Employee[] employees;
	TimeSlot[] slots;
	String[] imgs;
	boolean favourite = false;

	public String getPromo() {
		return promo;
	}

	public void setPromo(String promo) {
		this.promo = promo;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public boolean isFavourite() {
		return favourite;
	}

	public void setFavourite(boolean favourite) {
		this.favourite = favourite;
	}

	public String getReview() {
		return review;
	}

	public void setReview(String review) {
		this.review = review;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String[] getImgs() {
		return imgs;
	}

	public void setImgs(String[] imgs) {
		this.imgs = imgs;
	}

	public String getId() {
		return id;
	}

	public Saloon setId(String id) {
		this.id = id;
		return this;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBusiness_name() {
		return business_name;
	}

	public void setBusiness_name(String business_name) {
		this.business_name = business_name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAbout_us() {
		return about_us;
	}

	public void setAbout_us(String about_us) {
		this.about_us = about_us;
	}

	public String getLocation_id() {
		return location_id;
	}

	public void setLocation_id(String location_id) {
		this.location_id = location_id;
	}

	public String getCity_id() {
		return city_id;
	}

	public void setCity_id(String city_id) {
		this.city_id = city_id;
	}

	public Employee[] getEmployees() {
		return employees;
	}

	public void setEmployees(Employee[] svcslots) {
		this.employees = svcslots;
	}

	public TimeSlot[] getSlots() {
		return slots;
	}

	public void setSlots(TimeSlot[] slots) {
		this.slots = slots;
	}

	@Override
	public int compareTo(Saloon another) {
		int myID = Integer.valueOf(this.id);
		int anotherId = Integer.valueOf(another.id);
		return myID - anotherId;
	}

}
