package com.bookr.employees;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.bookr.R;
import com.bookr.Splash;
import com.bookr.gcm.GCMHelper;
import com.bookr.utils.SalonEmployee;

public class EmployeeTabs extends FragmentActivity {
	FragmentTabHost host;
	ImageView imgPopUp;
	TextView txtEmpName;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.employee_frags);
		txtEmpName = (TextView) findViewById(R.id.txtEmpName);
		imgPopUp = (ImageView) findViewById(R.id.imgPopUp);
		txtEmpName.setText(SalonEmployee.getInstance().getEmpName());
		imgPopUp.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				PopupMenu popup = new PopupMenu(EmployeeTabs.this, imgPopUp);
				popup.getMenuInflater().inflate(R.menu.popup_menu,
						popup.getMenu());
				popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
					public boolean onMenuItemClick(MenuItem item) {
						switch (item.getItemId()) {
						case R.id.one:
							Intent i = new Intent(EmployeeTabs.this,
									EmpProfile.class);
							startActivity(i);
							break;

						case R.id.two:
							if(!GCMHelper.getSavedRegistrationId().equals("")){
								GCMHelper.unregister();
							}
							SalonEmployee.getInstance().logout();
							Intent intentLog = new Intent(EmployeeTabs.this,
									Splash.class);
							intentLog.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
									| Intent.FLAG_ACTIVITY_CLEAR_TASK
									| Intent.FLAG_ACTIVITY_NEW_TASK);
							startActivity(intentLog);
							Toast.makeText(EmployeeTabs.this,
									"You have successfully logged out.",
									Toast.LENGTH_SHORT).show();
							break;
						}
						return true;
					}
				});
				popup.show();
			}
		});

		initTabs();
	}

	private void initTabs() {
		host = (FragmentTabHost) findViewById(android.R.id.tabhost);
		host.setup(this, getSupportFragmentManager(), R.id.realtabcontent);
		host.addTab(
				host.newTabSpec("Recent Bookings").setIndicator(
						indicator("Recent Bookings")), EmpRecentBook.class,
				null);
		host.addTab(
				host.newTabSpec("All Bookings").setIndicator(
						indicator("All Bookings")), EmpAllBook.class, null);
		host.setCurrentTab(0);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (SalonEmployee.getInstance() != null) {
			// check if device registered
			GCMHelper.registerIfNotAlreadyDone(this);
			Log.v("On Resume", SalonEmployee.getInstance().getEmpName()
					.toString());

		} else {
			Intent intentLog = new Intent(this, Splash.class);
			intentLog.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_CLEAR_TASK
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intentLog);
			finish();
		}
	}

	@SuppressLint("InflateParams")
	private View indicator(String txt) {
		View v = getLayoutInflater().inflate(R.layout.tab_indicator, null,
				false);
		((TextView) v.findViewById(R.id.txtTab)).setText(txt);
		return v;
	}

}
