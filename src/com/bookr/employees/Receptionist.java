package com.bookr.employees;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.bookr.R;
import com.bookr.Splash;
import com.bookr.adapters.CustomSpinnerAdapter;
import com.bookr.adapters.ReceptAdapter;
import com.bookr.gcm.GCMHelper;
import com.bookr.models.AllEmployee;
import com.bookr.models.EmpApptList;
import com.bookr.utils.CallService;
import com.bookr.utils.CommonUtils;
import com.bookr.utils.Constants;
import com.bookr.utils.DataHolder;
import com.bookr.utils.SalonEmployee;
import com.bookr.utils.ServiceCallBack;

@SuppressWarnings("deprecation")
public class Receptionist extends Activity implements OnClickListener,
		ServiceCallBack, OnItemClickListener {
	ImageView imgPopUp;
	TextView txtEmpName;
	TextView spinEmp;
	ListView listBooking;
	ArrayList<AllEmployee> empList = new ArrayList<AllEmployee>();
	ArrayList<String> empNameList = new ArrayList<String>();
	ArrayList<EmpApptList> allBookingList = new ArrayList<EmpApptList>();
	Dialog listDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.receptionist);
		txtEmpName = (TextView) findViewById(R.id.txtRecpName);
		imgPopUp = (ImageView) findViewById(R.id.imgPopUpRecp);
		spinEmp = (TextView) findViewById(R.id.spinSelectEmp);
		listBooking = (ListView) findViewById(R.id.listbooking);
		txtEmpName.setText(SalonEmployee.getInstance().getEmpName());
		imgPopUp.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				PopupMenu popup = new PopupMenu(Receptionist.this, imgPopUp);
				popup.getMenuInflater().inflate(R.menu.popup_menu,
						popup.getMenu());
				popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
					public boolean onMenuItemClick(MenuItem item) {
						switch (item.getItemId()) {
						case R.id.one:
							Intent i = new Intent(Receptionist.this,
									EmpProfile.class);
							startActivity(i);
							break;

						case R.id.two:
							if (!GCMHelper.getSavedRegistrationId().equals("")) {
								GCMHelper.unregister();
							}
							SalonEmployee.getInstance().logout();
							Intent intentLog = new Intent(Receptionist.this,
									Splash.class);
							intentLog.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
									| Intent.FLAG_ACTIVITY_CLEAR_TASK
									| Intent.FLAG_ACTIVITY_NEW_TASK);
							startActivity(intentLog);
							Toast.makeText(Receptionist.this,
									"You have successfully logged out.",
									Toast.LENGTH_SHORT).show();
							break;
						}
						return true;
					}
				});
				popup.show();
			}
		});
		spinEmp.setOnClickListener(this);
		callservice();
		// List<BasicNameValuePair> values = new
		// ArrayList<BasicNameValuePair>();
		// values.add(new BasicNameValuePair("recep_id", SalonEmployee
		// .getInstance().getEmpId()));
		// new CallService(this, this, Constants.REQ_GET_SALON_ALLBOOKINGS,
		// true,
		// values).execute(Constants.GET_SALON_ALLBOOKINGS);

	}

	private void callservice() {
		List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
		values.add(new BasicNameValuePair("recep_id", SalonEmployee
				.getInstance().getEmpId()));
		new CallService(this, this, Constants.REQ_GET_SALON_ALLBOOKINGS, true,
				values).execute(Constants.GET_SALON_ALLBOOKINGS);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (SalonEmployee.getInstance() != null) {
			// check if device registered
			GCMHelper.registerIfNotAlreadyDone(this);
			Log.v("on resume", SalonEmployee.getInstance().getEmpName()
					.toString());

		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.spinSelectEmp:
			List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
			values.add(new BasicNameValuePair("recep_id", SalonEmployee
					.getInstance().getEmpId()));
			new CallService(this, this, Constants.REQ_GET_EMP_LIST, true,
					values).execute(Constants.GET_EMP_LIST);

			break;

		}
	}

	@Override
	public void onServiceResponse(int requestCode, JSONObject data) {
		switch (requestCode) {
		case Constants.REQ_GET_SALON_ALLBOOKINGS:
			try {
				int code = data.getInt("responseCode");
				if (code == 1) {
					JSONArray allBook = data.getJSONArray("Data");
					allBookingList.clear();
					for (int i = 0; i < allBook.length(); i++) {
						EmpApptList empAll = new EmpApptList();
						JSONObject book = allBook.getJSONObject(i)
								.getJSONObject("Booking");
						empAll.setBookingId(book.getString("id"));
						empAll.setDate(book.getString("appointment_date"));
						empAll.setEmployeeId(book.getString("employee_id"));
						empAll.setStartTime(book.getString("starttime"));
						empAll.setEndTime(book.getString("endtime"));
						empAll.setSlotId(book.getString("slot_id"));
						empAll.setVisitorId(book.getString("visitor_id"));
						empAll.setCustomerName(book.getString("customer_name"));
						empAll.setCustomerPhone(book.getString("phone_no"));
						JSONObject ob = allBook.getJSONObject(i).getJSONObject(
								"ServiceName");
						empAll.setSvcName(ob.getString("name"));
						JSONObject obj = allBook.getJSONObject(i)
								.getJSONObject("User");
						empAll.setSalonName(obj.getString("business_name"));
						allBookingList.add(empAll);
					}
					listBooking.setAdapter(new ReceptAdapter(this,
							allBookingList));
					if (allBookingList.size() == 0) {
						CommonUtils.showDialog(this,
								"No Appointment avaialable");
					}
				} else {
					CommonUtils.showDialog(this, data.getJSONObject("Message")
							.getString("error"));
				}
			} catch (Exception e) {
				e.printStackTrace();
				CommonUtils.showDialog(this, "Invalid response from server");
			}
			break;
		case Constants.REQ_GET_EMP_LIST:
			try {
				int code = data.getInt("responseCode");
				if (code == 1) {
					empList.clear();
					empNameList.clear();
					JSONArray empArr = data.getJSONArray("Data");
					empNameList = new ArrayList<String>(empArr.length() + 1);
					empNameList.add(0, "All");
					for (int i = 0; i < empArr.length(); i++) {
						JSONObject obj = empArr.getJSONObject(i).getJSONObject(
								"Employee");
						AllEmployee allemp = new AllEmployee();
						allemp.setEmpId(obj.getString("id"));
						allemp.setEmapEmail(obj.getString("email"));
						allemp.setEmpName(obj.getString("name"));
						allemp.setEmpAddress(obj.getString("address"));
						allemp.setEmpPhone(obj.getString("phone_no"));
						allemp.setEmpSalonId(obj.getString("user_id"));
						empNameList.add(allemp.getEmpName());
						empList.add(allemp);
					}
					showEmpDialog();
				}
			} catch (Exception e) {
				e.printStackTrace();
				CommonUtils.showDialog(this, "Invalid response from server");
			}
			break;
		case Constants.REQ_GET_EMP_BOOK:
			try {
				int code = data.getInt("responseCode");
				if (code == 1) {
					JSONArray allBook = data.getJSONArray("Data");
					allBookingList.clear();

					for (int i = 0; i < allBook.length(); i++) {
						EmpApptList empAll = new EmpApptList();
						JSONObject book = allBook.getJSONObject(i)
								.getJSONObject("Booking");
						empAll.setBookingId(book.getString("id"));
						empAll.setDate(book.getString("appointment_date"));
						empAll.setEmployeeId(book.getString("employee_id"));
						empAll.setStartTime(book.getString("starttime"));
						empAll.setEndTime(book.getString("endtime"));
						empAll.setSlotId(book.getString("slot_id"));
						empAll.setVisitorId(book.getString("visitor_id"));
						empAll.setCustomerName(book.getString("customer_name"));
						empAll.setCustomerPhone(book.getString("phone_no"));
						JSONObject ob = allBook.getJSONObject(i).getJSONObject(
								"ServiceName");
						empAll.setSvcName(ob.getString("name"));
						JSONObject obj = allBook.getJSONObject(i)
								.getJSONObject("User");
						empAll.setSalonName(obj.getString("business_name"));
						allBookingList.add(empAll);
					}
					listBooking.setAdapter(new ReceptAdapter(this,
							allBookingList));
					if (allBookingList.size() == 0) {
						CommonUtils.showDialog(this,
								"No Appointment avaialable");
					}
				} else {
					CommonUtils.showDialog(this, data.getJSONObject("Message")
							.getString("error"));
				}
			} catch (Exception e) {
				e.printStackTrace();
				CommonUtils.showDialog(this, "Invalid response from server");
			}
			break;

		}
	}

	@SuppressLint("InflateParams")
	private void showEmpDialog() {
		listDialog = new Dialog(this);
		listDialog.setTitle("Select Employee");
		LayoutInflater li = (LayoutInflater) this
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = li.inflate(R.layout.employee_namelist, null, false);
		listDialog.setContentView(v);
		listDialog.setCancelable(true);
		ListView listEmp = (ListView) listDialog.findViewById(R.id.listemp);

		listEmp.setAdapter(new CustomSpinnerAdapter(this, empNameList));
		listEmp.setOnItemClickListener(this);
		listDialog.show();

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		switch (parent.getId()) {
		case R.id.listemp:
			try {
				DataHolder.selectedEmp = empList.get(position - 1);
				Log.v("selectedEmp", DataHolder.selectedEmp.getEmpName());
				listDialog.dismiss();
				spinEmp.setText(DataHolder.selectedEmp.getEmpName());
				List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
				values.add(new BasicNameValuePair("emp_id",
						DataHolder.selectedEmp.getEmpId()));
				values.add(new BasicNameValuePair("recep_id", SalonEmployee
						.getInstance().getEmpId()));
				Log.v("values are", values.toString());
				new CallService(this, this, Constants.REQ_GET_EMP_BOOK, true,
						values).execute(Constants.GET_EMP_BOOK);
			} catch (Exception e) {
				e.printStackTrace();
				listDialog.dismiss();
				spinEmp.setText(empNameList.get(0));
				callservice();
			}
			break;
		}
	}
}
