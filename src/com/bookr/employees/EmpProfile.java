package com.bookr.employees;

import com.bookr.R;
import com.bookr.utils.SalonEmployee;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class EmpProfile extends Activity {
	TextView txtSalonNmae, txtEmpName, txtEmpEmail, txtEmpPhone;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.emp_profile);
		txtSalonNmae = (TextView) findViewById(R.id.txtSalonName);
		txtEmpName = (TextView) findViewById(R.id.txtEmpNmae);
		txtEmpEmail = (TextView) findViewById(R.id.txtEmpEmail);
		txtEmpPhone = (TextView) findViewById(R.id.txtempPhone);

		txtSalonNmae.setText(SalonEmployee.getInstance().getEmpSalonName());
		txtEmpName.setText(SalonEmployee.getInstance().getEmpName());
		txtEmpEmail.setText(SalonEmployee.getInstance().getEmpEmail());
		txtEmpPhone.setText(SalonEmployee.getInstance().getEmpPhone());
	}
}
