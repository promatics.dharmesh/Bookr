package com.bookr.employees;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.bookr.R;
import com.bookr.adapters.EmpApptListAdapter;
import com.bookr.models.EmpApptList;
import com.bookr.utils.CallService;
import com.bookr.utils.CommonUtils;
import com.bookr.utils.Constants;
import com.bookr.utils.SalonEmployee;
import com.bookr.utils.ServiceCallBack;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

@SuppressWarnings("deprecation")
public class EmpAllBook extends Fragment implements ServiceCallBack {
	ListView listApptList;
	ArrayList<EmpApptList> apptList = new ArrayList<EmpApptList>();

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.emp_main_page, container, false);
		listApptList = (ListView) v.findViewById(R.id.listAppts);
		callService();
		return v;
	}

	private void callService() {
		List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
		values.add(new BasicNameValuePair("employee_id", SalonEmployee
				.getInstance().getEmpId()));
		new CallService(this, getActivity(),
				Constants.REQ_GET_EMP_PAST_SCHEDULE, true, values)
				.execute(Constants.GET_EMP_PAST_SCHEDULE);
	}

	@Override
	public void onServiceResponse(int requestCode, JSONObject data) {
		switch (requestCode) {
		case Constants.REQ_GET_EMP_PAST_SCHEDULE:
			try {
				int code = data.getInt("responseCode");
				if (code == 1) {
					JSONArray scheduleArr = data.getJSONArray("Data");
					apptList.clear();
					for (int i = 0; i < scheduleArr.length(); i++) {
						EmpApptList appt = new EmpApptList();
						JSONObject bookobj = scheduleArr.getJSONObject(i)
								.getJSONObject("Booking");
						appt.setStartTime(bookobj.getString("starttime"));
						appt.setEndTime(bookobj.getString("endtime"));
						appt.setDate(bookobj.getString("appointment_date"));
						JSONObject visitor = scheduleArr.getJSONObject(i)
								.getJSONObject("Visitor");
						appt.setCustomerName(visitor.getString("name"));
						JSONObject svc = scheduleArr.getJSONObject(i)
								.getJSONObject("ServiceName");
						appt.setSvcName(svc.getString("name"));
						apptList.add(appt);
					}
					listApptList.setAdapter(new EmpApptListAdapter(
							getActivity(), apptList));
					if (apptList.size() == 0) {
						CommonUtils.showDialog(getActivity(),
								"No past Appointment avaialable");
					}
				} else {
					CommonUtils.showDialog(getActivity(),
							data.getJSONObject("Message").getString("error"));
				}
			} catch (Exception e) {
				e.printStackTrace();
				Toast.makeText(getActivity(), "Error Occurred",
						Toast.LENGTH_LONG).show();
			}

			break;
		}
	}
}
