package com.bookr;

import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.bookr.utils.Constants;
import com.bookr.utils.User;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;

public class MainActivity extends Activity implements OnClickListener {
	Button btnLogin, btnSignUp, btnSkip;
	private SliderLayout mDemoSlider;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sliderscreen);
		if (User.getInstance() != null) {
			startActivity(new Intent(this, Dashboard.class));
			finish();
		}
//		else if (NewUser.getInstance() != null) {
//			startActivity(new Intent(this, Dashboard.class));
//			finish();
//		}

		btnLogin = (Button) findViewById(R.id.btnlogin);
		btnSignUp = (Button) findViewById(R.id.btnsignup);
		btnSkip = (Button) findViewById(R.id.btnskip);
		mDemoSlider = (SliderLayout) findViewById(R.id.slider);
		btnLogin.setOnClickListener(this);
		btnSignUp.setOnClickListener(this);
		btnSkip.setOnClickListener(this);
		HashMap<String, Integer> file_maps = new HashMap<String, Integer>();
		file_maps.put("One", R.drawable.bookmebg);
		file_maps.put("Two", R.drawable.splashbg);
		for (String image : file_maps.keySet()) {
			DefaultSliderView sv = new DefaultSliderView(this);
			sv.image(file_maps.get(image));
			mDemoSlider
					.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
			mDemoSlider.addSlider(sv);
		}
		mDemoSlider.setDuration(3000);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnlogin:
			Intent i = new Intent(this, LoginView.class);
			startActivity(i);
			break;
		case R.id.btnsignup:
			Intent intent = new Intent(this, LoginView.class);
			startActivity(intent);
			break;
		case R.id.btnskip:
			Intent in = new Intent(this, Dashboard.class);
			App.getGlobalPrefs().edit()
					.putBoolean(Constants.KEY_USER_SKIP, true).apply();
			in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
					| Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(in);
			break;

		}
	}

}
