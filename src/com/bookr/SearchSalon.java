package com.bookr;

import static com.bookr.utils.DataHolder.searchSalons;
import static com.bookr.utils.DataHolder.selectedSaloon;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bookr.adapters.SearchedSalonAdapter;
import com.bookr.details.BookFrag;
import com.bookr.details.SaloonDetailActivity;
import com.bookr.gcm.GCMHelper;
import com.bookr.models.Saloon;
import com.bookr.utils.CallService;
import com.bookr.utils.CommonUtils;
import com.bookr.utils.Constants;
import com.bookr.utils.DataHolder;
import com.bookr.utils.ServiceCallBack;
import com.bookr.utils.User;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

@SuppressWarnings("deprecation")
public class SearchSalon extends Activity implements OnClickListener,
		ServiceCallBack, OnItemClickListener {
	EditText etSalonName;
	Button btnSearch;
	ListView listSearchedSalon;
	TextView searchServices, searchSalon, promotedSalon, myProfile, myBookings,
			myReviews, myFavourites, myrewards, myOffers, logout, facebook,
			twitter, instagram, youtube, contactAdmin, aboutBookme, name,
			txtNewSerchSvc, txtNewSerchName, txtNewProm, txtLogin,
			txtAboutBookr, txtContact;
	SlidingMenu menu, newMenu;
	ImageView imgdrawer;
	String salonId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.salon_search);
		if (User.getInstance() != null) {
			initDrawer();
		} else {
			newDrawer();
		}
		etSalonName = (EditText) findViewById(R.id.etSalonName);
		btnSearch = (Button) findViewById(R.id.btnSearch);
		listSearchedSalon = (ListView) findViewById(R.id.listSearchSalon);
		btnSearch.setOnClickListener(this);
		imgdrawer = (ImageView) findViewById(R.id.imgDrawer);
		imgdrawer.setOnClickListener(this);
	}

	@Override
	public void onBackPressed() {
		if (User.getInstance() != null) {
			if (menu.isMenuShowing()) {
				menu.toggle(true);
			} else {
				super.onBackPressed();
			}
		} else
			super.onBackPressed();
	}

	private void initDrawer() {
		menu = new SlidingMenu(this);
		menu.setMode(SlidingMenu.RIGHT);
		menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
		menu.setBehindOffset(100);
		menu.setShadowDrawable(R.drawable.shadow);
		menu.setShadowWidthRes(R.dimen.shadow_width);
		menu.setFadeDegree(0.35f);
		menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
		menu.setMenu(R.layout.drawer);

		searchServices = (TextView) menu.findViewById(R.id.txt_searchServices);
		searchSalon = (TextView) menu.findViewById(R.id.txt_serachSalons);
		promotedSalon = (TextView) menu.findViewById(R.id.txt_promotedSalons);
		myProfile = (TextView) menu.findViewById(R.id.txt_myProfile);
		myBookings = (TextView) menu.findViewById(R.id.txt_myBooking);
		myReviews = (TextView) menu.findViewById(R.id.txt_myReviews);
		myFavourites = (TextView) menu.findViewById(R.id.txtMyfavourites);
		myrewards = (TextView) menu.findViewById(R.id.txtMyrewards);
		myOffers = (TextView) menu.findViewById(R.id.txtMyOffers);
		logout = (TextView) menu.findViewById(R.id.txt_logout);
		// facebook = (TextView) menu.findViewById(R.id.txt_fb);
		// twitter = (TextView) menu.findViewById(R.id.txt_twitter);
		// instagram = (TextView) menu.findViewById(R.id.txt_instagram);
		contactAdmin = (TextView) menu.findViewById(R.id.txt_contactAdmin);
		aboutBookme = (TextView) menu.findViewById(R.id.txt_aboutBookme);
		name = (TextView) menu.findViewById(R.id.txt_username);
		if (User.getInstance().getName() != null) {
			name.setText(User.getInstance().getName());
		}
		searchServices.setOnClickListener(this);
		searchSalon.setOnClickListener(this);
		promotedSalon.setOnClickListener(this);
		myProfile.setOnClickListener(this);
		myBookings.setOnClickListener(this);
		myReviews.setOnClickListener(this);
		myFavourites.setOnClickListener(this);
		myrewards.setOnClickListener(this);
		myOffers.setOnClickListener(this);
		logout.setOnClickListener(this);
		// facebook.setOnClickListener(this);
		// twitter.setOnClickListener(this);
		// instagram.setOnClickListener(this);
		contactAdmin.setOnClickListener(this);
		aboutBookme.setOnClickListener(this);
	}

	private void newDrawer() {
		newMenu = new SlidingMenu(this);
		newMenu.setMode(SlidingMenu.RIGHT);
		newMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
		newMenu.setBehindOffset(100);
		newMenu.setShadowDrawable(R.drawable.shadow);
		newMenu.setShadowWidthRes(R.dimen.shadow_width);
		newMenu.setFadeDegree(0.35f);
		newMenu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
		newMenu.setMenu(R.layout.newdrawer);
		txtNewSerchSvc = (TextView) newMenu
				.findViewById(R.id.txt_searchservices);
		txtNewSerchName = (TextView) newMenu
				.findViewById(R.id.txt_serachsalons);
		txtNewProm = (TextView) newMenu.findViewById(R.id.txt_promotedsalons);
		txtLogin = (TextView) newMenu.findViewById(R.id.txt_login);
		txtContact = (TextView) newMenu.findViewById(R.id.txtContactAdmin);
		txtAboutBookr = (TextView) newMenu.findViewById(R.id.txtAboutBookr);

		txtNewSerchSvc.setOnClickListener(this);
		txtNewSerchName.setOnClickListener(this);
		txtNewProm.setOnClickListener(this);
		txtLogin.setOnClickListener(this);
		txtContact.setOnClickListener(this);
		txtAboutBookr.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.imgDrawer:
			if (User.getInstance() != null) {
				menu.toggle();
			} else {
				newMenu.toggle();
			}
			break;
		case R.id.txt_searchServices:
			Intent services = new Intent(this, Dashboard.class);
			startActivity(services);
			break;
		case R.id.txt_serachSalons:
			// Intent searchSalon = new Intent(this, SearchSalon.class);
			// startActivity(searchSalon);
			menu.toggle();
			break;
		case R.id.txt_promotedSalons:
			Intent promoted = new Intent(this, PromotedSalons.class);
			startActivity(promoted);
			menu.toggle();
			break;
		case R.id.txt_myProfile:
			Intent intent = new Intent(this, MyProfile.class);
			startActivity(intent);
			menu.toggle();
			break;
		case R.id.txt_myBooking:
			Intent i = new Intent(this, MyBookingActivity.class);
			startActivity(i);
			menu.toggle();
			break;
		case R.id.txt_myReviews:
			Intent review = new Intent(this, MyReviewsActivity.class);
			startActivity(review);
			menu.toggle();
			break;
		case R.id.txtMyfavourites:
			Intent MyFav = new Intent(this, MyFavouriteActivity.class);
			startActivity(MyFav);
			menu.toggle();
			break;
		case R.id.txtMyrewards:
			Intent rewards = new Intent(this, MyRewardsActivity.class);
			startActivity(rewards);
			menu.toggle();
			break;
		case R.id.txtMyOffers:
			Intent offers = new Intent(this, MyOffersActivity.class);
			startActivity(offers);
			menu.toggle();
			break;
		case R.id.txt_logout:
			menu.toggle();
			if (!GCMHelper.getSavedRegistrationId().equals("")) {
				GCMHelper.unregister();
			}
			User.getInstance().logout();
			Intent intentLog = new Intent(this, Splash.class);
			intentLog.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_CLEAR_TASK
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intentLog);
			Toast.makeText(this, "You have successfully logged out.",
					Toast.LENGTH_SHORT).show();
			finish();
			break;
		// case R.id.txt_fb:
		// Toast.makeText(this, "This module is pending", Toast.LENGTH_SHORT)
		// .show();
		// break;
		// case R.id.txt_twitter:
		// Toast.makeText(this, "This module is pending", Toast.LENGTH_SHORT)
		// .show();
		// break;
		// case R.id.txt_instagram:
		// Toast.makeText(this, "This module is pending", Toast.LENGTH_SHORT)
		// .show();
		// break;
		case R.id.txt_contactAdmin:
			menu.toggle();
			Intent email = new Intent(this, ContactAdmin.class);
			startActivity(email);
			break;
		case R.id.txt_aboutBookme:
			Intent about = new Intent(this, AboutBookMe.class);
			startActivity(about);
			menu.toggle();
			break;
		case R.id.txt_searchservices:
			Intent service = new Intent(this, Dashboard.class);
			startActivity(service);
			newMenu.toggle();
			break;
		case R.id.txt_serachsalons:
			newMenu.toggle();
			break;
		case R.id.txt_promotedsalons:
			Intent promo = new Intent(this, PromotedSalons.class);
			startActivity(promo);
			newMenu.toggle();
			break;
		case R.id.txt_login:
			Intent login = new Intent(this, LoginView.class);
			startActivity(login);
			newMenu.toggle();
			break;
		case R.id.txtContactAdmin:
			newMenu.toggle();
			Intent emails = new Intent(this, ContactAdmin.class);
			startActivity(emails);
			break;
		case R.id.txtAboutBookr:
			Intent abouts = new Intent(this, AboutBookMe.class);
			startActivity(abouts);
			newMenu.toggle();
			break;

		case R.id.btnSearch:
			if (etSalonName.getText().toString().equals("")) {
				AlertDialog.Builder alert = new Builder(this);
				alert.setMessage("Please enter Salon Name");
				alert.setTitle("Search Salon");
				alert.setPositiveButton("OK", null);
				alert.show();
				return;
			}
			List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
			values.add(new BasicNameValuePair("saloon_name", etSalonName
					.getText().toString()));
			new CallService(this, this, Constants.REQ_GET_SALON_BY_NAME, true,
					values).execute(Constants.GET_SALON_BY_NAME);
			try {
				InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
						0);
			} catch (Exception e) {
			}
			break;
		}
	}

	@Override
	public void onServiceResponse(int requestCode, JSONObject data) {
		switch (requestCode) {
		case Constants.REQ_GET_SALON_BY_NAME:
			try {
				int code = data.getInt("responseCode");
				if (code == 1) {
					JSONArray searchArr = data.getJSONArray("Data");
					searchSalons.clear();
					for (int i = 0; i < searchArr.length(); i++) {
						JSONObject obj = searchArr.getJSONObject(i)
								.getJSONObject("Saloon");
						Saloon searchsaloon = new Saloon();
						searchsaloon.setId(obj.getString("id"));
						searchsaloon.setBusiness_name(obj
								.getString("business_name"));
						searchsaloon.setCity_id(obj.getString("city_id"));
						searchsaloon.setLocation_id(obj
								.getString("location_id"));
						searchsaloon.setAddress(obj.getString("address"));
						searchSalons.add(searchsaloon);
					}
					listSearchedSalon
							.setAdapter(new SearchedSalonAdapter(this));
					if (searchSalons.size() == 0) {
						CommonUtils.showDialog(this, "No Salon found");
					}
					listSearchedSalon.setOnItemClickListener(this);
				} else {
					CommonUtils.showDialog(this,
							"Error occurred!\nPlease try again.");
				}
			} catch (Exception e) {
				e.printStackTrace();
				CommonUtils.showDialog(this, "Invalid response from server.");
			}
			break;
		case Constants.REQ_GET_SALON:
			try {
				int code = data.getInt("responseCode");
				if (code == 1) {
					JSONObject salonDetail = data.getJSONObject("Data")
							.getJSONObject("User");
					selectedSaloon = new Saloon();
					selectedSaloon.setId(salonDetail.getString("id"));
					selectedSaloon.setBusiness_name(salonDetail
							.getString("business_name"));
					selectedSaloon.setAddress(salonDetail.getString("address"));
					selectedSaloon.setAbout_us(salonDetail
							.getString("about_us"));
					selectedSaloon.setCity_id(salonDetail.getString("city_id"));
					selectedSaloon.setLat(salonDetail.getString("lat"));
					selectedSaloon.setLng(salonDetail.getString("lng"));
					selectedSaloon.setReview(salonDetail.getString("review"));
					selectedSaloon.setRating(salonDetail.getString("rating"));
					if (salonDetail.has("favourite_status")) {
						selectedSaloon.setFavourite(salonDetail.getString(
								"favourite_status").equals("1"));
					}
					JSONArray imgArr = data.getJSONObject("Data").getJSONArray(
							"BusinessImage");
					if (imgArr != null && imgArr.length() != 0) {
						String[] imgs = new String[imgArr.length()];
						for (int j = 0; j < imgArr.length(); j++) {
							imgs[j] = imgArr.getJSONObject(j).getString("url");
							if (j == 0) {
								selectedSaloon.setImg(imgs[0]);
							}
							imgs[j] = imgArr.getJSONObject(j).getString("url");
						}
						selectedSaloon.setImgs(imgs);
					}
					DataHolder.saloons.add(selectedSaloon);
					Intent i = new Intent(this, SaloonDetailActivity.class);
					startActivity(i);

				} else {
					CommonUtils.showDialog(this,
							"Error occurred!\nPlease try again.");
				}
			} catch (Exception e) {
				e.printStackTrace();
				CommonUtils.showDialog(this, "Invalid response from server.");
			}
			break;

		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

		BookFrag.searchFlag = true;
		salonId = searchSalons.get(position).getId();
		callservice();
		// startActivity(new Intent(this, SaloonDetailActivity.class).putExtra(
		// "salonId", searchSalons.get(position).getId()));

	}

	private void callservice() {
		List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
		values.add(new BasicNameValuePair("saloon_id", salonId));
		if (User.getInstance() != null) {
			values.add(new BasicNameValuePair("visitor_id", User.getInstance()
					.getUserID()));
		}
		new CallService(this, this, Constants.REQ_GET_SALON, true, values)
				.execute(Constants.GET_SALOON);
	}

}
