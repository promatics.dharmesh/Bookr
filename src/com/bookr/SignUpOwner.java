package com.bookr;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Patterns;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.bookr.R;
import com.bookr.utils.CallService;
import com.bookr.utils.CommonUtils;
import com.bookr.utils.Constants;
import com.bookr.utils.ServiceCallBack;

@SuppressWarnings("deprecation")
public class SignUpOwner extends Activity implements OnClickListener,
		ServiceCallBack {
	EditText owner_name, owner_phone, owner_email, owner_businessname,
			owner_website;
	Button owner_Joinbtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ownersignup);
		owner_name = (EditText) findViewById(R.id.txt_owner_name);
		owner_phone = (EditText) findViewById(R.id.txt_owner_phone);
		owner_email = (EditText) findViewById(R.id.txt_owner_email);
		owner_businessname = (EditText) findViewById(R.id.txt_owner_busineename);
		owner_website = (EditText) findViewById(R.id.txt_owner_website);
		owner_Joinbtn = (Button) findViewById(R.id.btn_signup_owner);
		owner_Joinbtn.setOnClickListener(this);

	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.slide_down);

	}

	@Override
	public void onClick(View v) {
		if (owner_name.getText().toString().equals("")) {
			AlertDialog.Builder alert = new AlertDialog.Builder(
					new ContextThemeWrapper(this,
							android.R.style.Theme_Holo_Light));
			alert.setMessage("Enter your name");
			alert.setTitle("Alert");
			alert.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
						}
					});
			alert.show();
			return;
		}
		if (!owner_phone.getText().toString().matches(Patterns.PHONE.pattern())) {
			AlertDialog.Builder alert = new AlertDialog.Builder(
					new ContextThemeWrapper(this,
							android.R.style.Theme_Holo_Light));
			alert.setMessage("Enter valid Phone number");
			alert.setTitle("Alert");
			alert.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
						}
					});
			alert.show();
			return;
		}
		if (!owner_email.getText().toString()
				.matches(Patterns.EMAIL_ADDRESS.pattern())) {
			AlertDialog.Builder alert = new AlertDialog.Builder(
					new ContextThemeWrapper(this,
							android.R.style.Theme_Holo_Light));
			alert.setMessage("Enter valid email address");
			alert.setTitle("Alert");
			alert.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
						}
					});
			alert.show();
			return;
		}
		if (owner_businessname.getText().toString().equals("")) {
			AlertDialog.Builder alert = new AlertDialog.Builder(
					new ContextThemeWrapper(this,
							android.R.style.Theme_Holo_Light));
			alert.setMessage("Enter your BusinessName");
			alert.setTitle("Alert");
			alert.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
						}
					});
			alert.show();
			return;
		}
		if (!owner_website.getText().toString()
				.matches(Patterns.WEB_URL.pattern())
				&& !owner_website.getText().toString().equals("")) {
			AlertDialog.Builder alert = new AlertDialog.Builder(
					new ContextThemeWrapper(this,
							android.R.style.Theme_Holo_Light));
			alert.setMessage("Enter valid website");
			alert.setTitle("Alert");
			alert.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
						}
					});
			alert.show();
			return;
		}
		try {
			InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
		} catch (Exception e) {
		}
		List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
		values.add(new BasicNameValuePair("name", owner_name.getText()
				.toString()));
		values.add(new BasicNameValuePair("phone_no", owner_phone.getText()
				.toString()));
		values.add(new BasicNameValuePair("email", owner_email.getText()
				.toString()));
		values.add(new BasicNameValuePair("business_name", owner_businessname
				.getText().toString()));
		values.add(new BasicNameValuePair("website", owner_website.getText()
				.toString()));

		new CallService(this, this, Constants.REQ_OWNER_REG, true, values)
				.execute(Constants.OWNER_REGISTRATION);
		owner_name.setText("");
		owner_phone.setText("");
		owner_email.setText("");
		owner_businessname.setText("");
		owner_website.setText("");
	}

	@Override
	public void onServiceResponse(int requestCode, JSONObject data) {
		switch (requestCode) {
		case Constants.REQ_OWNER_REG:
			try {
				int code = data.getInt("responseCode");
				if (code == 1) {
					AlertDialog.Builder alert = new AlertDialog.Builder(
							new ContextThemeWrapper(this,
									android.R.style.Theme_Holo_Light));
					alert.setMessage("Your submission has been received. We will get back to you within 24 hours.");
					alert.setTitle("Thanks");
					alert.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {

								}
							});
					alert.show();

				} else {
					CommonUtils.showDialog(this, data.getJSONObject("Message")
							.getString("error"));
				}
			} catch (Exception e) {
				e.printStackTrace();
				CommonUtils.showDialog(this, "Error Occurred:" + e.getMessage());
			}
			break;
		}

	}
}
