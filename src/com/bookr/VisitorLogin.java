package com.bookr;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.bookr.details.BookingReview;
import com.bookr.models.City;
import com.bookr.models.State;
import com.bookr.utils.CallService;
import com.bookr.utils.CommonUtils;
import com.bookr.utils.Constants;
import com.bookr.utils.DataHolder;
import com.bookr.utils.ServiceCallBack;
import com.bookr.utils.User;

@SuppressWarnings("deprecation")
public class VisitorLogin extends Activity implements OnClickListener,
		ServiceCallBack {
	EditText visitor_email, visitor_password;
	Button visitor_login;
	TextView visitor_signup, visitor_forgot_pass;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.visitorlogin);
		visitor_email = (EditText) findViewById(R.id.txt_visitor_username);
		visitor_password = (EditText) findViewById(R.id.txt_visitor_password);
		visitor_login = (Button) findViewById(R.id.visitor_login_btn);
		visitor_signup = (TextView) findViewById(R.id.txtview_signup);
		visitor_forgot_pass = (TextView) findViewById(R.id.txtview_forgot_password);
		visitor_login.setOnClickListener(this);
		visitor_signup.setOnClickListener(this);
		visitor_forgot_pass.setOnClickListener(this);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.slide_down);

	}

	@Override
	public void onClick(View v) {
		try {
			switch (v.getId()) {
			case R.id.txtview_signup:
				Intent i = new Intent(this, SignUpVisitor.class);
				startActivity(i);
				overridePendingTransition(R.anim.slide_up, 0);
				break;
			case R.id.txtview_forgot_password:
				i = new Intent(this, ForgotPass.class);
				startActivity(i);
				overridePendingTransition(R.anim.slide_up, 0);
				break;
			case R.id.visitor_login_btn:
				if (!visitor_email.getText().toString()
						.matches(Patterns.EMAIL_ADDRESS.pattern())) {
					AlertDialog.Builder alert = new AlertDialog.Builder(
							new ContextThemeWrapper(this,
									android.R.style.Theme_Holo_Light));
					alert.setMessage("Please fill out the valid Email");
					alert.setTitle("Visitor Login");
					alert.setPositiveButton("OK", null);
					alert.show();
					return;
				}
				if (visitor_password.getText().toString().equals("")) {
					AlertDialog.Builder alert = new AlertDialog.Builder(
							new ContextThemeWrapper(this,
									android.R.style.Theme_Holo_Light));
					alert.setMessage("Please Enter Password");
					alert.setTitle("Visitor Login");
					alert.setPositiveButton("OK", null);
					alert.show();
					return;
				}
				InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
						0);

				List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
				values.add(new BasicNameValuePair("email", visitor_email
						.getText().toString()));
				values.add(new BasicNameValuePair("username", visitor_email
						.getText().toString()));
				values.add(new BasicNameValuePair("password", visitor_password
						.getText().toString()));
				new CallService(this, this, Constants.REQ_VISI_LOGIN, true,
						values).execute(Constants.VISITOR_LOGIN);

				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			CommonUtils.showDialog(this, "Error Occurred");
		}
	}

	@Override
	public void onServiceResponse(int requestCode, JSONObject data) {
		switch (requestCode) {
		case Constants.REQ_VISI_LOGIN:
			try {
				int code = data.getInt("responseCode");
				if (code == 1) {
					JSONObject user = data.getJSONObject("Data").getJSONObject(
							"User");
					String userID = user.getString("id");
					String name = user.getString("name");
					String username = user.getString("username");
					String phone = user.getString("phone_no");
					String dob = user.getString("dob");
					String password = user.getString("original_password");
					String locationId = user.getString("location_id");
					String cityId = user.getString("city_id");
					String email = user.getString("email");
					String userType = user.getString("user_type");
					User.storeUser(userID, name, username, phone, dob,
							password, locationId, cityId, email, userType);
					if (!locationId.equals("0")) {
						User.saveState(new State(locationId, data
								.getJSONObject("Data")
								.getJSONObject("Location")
								.getString("location_name")));
						if (!cityId.equals("0")) {
							User.saveCity(new City(cityId, locationId, data
									.getJSONObject("Data")
									.getJSONObject("City").getString("name")));
						}
					}
					if (DataHolder.selectedslot != null) {
						Intent i = new Intent(this, BookingReview.class);
						i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
								| Intent.FLAG_ACTIVITY_CLEAR_TOP
								| Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(i);
						finish();
					} else {
						Intent i = new Intent(this, Dashboard.class);
						i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
								| Intent.FLAG_ACTIVITY_CLEAR_TOP
								| Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(i);
						finish();
					}
				} else {
					CommonUtils.showDialog(this,
							"Your email and password do not match.");
				}
			} catch (Exception e) {
				e.printStackTrace();
				User.getInstance().logout();
				CommonUtils.showDialog(this, "Error Occurred:" + e.getMessage());
			}
			break;
		}
	}
}
