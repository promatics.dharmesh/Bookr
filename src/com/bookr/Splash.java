package com.bookr;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.bookr.employees.EmployeeTabs;
import com.bookr.employees.Receptionist;
import com.bookr.gcm.GCMHelper;
import com.bookr.utils.NewUser;
import com.bookr.utils.SalonEmployee;
import com.bookr.utils.User;

public class Splash extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		if (User.getInstance() != null) {
			startActivity(new Intent(this, Dashboard.class));
			finish();
		} else if (SalonEmployee.getInstance() != null
				&& SalonEmployee.getInstance().getReceptionist().equals("1")) {
			startActivity(new Intent(this, Receptionist.class));
			finish();
		} else if (SalonEmployee.getInstance() != null
				&& SalonEmployee.getInstance().getReceptionist().equals("0")) {
			startActivity(new Intent(this, EmployeeTabs.class));
			finish();
		} else if (SalonEmployee.getInstance() == null
				&& NewUser.getInstance() != null) {
			startActivity(new Intent(this, Dashboard.class));
			finish();
		} else {
			IntentLauncher launcher = new IntentLauncher();
			launcher.start();
		}
	}

	private class IntentLauncher extends Thread {
		public void run() {
			try {
				Log.v("Thread", "sleeping");
				Thread.sleep(3 * 1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
			Intent i = new Intent(Splash.this, Dashboard.class);
			startActivity(i);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (User.getInstance() == null
				&& !GCMHelper.getSavedRegistrationId().isEmpty())
			GCMHelper.unregister();
		if (SalonEmployee.getInstance() == null
				&& !GCMHelper.getSavedRegistrationId().isEmpty()) {
			GCMHelper.unregister();
		}
	}
}