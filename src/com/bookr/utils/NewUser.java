package com.bookr.utils;

import static com.bookr.utils.Constants.KEY_CITYNAME;
import static com.bookr.utils.Constants.KEY_CITY_ID;

import com.bookr.App;

import android.content.SharedPreferences;

public class NewUser {
	private final String CityId;
	private String CityName;
	private SharedPreferences prefs;
	private static NewUser instance = null;

	private NewUser() {
		super();
		prefs = App.getInstance().getSharedPreferences(Constants.KEY_CITY, 0);
		CityId = prefs.getString(KEY_CITY_ID, null);
		CityName = prefs.getString(KEY_CITYNAME, "");
	}

	public static synchronized NewUser getInstance() {
		instance = new NewUser();
		if (instance.CityId != null)
			return instance;
		else
			return null;
	}

	public static void storeNewUser(String CityId, String CityName) {
		SharedPreferences.Editor editor = App.getInstance()
				.getSharedPreferences(Constants.KEY_CITY, 0).edit();
		editor.putString(KEY_CITY_ID, CityId);
		editor.putString(KEY_CITYNAME, CityName);
		editor.commit();
		instance = new NewUser();
	}

	public String getCityName() {
		return CityName;
	}

	public void setCityName(String cityName) {
		CityName = cityName;
	}

	public String getCityId() {
		return CityId;
	}
	
}
