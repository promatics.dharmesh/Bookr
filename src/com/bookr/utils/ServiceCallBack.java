package com.bookr.utils;

import org.json.JSONObject;

public interface ServiceCallBack {

	void onServiceResponse(int requestCode, JSONObject response);

}
