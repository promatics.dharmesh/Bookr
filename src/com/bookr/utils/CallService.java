package com.bookr.utils;

import static com.bookr.utils.CommonUtils.showDialog;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

@SuppressWarnings("deprecation")
public class CallService extends AsyncTask<String, Void, String> {

	ServiceCallBack callback;
	final int requestCode;
	Context ctx;
	List<BasicNameValuePair> data;
	boolean showLoader;
	ProgressDialog dialog;

	private CallService(ServiceCallBack callback, Context ctx, int requestCode,
			boolean showLoader) {
		super();
		this.callback = callback;
		this.ctx = ctx;
		this.requestCode = requestCode;
		this.showLoader = showLoader;
	}

	public CallService(ServiceCallBack callback, Context ctx, int requestCode,
			boolean showLoader, List<BasicNameValuePair> data) {
		this(callback, ctx, requestCode, showLoader);
		this.data = data;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		if (showLoader) {
			dialog = new ProgressDialog(ctx);
			dialog.setMessage("Loading...");
			dialog.setIndeterminate(true);
			dialog.setCancelable(false);
			dialog.show();
		}
	}

	@Override
	protected String doInBackground(String... params) {
		final String url = params[0];
		Log.v("URL", url);
		HttpParams httpParams = new BasicHttpParams();
		HttpConnectionParams
				.setConnectionTimeout(httpParams, Constants.TIMEOUT);
		HttpConnectionParams.setSoTimeout(httpParams, Constants.TIMEOUT);
		DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);
		try {
			HttpPost httpPost = new HttpPost(url);
			httpPost.setParams(httpParams);
			if (data != null) {
				UrlEncodedFormEntity entity = new UrlEncodedFormEntity(data);
				httpPost.setEntity(entity);
			}
			HttpResponse httpResponse = httpClient.execute(httpPost);
			StatusLine statusLine = httpResponse.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == HttpURLConnection.HTTP_OK) {
				HttpEntity httpEntity = httpResponse.getEntity();
				InputStream is = httpEntity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(is, "iso-8859-1"), 8);
				StringBuilder sb = new StringBuilder();
				String line = null;
				while ((line = reader.readLine()) != null) {
					sb.append(line + "\n");
				}
				is.close();
				Log.v("Response", sb.toString());
				return sb.toString();
			} else
				return "Invalid response from server";
		} catch (IOException e) {
			e.printStackTrace();
			return "Unable to reach server.\nPlease check your internet connection and try again.";
		} catch (Exception e) {
			e.printStackTrace();
			return "Error Occured: " + e.getMessage();
		}
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		if (showLoader)
			dialog.dismiss();
		try {
			JSONObject response = new JSONObject(result);
			callback.onServiceResponse(requestCode, response);
		} catch (JSONException e) {
			showDialog(ctx, result);
		} catch (Exception e) {
			e.printStackTrace();
			showDialog(ctx, "Server Error: " + e.getMessage());
		}
	}
}
