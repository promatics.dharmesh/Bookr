package com.bookr.utils;

import static com.bookr.utils.Constants.KEY_CITYID;
import static com.bookr.utils.Constants.KEY_CITY_NAME;
import static com.bookr.utils.Constants.KEY_DOB;
import static com.bookr.utils.Constants.KEY_EMAIL;
import static com.bookr.utils.Constants.KEY_LOCATIONID;
import static com.bookr.utils.Constants.KEY_LOCATION_NAME;
import static com.bookr.utils.Constants.KEY_NAME;
import static com.bookr.utils.Constants.KEY_PASSWORD;
import static com.bookr.utils.Constants.KEY_PHONE;
import static com.bookr.utils.Constants.KEY_UNAME;
import static com.bookr.utils.Constants.KEY_USER_ID;
import static com.bookr.utils.Constants.KEY_USER_TYPE;
import android.content.SharedPreferences;

import com.bookr.App;
import com.bookr.models.City;
import com.bookr.models.State;

public class User {

	private final String userID;
	private String name;
	private String username;
	private String phone;
	private String dob;
	private String password;
	private String locationId;
	private String locationName;
	private String cityId;
	private String cityName;
	private String email;
	private String userType;
	private SharedPreferences prefs;
	private static User instance = null;

	private User() {
		super();
		prefs = App.getInstance().getSharedPreferences(Constants.KEY_PREF, 0);
		userID = prefs.getString(KEY_USER_ID, null);
		name = prefs.getString(KEY_NAME, null);
		username = prefs.getString(KEY_UNAME, null);
		phone = prefs.getString(KEY_PHONE, null);
		dob = prefs.getString(KEY_DOB, null);
		password = prefs.getString(KEY_PASSWORD, null);
		locationId = prefs.getString(KEY_LOCATIONID, null);
		cityId = prefs.getString(KEY_CITYID, null);
		cityName = prefs.getString(KEY_CITY_NAME, "");
		locationName = prefs.getString(KEY_LOCATION_NAME, null);
		email = prefs.getString(KEY_EMAIL, null);
		userType = prefs.getString(KEY_USER_TYPE, null);
	}

	public static synchronized User getInstance() {
		instance = new User();
		if (instance.userID != null)
			return instance;
		else
			return null;
	}

	public static void storeUser(String userID, String name, String username,
			String phone, String dob, String password, String locationId,
			String cityId, String email, String userType) {
		SharedPreferences.Editor editor = App.getInstance()
				.getSharedPreferences(Constants.KEY_PREF, 0).edit();
		editor.putString(KEY_USER_ID, userID);
		editor.putString(KEY_NAME, name);
		editor.putString(KEY_UNAME, username);
		editor.putString(KEY_PHONE, phone);
		editor.putString(KEY_DOB, dob);
		editor.putString(KEY_PASSWORD, password);
		editor.putString(KEY_LOCATIONID, locationId);
		editor.putString(KEY_CITYID, cityId);
		editor.putString(KEY_EMAIL, email);
		editor.putString(KEY_USER_TYPE, userType);
		editor.commit();
		instance = new User();
	}

	public static void saveState(State state) {
		SharedPreferences.Editor editor = App.getInstance()
				.getSharedPreferences(Constants.KEY_PREF, 0).edit();
		editor.putString(KEY_LOCATIONID, state.getId());
		editor.putString(KEY_LOCATION_NAME, state.getLocation_name());
		editor.commit();
		instance = new User();
	}

	public static void saveCity(City city) {
		SharedPreferences.Editor editor = App.getInstance()
				.getSharedPreferences(Constants.KEY_PREF, 0).edit();
		editor.putString(KEY_CITYID, city.getId());
		editor.putString(KEY_CITY_NAME, city.getCityname());
		editor.commit();
		instance = new User();
	}

	public void logout() {
		App.getInstance().getSharedPreferences(Constants.KEY_PREF, 0).edit()
				.clear().commit();
		instance = null;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getUserID() {
		return userID;
	}

}
