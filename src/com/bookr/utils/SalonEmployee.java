package com.bookr.utils;

import static com.bookr.utils.Constants.KEY_EMP_ADDRESS;
import static com.bookr.utils.Constants.KEY_EMP_EMAIL;
import static com.bookr.utils.Constants.KEY_EMP_ID;
import static com.bookr.utils.Constants.KEY_EMP_NAME;
import static com.bookr.utils.Constants.KEY_EMP_PASSWORD;
import static com.bookr.utils.Constants.KEY_EMP_PHONE;
import static com.bookr.utils.Constants.KEY_EMP_SALONID;
import static com.bookr.utils.Constants.KEY_EMP_SALON_NAME;
import static com.bookr.utils.Constants.KEY_EMP_SERVICE_ID;
import static com.bookr.utils.Constants.KEY_EMP_STATUS;
import static com.bookr.utils.Constants.KEY_EMP_SVC_CAT_ID;
import static com.bookr.utils.Constants.KEY_EMP_RECEPTIONIST;
import android.content.SharedPreferences;

import com.bookr.App;

public class SalonEmployee {
	private final String empId;
	private String empName;
	private String empEmail;
	private String empPassword;
	private String empAddress;
	private String empPhone;
	private String empSalonId;
	private String empSalonName;
	private String empSvcId;
	private String empCatId;
	private String empStatus;
	private String receptionist;
	private SharedPreferences prefs;
	private static SalonEmployee instance = null;

	private SalonEmployee() {
		super();
		prefs = App.getInstance().getSharedPreferences(Constants.KEY_PREFE, 0);
		empId = prefs.getString(KEY_EMP_ID, null);
		empName = prefs.getString(KEY_EMP_NAME, null);
		empEmail = prefs.getString(KEY_EMP_EMAIL, null);
		empPassword = prefs.getString(KEY_EMP_PASSWORD, null);
		empAddress = prefs.getString(KEY_EMP_ADDRESS, null);
		empPhone = prefs.getString(KEY_EMP_PHONE, null);
		empSalonId = prefs.getString(KEY_EMP_SALONID, null);
		empSalonName = prefs.getString(KEY_EMP_SALON_NAME, null);
		empSvcId = prefs.getString(KEY_EMP_SERVICE_ID, null);
		empCatId = prefs.getString(KEY_EMP_SVC_CAT_ID, null);
		empStatus = prefs.getString(KEY_EMP_STATUS, null);
		receptionist = prefs.getString(KEY_EMP_RECEPTIONIST, null);

	}

	public static synchronized SalonEmployee getInstance() {
		instance = new SalonEmployee();
		if (instance.empId != null) {
			return instance;
		} else {
			return null;
		}
	}

	public static void storeEmployee(String empId, String empName,
			String empEmail, String empPassword, String empAddress,
			String empPhone, String empSalonId, String empSalonName, String empSvcId,
			String empCatId, String empStatus, String receptionist) {
		SharedPreferences.Editor editor = App.getInstance()
				.getSharedPreferences(Constants.KEY_PREFE, 0).edit();
		editor.putString(KEY_EMP_ID, empId);
		editor.putString(KEY_EMP_NAME, empName);
		editor.putString(KEY_EMP_EMAIL, empEmail);
		editor.putString(KEY_EMP_PASSWORD, empPassword);
		editor.putString(KEY_EMP_ADDRESS, empAddress);
		editor.putString(KEY_EMP_PHONE, empPhone);
		editor.putString(KEY_EMP_SALONID, empSalonId);
		editor.putString(KEY_EMP_SALON_NAME, empSalonName);
		editor.putString(KEY_EMP_SERVICE_ID, empSvcId);
		editor.putString(KEY_EMP_SVC_CAT_ID, empCatId);
		editor.putString(KEY_EMP_STATUS, empStatus);
		editor.putString(KEY_EMP_RECEPTIONIST, receptionist);
		editor.commit();
		instance = new SalonEmployee();
	}

	public void logout() {
		App.getInstance().getSharedPreferences(Constants.KEY_PREFE, 0).edit()
		.clear().commit();
		instance = null;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpEmail() {
		return empEmail;
	}

	public void setEmpEmail(String empEmail) {
		this.empEmail = empEmail;
	}

	public String getEmpPassword() {
		return empPassword;
	}

	public void setEmpPassword(String empPassword) {
		this.empPassword = empPassword;
	}

	public String getEmpAddress() {
		return empAddress;
	}

	public void setEmpAddress(String empAddress) {
		this.empAddress = empAddress;
	}

	public String getEmpPhone() {
		return empPhone;
	}

	public void setEmpPhone(String empPhone) {
		this.empPhone = empPhone;
	}

	public String getEmpSalonId() {
		return empSalonId;
	}

	public void setEmpSalonId(String empSalonId) {
		this.empSalonId = empSalonId;
	}

	public String getEmpSalonName() {
		return empSalonName;
	}

	public void setEmpSalonName(String empSalonName) {
		this.empSalonName = empSalonName;
	}

	public String getEmpSvcId() {
		return empSvcId;
	}

	public void setEmpSvcId(String empSvcId) {
		this.empSvcId = empSvcId;
	}

	public String getEmpCatId() {
		return empCatId;
	}

	public void setEmpCatId(String empCatId) {
		this.empCatId = empCatId;
	}

	public String getEmpStatus() {
		return empStatus;
	}

	public void setEmpStatus(String empStatus) {
		this.empStatus = empStatus;
	}

	public String getEmpId() {
		return empId;
	}

	public String getReceptionist() {
		return receptionist;
	}

	public void setReceptionist(String receptionist) {
		this.receptionist = receptionist;
	}

}
