package com.bookr.utils;

import java.util.ArrayList;

import com.bookr.models.AllEmployee;
import com.bookr.models.BookedSalonList;
import com.bookr.models.Category;
import com.bookr.models.City;
import com.bookr.models.Saloon;
import com.bookr.models.SelectedSlot;
import com.bookr.models.Service;
import com.bookr.models.State;

public class DataHolder {
	public static ArrayList<Category> categories = new ArrayList<Category>();
	public static ArrayList<Service> services = new ArrayList<Service>();
	public static ArrayList<State> states = new ArrayList<State>();
	public static ArrayList<Saloon> saloons = new ArrayList<Saloon>();
	public static ArrayList<Saloon> searchSalons = new ArrayList<Saloon>();
	public static ArrayList<Saloon> promotedSalons = new ArrayList<Saloon>();
	public static SelectedSlot selectedslot;
	public static Category categorySelected;
	public static Service selectedService;
	public static City selectedCity;
	public static Saloon selectedSaloon;
	public static String selectedDate;
	public static BookedSalonList salondetail;
	public static String salonName, newdate;
	public static String notifsalonId;
	public static AllEmployee selectedEmp;
	public static boolean salonList = false;
	public static boolean offerList = false;
	public static boolean check = false;
	public static String empNo;
}
