package com.bookr.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.ContextThemeWrapper;
import android.widget.Toast;

public class CommonUtils {
	public static void showDialog(Context ctx, String msg) {

		AlertDialog.Builder alert = new AlertDialog.Builder(new 
				ContextThemeWrapper(ctx,android.R.style.
						Theme_Holo_Light));
		alert.setMessage(msg);
		alert.setTitle("Bookr!");
		alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
			}
		});
		try {
			alert.show();
		} catch (Exception e) {
			Toast.makeText(ctx.getApplicationContext(), msg, Toast.LENGTH_LONG)
					.show();
		}

	}
}
