package com.bookr.utils;

public class Constants {
	public static final String KEY_CITY = "city";
	public static final String KEY_CITY_ID = "cityid";
	public static final String KEY_CITYNAME = "cityname";

	public static final String KEY_PREF = "settings";
	public static final String KEY_USER_ID = "userid";
	public static final String KEY_NAME = "name";
	public static final String KEY_UNAME = "username";
	public static final String KEY_PHONE = "phone";
	public static final String KEY_DOB = "dob";
	public static final String KEY_PASSWORD = "password";
	public static final String KEY_LOCATIONID = "locationId";
	public static final String KEY_LOCATION_NAME = "locationName";
	public static final String KEY_CITYID = "cityId";
	public static final String KEY_CITY_NAME = "cityName";
	public static final String KEY_EMAIL = "email";
	public static final String KEY_USER_TYPE = "usertype";

	public static final String KEY_PREFE = "employee";
	public static final String KEY_EMP_ID = "empId";
	public static final String KEY_EMP_NAME = "empName";
	public static final String KEY_EMP_EMAIL = "empEmail";
	public static final String KEY_EMP_PASSWORD = "empPass";
	public static final String KEY_EMP_ADDRESS = "empAddress";
	public static final String KEY_EMP_PHONE = "empPhone";
	public static final String KEY_EMP_SALONID = "empSalonId";
	public static final String KEY_EMP_SALON_NAME = "empSalonName";
	public static final String KEY_EMP_STATUS = "empStatus";
	public static final String KEY_EMP_SERVICE_ID = "empSvcID";
	public static final String KEY_EMP_SVC_CAT_ID = "empCatId";
	public static final String KEY_EMP_RECEPTIONIST = "receptionist";

	public static final String KEY_USER_SKIP = "keySkip";

	public static final int TIMEOUT = 10 * 1000;
	public static final String BASE = "http://www.voilabooking.com/mobiles/";
	public static final String VISITOR_REGISTRATION = BASE
			+ "visitorRegistration";
	public static final String VISITOR_NAME = "name";
	public static final String VISITOR_USERNAME = "username";
	public static final String VISITOR_EMAIL = "email";
	public static final String VISITOR_PHONE = "phone";
	public static final String VISITOR_PASSWORD = "password";
	public static final String VISITOR_CONFIRM_PASSWORD = "confirmPassword";
	public static final int REQ_VISI_REG = 100;

	public static final String OWNER_REGISTRATION = BASE + "ownerRegistration";
	public static final String OWNER_USERNAME = "username";
	public static final String OWNER_PHONE = "phone";
	public static final String OWNER_EMAIL = "email";
	public static final String OWNER_BUSINESS_NAME = "businessname";
	public static final String OWNER_WEBSITE = "website";
	public static final int REQ_OWNER_REG = 200;

	public static final String VISITOR_LOGIN = BASE + "login";
	public static final String EMAIL = "email";
	public static final String USERNAME = "username";
	public static final String PASSWORD = "password";
	public static final int REQ_VISI_LOGIN = 300;

	public static final String VISI_FORGOT_PASS = BASE + "forgotPassword";
	public static final String EMAILID = "email";
	public static final int REQ_VISI_FORGOT_PASS = 400;

	public static final String GET_CATEGORIES = BASE + "getCategories";
	public static final int REQ_GET_CAT = 500;

	public static final String GET_SERVICES = BASE + "getServices";
	public static final String CAT_ID = "id";
	public static final int REQ_GET_SERVICES = 550;

	public static final String GET_STATES = BASE + "getStates";
	public static final int REQ_GET_STATES = 600;

	public static final String GET_CITIES = BASE + "getCities";
	public static final String STATE_ID = "id";
	public static final int REQ_GET_CITIES = 700;

	public static final String UPDATE_PROFILE = BASE + "editProfile";
	public static final String Id = "id";
	public static final String name = "name";
	public static final String username = "username";
	public static final String phone = "phone_no";
	public static final int REQ_UPDATE_PROFILE = 800;

	public static final String CHANGE_PASSWORD = BASE + "changePassword";
	public static final String ID = "id";
	public static final String NEW_PASSWORD = "new_password";
	public static final int REQ_CHANGE_PASSWORD = 850;

	public static final String UPDATE_STATE_CITY = BASE + "updateStateCity";
	public static final String USER_ID = "user_id";
	public static final String STATEID = "state_id";
	public static final String CITYID = "city_id";
	public static final int REQ_UPDATE_STATE_CITY = 900;

	public static final String SALON_LIST = BASE + "saloonList";
	public static final String SERVICENAME = "name";
	public static final String CITYId = "city_id";
	public static final int REQ_SALON_LIST = 950;

	public static final String GET_SALOON = BASE + "getSaloon";
	public static final String SALOONID = "saloon_id";
	public static final String VISITORId = "visitor_id";
	public static final int REQ_GET_SALON = 960;

	public static final String GET_STATE_CITY = BASE + "getStateCity";
	public static final int REQ_GET_STATE_CITY = 965;

	public static final String GET_AVAILABILITY = BASE + "todayAvailable";
	public static final String SALOON_ID = "saloon_id";
	public static final String CATID = "cat_id";
	public static final String DATE = "date";
	public static final String TIME = "time";
	public static final int REQ_GET_AVAILABILITY = 50;

	public static final String GET_BOOKINGS = BASE + "bookings";
	public static final String VISITOR_ID = "visitor_id";
	public static final String SALON_ID = "saloon_id";
	public static final String SERVICEID = "service_id";
	public static final String SLOT_ID = "slot_id";
	public static final String APPOINTMENT_DATE = "appointment_date";
	public static final int REQ_GET_BOOKINGS = 60;

	public static final String GET_APPOINTMENT = BASE + "getAppointment";
	public static final String VISITORID = "visitor_id";
	public static final int REQ_GET_APPOINTMENT = 65;

	public static final String CANCEL_APPOINTMENT = BASE + "cancelBooking";
	public static final String BOOKING_ID = "booking_id";
	public static final int REQ_CANCEL_APPOINTMENT = 66;

	public static final String SALON_REVIEW = BASE + "getReviewAndRating";
	public static final String SALONID = "saloon_id";
	public static final int REQ_SALON_REVIEW = 67;

	public static final String SALON_MAP = BASE + "getLatLng";
	public static final String NE_LAT = "north_east_lat";
	public static final String NE_LNG = "north_east_lng";
	public static final String SW_LAT = "south_west_lat";
	public static final String SW_LNG = "south_west_lng";
	public static final int REQ_SALON_MAP = 68;

	public static final String ADD_FAVOURITE = BASE + "addFavourite";
	public static final String VISITOR_Id = "visitor_id";
	public static final String SALOON_Id = "saloon_id";
	public static final int REQ_ADD_FAVOURITE = 69;

	public static final String GET_FAVOURITE = BASE + "getFavouriteByUserId";
	public static final String VISITOR_id = "visitor_id";
	public static final int REQ_GET_FAVOURITE = 70;

	public static final String GET_MOST_BOOKED_SALON = BASE
			+ "getMostBookedSaloonsByEmployee";
	public static final int REQ_GET_MOST_BOOKED_SALON = 71;

	public static final String GET_MOST_RATED_SALON = BASE
			+ "getMostRatedSaloons";
	public static final int REQ_GET_MOST_RATED_SALON = 72;

	public static final String GET_USER_SALON_LIST = BASE + "getUserSaloonList";
	public static final int REQ_GET_USER_SALON_LIST = 73;

	public static final String PROVIDE_REVIEW_AND_RATING = BASE
			+ "provideReviewAndRating";
	public static final int REQ_PROVIDE_REVIEW_AND_RATING = 74;

	public static final String GET_MY_REVIEW = BASE + "getReviewByUserId";
	public static final int REQ_GET_MY_REVIEW = 75;

	public static final String GET_SALON_BY_NAME = BASE + "getSaloonByName";
	public static final int REQ_GET_SALON_BY_NAME = 76;

	public static final String GET_PROMOTED_SALON = BASE
			+ "searchPromotedSaloon";
	public static final int REQ_GET_PROMOTED_SALON = 77;

	public static final String GET_CMS_PAGE = BASE + "getCmsPageById/4";
	public static final int REQ_GET_CMS_PAGE = 78;

	public static final String GET_REWARDS = BASE + "getRewardsById";
	public static final int REQ_GET_REWARDS = 79;

	public static final String CASH_VOUCHER = BASE + "cashVoucher";
	public static final int REQ_GET_CASH_VOUCHER = 80;

	public static final String REGISTER_DEVICE = BASE + "registerDevice";
	public static final int REQ_REG_DEV = 81;

	public static final String UNREGISTER_DEVICE = BASE + "unregisterDevice";
	public static final int REQ_UNREG_DEV = 82;

	/**
	 * Received when a Booking is confirmed.
	 * */
	public static final String ACTION_CONFIRMED = "com.bookr.action.booking_confirmed";
	/**
	 * Received when a Booking is cancelled.
	 * */
	public static final String ACTION_CANCELLED = "com.bookr.action.booking_cancelled";
	/**
	 * A new Promotion is received by salon.
	 * */
	public static final String ACTION_SALON_OFFER = "com.bookr.action.promotion_received";
	/**
	 * An offer added by Admin.
	 * */
	public static final String ACTION_ADMIN_OFFERS = "com.bookr.action.offer_received_admin";
	/**
	 * A new salon is promoted by admin.
	 * */
	public static final String ACTION_ADMIN_PROMOTED_SALON = "com.bookr.action.promoted_salon";
	/**
	 * new booking of employee
	 * */
	public static final String ACTION_NEW_BOOKING_EMPLOYEE = "com.bookr.action.new_booking";
	/**
	 * Add booking of receptionist
	 * */
	public static final String ACTION_NEW_BOOKING_RECETIONIST = "com.bookr.action.new_booking_receptionist";
	/**
	 * cancel booking of employee
	 * */
	public static final String ACTION_CANCEL_BOOKING_EMPLOYEE = "com.bookr.action.cancel_booking";
	/**
	 * cancel booking of receptionist
	 * */
	public static final String ACTION_CANCEL_BOOKING_RECEPTIONIST = "com.bookr.action.cancel_booking_receptionist";

	public static final String DEFAULT_SALONLIST_BY_EMP = BASE
			+ "saloonListByEmployees";
	public static final int REQ_DEFAULT_SALONLIST_BY_EMP = 83;

	public static final String TODAY_AVAILABLE_BY_EMP = BASE
			+ "todayAvailableByEmployee";
	public static final int REQ_TODAY_AVAILABLE_BY_EMP = 84;

	public static final String GET_SOONEST_AVAILABLE_BY_EMP = BASE
			+ "soonestAvailableSaloonByEmployee";
	public static final int REQ_GET_SOONEST_AVAILABLE_BY_EMP = 85;

	public static final String GET_MOST_RATED_SALON_BY_EMP = BASE
			+ "getMostRatedSaloonsByEmployees";

	public static final String GET_MOST_BOOKED_SALON_BY_EMP = BASE
			+ "getMostBookedSaloonsByEmployee";

	public static final String GET_NEAR_ME_BY_EMP = BASE
			+ "searchNearByEmployee";

	public static final String GET_MY_OFFERS = BASE + "myOffers";
	public static final int REQ_GET_MY_OFFERS = 86;

	public static final String GET_SALON_OFFER = BASE + "saloonOffers";
	public static final int REQ_GET_SALON_OFFER = 87;

	public static final String GET_EMP_LOGIN = BASE + "employeeLogin";
	public static final int REQ_GET_EMP_LOGIN = 88;

	public static final String GET_EMP_SCHEDULE = BASE + "employeeSchdules";
	public static final int REQ_GET_EMP_SCHEDULE = 89;

	public static final String GET_EMP_PAST_SCHEDULE = BASE
			+ "employeePastBookings";
	public static final int REQ_GET_EMP_PAST_SCHEDULE = 90;

	public static final String GET_EMP_LIST = BASE + "empList";
	public static final int REQ_GET_EMP_LIST = 91;

	public static final String GET_SALON_ALLBOOKINGS = BASE + "empAllbooking";
	public static final int REQ_GET_SALON_ALLBOOKINGS = 92;

	public static final String GET_EMP_BOOK = BASE + "empAllbookingbyreception";
	public static final int REQ_GET_EMP_BOOK = 93;

}
