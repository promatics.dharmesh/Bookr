package com.bookr.adapters;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bookr.R;
import com.bookr.models.MyReview;

public class MyReviewListAdapter extends BaseAdapter{

	Context ctx;
	List<MyReview> myreviews;

	public MyReviewListAdapter(Context ctx, List<MyReview> myreviews ) {
		super();
		this.ctx = ctx;
		this.myreviews = myreviews;
	}

	@Override
	public int getCount() {
		return myreviews.size();
	}

	@Override
	public Object getItem(int position) {
		return myreviews.get(position);
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@SuppressLint("ViewHolder")
	@Override
	public View getView(int position, View v, ViewGroup parent) {
		TextView txtSalonName, txtReviewTitle, txtDetail;
		RatingBar ratingBar;
			v = LayoutInflater.from(ctx).inflate(R.layout.myreview_list_item,
					parent, false);
			MyReview review = myreviews.get(position);
			txtSalonName=(TextView)v.findViewById(R.id.txtSalonName);
			txtReviewTitle=(TextView)v.findViewById(R.id.txtReviewTitle);
			txtDetail=(TextView)v.findViewById(R.id.txtDetail);
			ratingBar = (RatingBar) v.findViewById(R.id.ratingBar);
			
			txtSalonName.setText(review.getName());
			txtReviewTitle.setText(review.getTitle());
			txtDetail.setText(review.getReview());
			float rating = Float.parseFloat(review.getRating());
			ratingBar.setRating(rating);
		
		return v;
	}


}


