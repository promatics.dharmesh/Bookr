package com.bookr.adapters;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bookr.R;
import com.bookr.models.SalonOffers;

public class SalonOfferListAdapter extends BaseAdapter{
	Context ctx;
	List<SalonOffers> offrs;
	
	public SalonOfferListAdapter(Context ctx, List<SalonOffers> offers) {
		super();
		this.ctx = ctx;
		this.offrs = offers;
	}

	@Override
	public int getCount() {
		return offrs.size();
	}

	@Override
	public Object getItem(int position) {
		return offrs.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@SuppressLint({ "ViewHolder", "SimpleDateFormat" })
	@Override
	public View getView(int position, View v, ViewGroup parent) {
		TextView txtTitle, txtDesc, txtDate;
		//SalonOffers offer = offrs.get(position);
		v = LayoutInflater.from(ctx).inflate(R.layout.salon_offer_listitem, parent, false);
		txtTitle = (TextView)v.findViewById(R.id.txtOffrTitle);
		txtDesc = (TextView)v.findViewById(R.id.txtDscription);
		txtDate = (TextView)v.findViewById(R.id.txtexpDate);
		txtTitle.setText(offrs.get(position).getTitle());
		txtDesc.setText(offrs.get(position).getDescription());
		String date = offrs.get(position).getExpDate();
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(
					"yyyy-MM-dd");
			Date dateobj = sdf.parse(date);
			txtDate.setText(new SimpleDateFormat(
					"dd-MM-yyyy").format(dateobj));
		} catch (final Exception e) {
			e.printStackTrace();
		}
		return v;
	}
}
