package com.bookr.adapters;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bookr.R;
import com.bookr.models.MyOffers;

public class MyOfferListAdapter extends BaseAdapter{
	Context ctx;
	List<MyOffers> offers;

	public MyOfferListAdapter(Context ctx,List<MyOffers> offers){
		super();
		this.ctx = ctx;
		this.offers = offers;
	}

	@Override
	public int getCount() {
		return offers.size();
	}

	@Override
	public Object getItem(int position) {
		return offers.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@SuppressLint({ "ViewHolder", "SimpleDateFormat" })
	@Override
	public View getView(int position, View v, ViewGroup parent) {
		TextView txtTitle, txtDesc, txtDate, txtnameType;
		v = LayoutInflater.from(ctx).inflate(R.layout.myoffer_list_item, parent, false);
		txtnameType =(TextView)v.findViewById(R.id.txtnameType);
		txtTitle = (TextView)v.findViewById(R.id.txtOfferTitle);
		txtDesc = (TextView)v.findViewById(R.id.txtDescription);
		txtDate = (TextView)v.findViewById(R.id.txtExpDate);
		
		MyOffers offer = offers.get(position);
		if(offer.getSalonId() != "0" && !offer.getSalonId().equals("0")){
			txtnameType.setVisibility(View.VISIBLE);
			txtnameType.setText(offer.getBusinessName());
		}
		else{
			txtnameType.setVisibility(View.VISIBLE);
			txtnameType.setText("From Bookr!");
		}
		txtTitle.setText(offer.getTitle());
		txtDesc.setText(offer.getDescription());
		String date = offer.getExpDate();
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(
					"yyyy-MM-dd");
			Date dateobj = sdf.parse(date);
			txtDate.setText(new SimpleDateFormat(
					"dd-MM-yyyy").format(dateobj));
		} catch (final Exception e) {
			e.printStackTrace();
		}
		return v;
	}

}
