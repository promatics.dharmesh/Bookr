package com.bookr.adapters;

import static com.bookr.utils.DataHolder.categories;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.bookr.R;

public class GridAdapter extends BaseAdapter {

	Context ctx;
	AQuery aq;

	public GridAdapter(Context ctx) {
		this.ctx = ctx;
		aq = new AQuery(ctx);
	}

	@Override
	public int getCount() {
		return categories.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View v, ViewGroup parent) {
		ViewHolder holder = new ViewHolder();
		if (v == null) {
			v = LayoutInflater.from(ctx).inflate(R.layout.grid_item, parent,
					false);
			holder.img = (ImageView) v.findViewById(R.id.img);
			holder.txt = (TextView) v.findViewById(R.id.txt);
			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}
		aq.id(holder.img).image(categories.get(position).getImage());
		holder.txt.setText(categories.get(position).getName());
		return v;
	}

	private static class ViewHolder {
		ImageView img;
		TextView txt;
	}
}
