package com.bookr.adapters;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bookr.R;
import com.bookr.models.MyBooking;
import com.bookr.utils.CallService;
import com.bookr.utils.CommonUtils;
import com.bookr.utils.Constants;
import com.bookr.utils.ServiceCallBack;

@SuppressWarnings("deprecation")
public class MyBookingListAdapter extends BaseAdapter implements
		ServiceCallBack {
	Context ctx;
	List<MyBooking> bookings;

	// MyBooking[] mybookings;
	public MyBookingListAdapter(Context ctx, List<MyBooking> books) {
		super();
		this.ctx = ctx;
		this.bookings = books;
		// this.mybookings = mybookings;
	}

	@Override
	public int getCount() {
		return bookings.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@SuppressLint({ "SimpleDateFormat", "ViewHolder" })
	@Override
	public View getView(final int position, View v, ViewGroup parent) {
		TextView txtServiceName, txtSalonName, txtSalonAddress, txtDate, txtTime, txtEmpName;
		Button btnCancel;
		ImageView btnCall;
		final String number;
		final MyBooking book = bookings.get(position);
		v = LayoutInflater.from(ctx).inflate(R.layout.mybooking_listitem,
				parent, false);
		txtServiceName = (TextView) v.findViewById(R.id.txtserviceName);
		txtSalonName = (TextView) v.findViewById(R.id.txtsalonNmae);
		txtSalonAddress = (TextView) v.findViewById(R.id.txtaddress);
		txtDate = (TextView) v.findViewById(R.id.txtshowdate);
		txtTime = (TextView) v.findViewById(R.id.txtshowtime);
		txtEmpName = (TextView) v.findViewById(R.id.txtEmpName);
		number = "tel:" + book.getSalonNo();
		btnCall = (ImageView) v.findViewById(R.id.btnCall);
		btnCancel = (Button) v.findViewById(R.id.btncancel);
		btnCall.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_DIAL);
				intent.setData(Uri.parse(number));
				ctx.startActivity(intent);
			}
		});
		btnCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder alert = new AlertDialog.Builder(
						new ContextThemeWrapper(ctx,
								android.R.style.Theme_Holo_Light));
				alert.setTitle("Cancel Appointment");
				alert.setMessage("Do you want to cancel this appointment?");
				alert.setPositiveButton("No", null);
				alert.setNegativeButton("Yes",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
								values.add(new BasicNameValuePair("booking_id",
										book.getBookingID()));
								Log.v("values are =", values.toString());
								new CallService(MyBookingListAdapter.this, ctx,
										Constants.REQ_CANCEL_APPOINTMENT, true,
										values)
										.execute(Constants.CANCEL_APPOINTMENT);
								bookings.remove(position);
							}
						});
				alert.show();
			}
		});
		txtServiceName.setText(book.getServiceName());
		if (txtServiceName.getText() == "null"
				&& txtServiceName.getText().equals("null")) {
			txtServiceName.setText(book.getEmpName());
		}
		txtSalonName.setText(book.getSalonName());
		txtSalonAddress.setText(book.getSalonAddress());
		txtDate.setText(book.getAppointmentDate());
		txtEmpName.setText(book.getEmpName());
		String date = txtDate.getText().toString();
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date dateobj = sdf.parse(date);
			txtDate.setText(new SimpleDateFormat("dd-MM-yyyy").format(dateobj));
		} catch (final Exception e) {
			e.printStackTrace();
		}
		txtTime.setText(book.getStartTime().substring(0, 5) + "-"
				+ book.getEndTime().substring(0, 5));
		String time = txtTime.getText().toString();
		StringTokenizer tokens = new StringTokenizer(time, "-");
		String first = tokens.nextToken();
		String second = tokens.nextToken();
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
			Date dateObj = sdf.parse(first);
			Date dateObj1 = sdf.parse(second);
			txtTime.setText(new SimpleDateFormat("hh:mm a").format(dateObj)
					+ " - " + new SimpleDateFormat("hh:mm a").format(dateObj1));
		} catch (final Exception e) {
			e.printStackTrace();
		}
		return v;
	}

	@Override
	public void onServiceResponse(int requestCode, JSONObject data) {
		switch (requestCode) {
		case Constants.REQ_CANCEL_APPOINTMENT:
			try {
				int code = data.getInt("responseCode");
				if (code == 1) {
					Toast.makeText(ctx,
							"Yout appointment has been " + "cancelled",
							Toast.LENGTH_LONG).show();
					notifyDataSetChanged();
				} else {
					CommonUtils.showDialog(ctx, data.getJSONObject("Message")
							.getString("error"));
				}
			} catch (Exception e) {
				e.printStackTrace();
				CommonUtils.showDialog(ctx, "Error Occured:" + e.getMessage());
			}
			break;
		}
	}
}
