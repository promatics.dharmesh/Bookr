package com.bookr.adapters;

import static com.bookr.utils.DataHolder.states;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

public class StateCityAdapter extends BaseExpandableListAdapter {

	Context ctx;

	public StateCityAdapter(Context ctx) {
		this.ctx = ctx;
	}

	@Override
	public Object getChild(int arg0, int arg1) {
		return null;
	}

	@Override
	public long getChildId(int arg0, int arg1) {
		return 0;
	}

	@Override
	public View getChildView(int arg0, int arg1, boolean arg2, View v,
			ViewGroup arg4) {
		v = LayoutInflater.from(ctx).inflate(
				android.R.layout.simple_expandable_list_item_1, arg4, false);
		TextView txt = (TextView) v.findViewById(android.R.id.text1);
		txt.setText(states.get(arg0).getCities()[arg1].getCityname());
		txt.setTextColor(Color.BLACK);
		return v;
	}

	@Override
	public int getChildrenCount(int position) {
		try {
			Log.v("size", "" + states.get(position).getCities().length);
			return states.get(position).getCities().length;
		} catch (Exception e) {
			return 0;
		}
	}

	@Override
	public Object getGroup(int arg0) {
		return null;
	}

	@Override
	public int getGroupCount() {
		return states.size();
	}

	@Override
	public long getGroupId(int arg0) {
		return 0;
	}

	@Override
	public View getGroupView(int arg0, boolean arg1, View v, ViewGroup arg3) {
		v = LayoutInflater.from(ctx).inflate(
				android.R.layout.simple_expandable_list_item_1, arg3, false);
		TextView txt = (TextView) v.findViewById(android.R.id.text1);
		txt.setTextColor(Color.BLACK);
		txt.setText(states.get(arg0).getLocation_name());
		txt.setBackgroundColor(Color.LTGRAY);
		return v;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int arg0, int arg1) {
		return true;
	}

}
