package com.bookr.adapters;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bookr.MyRewardsActivity;
import com.bookr.R;
import com.bookr.models.MyReward;
import com.bookr.utils.CallService;
import com.bookr.utils.CommonUtils;
import com.bookr.utils.Constants;
import com.bookr.utils.ServiceCallBack;
import com.bookr.utils.User;

@SuppressWarnings("deprecation")
public class MyRewardListAdapter extends BaseAdapter implements ServiceCallBack {
	Context ctx;
	MyRewardsActivity act;
	List<MyReward> rewards;

	public MyRewardListAdapter(Context ctx, MyRewardsActivity act,
			List<MyReward> myreward) {
		super();
		this.ctx = ctx;
		this.act = act;
		this.rewards = myreward;
	}

	@Override
	public int getCount() {
		return rewards.size();
	}

	@Override
	public Object getItem(int position) {
		return rewards.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@SuppressLint("ViewHolder")
	@Override
	public View getView(final int position, View v, ViewGroup parent) {
		final TextView txtPoints;
		TextView txtrewardDeatil;
		Button btnRedeem;
		v = LayoutInflater.from(ctx).inflate(R.layout.my_rewrads_listitem,
				parent, false);
		final MyReward rew = rewards.get(position);
		txtPoints = (TextView) v.findViewById(R.id.txtpoints);
		txtrewardDeatil = (TextView) v.findViewById(R.id.txtreward);
		btnRedeem = (Button) v.findViewById(R.id.btnRedeem);
		txtPoints.setText(rew.getReward_point() + " Reward Points");
		txtrewardDeatil.setText(rew.getReward_price());
		btnRedeem.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
				values.add(new BasicNameValuePair("visitor_id", User
						.getInstance().getUserID()));
				values.add(new BasicNameValuePair("reward_id", rew
						.getReward_id()));
				new CallService(MyRewardListAdapter.this, ctx,
						Constants.REQ_GET_CASH_VOUCHER, true, values)
						.execute(Constants.CASH_VOUCHER);
			}
		});
		return v;
	}

	@Override
	public void onServiceResponse(int requestCode, JSONObject data) {
		switch (requestCode) {
		case Constants.REQ_GET_CASH_VOUCHER:
			try {
				int code = data.getInt("responseCode");
				if (code == 1) {
					Toast.makeText(ctx, "You have redeemed your reward",
							Toast.LENGTH_LONG).show();
					MyRewardsActivity.txtMypoitns.setText(data
							.getString("total_points"));
					notifyDataSetChanged();
					act.callservice();
				} else {
					CommonUtils.showDialog(ctx, data.getJSONObject("Message")
							.getString("error"));
				}
			} catch (Exception e) {
				e.printStackTrace();
				CommonUtils.showDialog(ctx, "Error Occured:" + e.getMessage());
			}
			break;

		}
	}
}
