package com.bookr.adapters;


import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bookr.R;
import com.bookr.models.SalonReview;
public class SalonReviewListAdapter extends BaseAdapter{
	Context ctx;
	List<SalonReview> salonreview;
	public SalonReviewListAdapter(Context ctx,List<SalonReview> salonreview) {
		super();
		this.ctx = ctx;
		this.salonreview = salonreview;
	}
	@Override
	public int getCount() {
		return salonreview.size();
	}

	@Override
	public Object getItem(int position) {
		return salonreview.get(position);
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@SuppressLint("ViewHolder")
	@Override
	public View getView(int position, View v,  ViewGroup parent) {
		TextView txtUserName, txtTitle, txtDetail;
		RatingBar ratingBar;
		SalonReview review = salonreview.get(position);
		v = LayoutInflater.from(ctx).inflate(R.layout.
				saloon_review_list_item, parent, false);
		txtUserName=(TextView)v.findViewById(R.id.txtUsersName);
		txtTitle=(TextView)v.findViewById(R.id.txtTitle);
		txtDetail=(TextView)v.findViewById(R.id.txtDetail);
		ratingBar=(RatingBar)v.findViewById(R.id.saloonRating);
		txtUserName.setText(review.getUsername());
		txtTitle.setText(review.getTitle());
		txtDetail.setText(review.getReview());
		float ratings = Float.valueOf(review.getRating());
		ratingBar.setRating(ratings);
		return v;
	}
}


