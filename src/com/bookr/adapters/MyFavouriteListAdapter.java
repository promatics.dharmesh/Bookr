package com.bookr.adapters;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.bookr.R;
import com.bookr.models.MyFavourite;
import com.bookr.utils.CallService;
import com.bookr.utils.CommonUtils;
import com.bookr.utils.Constants;
import com.bookr.utils.ServiceCallBack;
import com.bookr.utils.User;

@SuppressWarnings("deprecation")
public class MyFavouriteListAdapter extends BaseAdapter implements
		ServiceCallBack {
	Context ctx;
	AQuery aquery;
	List<MyFavourite> myFavourite;

	public MyFavouriteListAdapter(Context ctx, List<MyFavourite> myFavourite) {
		super();
		this.ctx = ctx;
		aquery = new AQuery(ctx);
		this.myFavourite = myFavourite;

	}

	@Override
	public int getCount() {
		return myFavourite.size();
	}

	@Override
	public Object getItem(int position) {
		return myFavourite.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@SuppressLint("ViewHolder")
	@Override
	public View getView(final int position, View v, ViewGroup parent) {
		v = LayoutInflater.from(ctx).inflate(R.layout.myfavourite_list_item,
				parent, false);
		ImageView imgSalon, imgfav;
		TextView txtSalonName, txtsalonAdress, txtsalonReview;
		RatingBar salonRating;

		final MyFavourite favourite = myFavourite.get(position);
		imgSalon = (ImageView) v.findViewById(R.id.imgsalon);
		imgfav = (ImageView) v.findViewById(R.id.imgFav);
		txtSalonName = (TextView) v.findViewById(R.id.txtSAlonName);
		txtsalonAdress = (TextView) v.findViewById(R.id.txtsalonAddress);
		txtsalonReview = (TextView) v.findViewById(R.id.txtsalonReviews);
		salonRating = (RatingBar) v.findViewById(R.id.saloonRating);
		aquery.id(imgSalon).image(favourite.getImg());
		txtSalonName.setText(favourite.getName());
		txtsalonAdress.setText(favourite.getAddress());
		txtsalonReview.setText(favourite.getReview() + " Reviews");
		float ratings = Float.valueOf(favourite.getRating());
		salonRating.setRating(ratings);
		imgfav.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				AlertDialog.Builder alert = new AlertDialog.Builder(
						new ContextThemeWrapper(ctx,
								android.R.style.Theme_Holo_Light));
				alert.setTitle("Unfavorite Salon");
				alert.setMessage("Do you want to unfavorite this salon?");
				alert.setPositiveButton("No", null);
				alert.setNegativeButton("Yes",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
								values.add(new BasicNameValuePair("visitor_id",
										User.getInstance().getUserID()));
								values.add(new BasicNameValuePair("saloon_id",
										favourite.getId()));
								new CallService(MyFavouriteListAdapter.this,
										ctx, Constants.REQ_ADD_FAVOURITE, true,
										values)
										.execute(Constants.ADD_FAVOURITE);
								myFavourite.remove(position);
							}
						});
				alert.show();
			}
		});
		return v;
	}

	@Override
	public void onServiceResponse(int requestCode, JSONObject data) {
		switch (requestCode) {
		case Constants.REQ_ADD_FAVOURITE:
			try {
				int code = data.getInt("responseCode");
				if (code == 1) {
					Toast.makeText(ctx, "You have unfavorite this" + " salon",
							Toast.LENGTH_LONG).show();
					notifyDataSetChanged();
				} else {
					CommonUtils.showDialog(ctx, data.getJSONObject("Message")
							.getString("error"));
				}
			} catch (Exception e) {
				e.printStackTrace();
				CommonUtils.showDialog(ctx, "Error Occured:" + e.getMessage());
			}
			break;
		}
	}
}
