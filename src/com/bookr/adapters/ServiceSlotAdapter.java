package com.bookr.adapters;

import static com.bookr.utils.DataHolder.selectedslot;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bookr.R;
import com.bookr.VisitorLogin;
import com.bookr.details.BookingReview;
import com.bookr.models.Employee;
import com.bookr.models.SelectedSlot;
import com.bookr.models.TimeSlot;
import com.bookr.utils.DataHolder;
import com.bookr.utils.User;

public class ServiceSlotAdapter extends BaseAdapter {

	Context ctx;
	Employee[] arr;

	public ServiceSlotAdapter(Context ctx, Employee[] arr) {
		super();
		this.ctx = ctx;
		this.arr = arr;
	}

	@Override
	public int getCount() {
		return arr.length;
	}

	@Override
	public Object getItem(int arg0) {
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@SuppressLint({ "ViewHolder", "SimpleDateFormat" })
	@Override
	public View getView(int position, View v, ViewGroup grp) {
		TextView txtService, txtSalonName, txtTime, txtDate;
		ImageView imgCall;
		final String number;
		final Employee ss = arr[position];
		v = LayoutInflater.from(ctx).inflate(R.layout.available_time_listitem,
				grp, false);
		LinearLayout slotgrp = (LinearLayout) v.findViewById(R.id.slotLayout);
		txtService = (TextView) v.findViewById(R.id.txtServices);
		// txtSalonName = (TextView) v.findViewById(R.id.txtSalonname);
		// txtDate = (TextView) v.findViewById(R.id.txtdate);
		// txtTime = (TextView) v.findViewById(R.id.txttime);
		// imgCall = (ImageView) v.findViewById(R.id.imgCall);
		DataHolder.empNo = "tel:" + ss.getEmpPhone();
		txtService.setText(ss.getEmpName());
		// txtSalonName.setText("Salon : "
		// + DataHolder.selectedSaloon.getBusiness_name());
		// txtDate.setText("Date : " + DataHolder.newdate);
		// txtTime.setText("Time :" + ss.getEmpStartTime().substring(0, 5) + "-"
		// + ss.getEmpEndTime().substring(0, 5));
		// String times = txtTime.getText().toString();
		// StringTokenizer tokens = new StringTokenizer(times, "-");
		// String first = ss.getEmpStartTime().substring(0, 5);
		// String second = ss.getEmpEndTime().substring(0, 5);
		// System.out.println(first + second);
		// try {
		// SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		// Date dateObj = sdf.parse(first);
		// Date dateObj1 = sdf.parse(second);
		// txtTime.setText("Time : "
		// + new SimpleDateFormat("hh:mm a").format(dateObj) + " - "
		// + new SimpleDateFormat("hh:mm a").format(dateObj1));
		// } catch (final Exception e) {
		// e.printStackTrace();
		// }
		//imgCall.setOnClickListener(new OnClickListener() {
//			public void onClick(View v) {
//				Intent intent = new Intent(Intent.ACTION_DIAL);
//				intent.setData(Uri.parse(number));
//				ctx.startActivity(intent);
//			}
//		});
		for (int i = 0; i < ss.getSlots().length; i++) {
			LinearLayout.LayoutParams buttonLayoutParams = new LinearLayout.LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			buttonLayoutParams.setMargins(0, 0, 15, 0);
			final Button btn = new Button(v.getContext());
			final TimeSlot slot = ss.getSlots()[i];
			btn.setLayoutParams(buttonLayoutParams);
			btn.setBackgroundColor(Color.WHITE);
			String time = slot.getStartTime().substring(0, 5);
			Log.v("24Hr", time);
			try {
				final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
				final Date dateObj = sdf.parse(time);
				btn.setText(new SimpleDateFormat("hh:mm a").format(dateObj));
			} catch (final Exception e) {
				e.printStackTrace();
			}
			btn.setTextColor(Color.BLACK);
			btn.setTag(position + ":" + i);
			btn.setBackgroundResource(R.drawable.btn_slot);
			btn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					Log.v("btn", btn.getTag().toString());
					if (User.getInstance() != null) {
						Intent i = new Intent(ctx, BookingReview.class);
						selectedslot = new SelectedSlot();
						selectedslot.setStartTime(slot.getStartTime());
						selectedslot.setEndTime(slot.getEndTime());
						selectedslot.setSlotid(slot.getSlotID());
						selectedslot.setEmpId(slot.getEmpId());
						// selectedslot.setServiceid(slot.getServiceID());
						selectedslot.setEmpName(ss.getEmpName());
						System.out.println(ss.getEmpName());
						selectedslot.setSalonName(DataHolder.selectedSaloon
								.getBusiness_name());
						// selectedslot.setServiceName(slot.getServiceName());
						selectedslot.setSalonId(DataHolder.selectedSaloon
								.getId());
						ctx.startActivity(i);
					} else {
						final Dialog dialog = new Dialog(ctx);
						dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
						dialog.setContentView(R.layout.dialog_popup);
						Button btnCancel = (Button) dialog
								.findViewById(R.id.btnCancel);
						Button btnLogin = (Button) dialog
								.findViewById(R.id.btnlogins);
						btnCancel.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								dialog.dismiss();
							}
						});
						btnLogin.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								selectedslot = new SelectedSlot();
								selectedslot.setStartTime(slot.getStartTime());
								selectedslot.setEndTime(slot.getEndTime());
								selectedslot.setSlotid(slot.getSlotID());
								selectedslot.setEmpId(slot.getEmpId());
								selectedslot.setEmpName(ss.getEmpName());
								selectedslot
										.setSalonName(DataHolder.selectedSaloon
												.getBusiness_name());
								selectedslot
										.setSalonId(DataHolder.selectedSaloon
												.getId());
								selectedslot.setEmpNo(DataHolder.empNo);
								ctx.startActivity(new Intent(ctx,
										VisitorLogin.class));
							}

						});
						dialog.show();

					}
				}
			});
			slotgrp.addView(btn);
		}
		return v;
	}
}
