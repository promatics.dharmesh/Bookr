package com.bookr.adapters;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bookr.models.BookedSalonList;

public class BookedSalonListAdapter extends BaseAdapter{
	Context ctx;
	List<BookedSalonList> bookedList;
	
	public BookedSalonListAdapter(Context ctx,List<BookedSalonList> bookedList) {
		super();
		this.ctx = ctx;
		this.bookedList= bookedList;
	}
	@Override
	public int getCount() {
		return bookedList.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@SuppressLint("ViewHolder")
	@Override
	public View getView(int position, View v, ViewGroup parent) {
		v= LayoutInflater.from(ctx).inflate(android.R.layout.simple_list_item_1, 
				parent, false);
		BookedSalonList booked = bookedList.get(position);
		TextView tv = (TextView) v.findViewById(android.R.id.text1);
		tv.setText(booked.getSalonName());
		tv.setTextColor(Color.BLACK);
		tv.setBackgroundColor(Color.WHITE);
		return v;
	}
}
