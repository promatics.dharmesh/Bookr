package com.bookr.adapters;

import static com.bookr.utils.DataHolder.promotedSalons;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.bookr.R;
import com.bookr.models.Saloon;

public class PromotedSalonListAdapter extends BaseAdapter{
	
	Context ctx;
	AQuery aquery;
	
	public PromotedSalonListAdapter(Context ctx) {
		super();
		this.ctx = ctx;
		aquery = new AQuery(ctx);
		
	}
	@Override
	public int getCount() {
		return promotedSalons.size();
	}

	@Override
	public Object getItem(int position) {
		return promotedSalons.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@SuppressLint("ViewHolder")
	@Override
	public View getView(int position, View v, ViewGroup parent) {
		v= LayoutInflater.from(ctx).inflate(R.layout.promoted_salon_list_item, parent, false);
		ImageView img;
		TextView txtSalonName, txtsalonAdress, txtsalonReview;
		RatingBar salonRating;
		Saloon promotdSalon = promotedSalons.get(position);
		img =(ImageView)v.findViewById(R.id.imgPromotedsalon);
		txtSalonName =(TextView)v.findViewById(R.id.txtSAlonName);
		txtsalonAdress=(TextView)v.findViewById(R.id.txtsalonAddress);
		txtsalonReview=(TextView)v.findViewById(R.id.txtsalonReviews);
		salonRating=(RatingBar)v.findViewById(R.id.saloonRating);

		aquery.id(img).image(promotdSalon.getImg());
		txtSalonName.setText(promotdSalon.getBusiness_name());
		txtsalonAdress.setText(promotdSalon.getAddress());
		txtsalonReview.setText(promotdSalon.getReview() + " Reviews");
		float ratings = Float.valueOf(promotdSalon.getRating());
		salonRating.setRating(ratings);
		return v;
	}

}
