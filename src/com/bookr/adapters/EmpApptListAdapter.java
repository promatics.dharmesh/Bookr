package com.bookr.adapters;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bookr.R;
import com.bookr.models.EmpApptList;

public class EmpApptListAdapter extends BaseAdapter {
	Context ctx;
	List<EmpApptList> apptList;

	public EmpApptListAdapter(Context ctx, ArrayList<EmpApptList> apptList) {
		super();
		this.ctx = ctx;
		this.apptList = apptList;
	}

	@Override
	public int getCount() {
		return apptList.size();
	}

	@Override
	public Object getItem(int position) {
		return apptList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@SuppressLint({ "ViewHolder", "SimpleDateFormat" })
	@Override
	public View getView(int position, View v, ViewGroup parent) {
		v = LayoutInflater.from(ctx).inflate(R.layout.emp_apts_listitem,
				parent, false);
		TextView txtTime, txtDate, txtSvc, txtName;
		txtDate = (TextView) v.findViewById(R.id.txtdate);
		txtTime = (TextView) v.findViewById(R.id.txtTime);
		txtSvc = (TextView) v.findViewById(R.id.txtName);
		txtName = (TextView) v.findViewById(R.id.txtNameCustomer);
		EmpApptList appt = apptList.get(position);
		txtDate.setText(appt.getDate());
		String date = txtDate.getText().toString();
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date dateobj = sdf.parse(date);
			txtDate.setText(new SimpleDateFormat("dd-MM-yyyy").format(dateobj));
		} catch (final Exception e) {
			e.printStackTrace();
		}
		txtTime.setText(appt.getStartTime().substring(0, 5) + "-"
				+ appt.getEndTime().substring(0, 5));
		String time = txtTime.getText().toString();
		StringTokenizer tokens = new StringTokenizer(time, "-");
		String first = tokens.nextToken();
		String second = tokens.nextToken();
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
			Date dateObj = sdf.parse(first);
			Date dateObj1 = sdf.parse(second);
			txtTime.setText(new SimpleDateFormat("hh:mm a").format(dateObj)
					+ "-" + new SimpleDateFormat("hh:mm a").format(dateObj1));
		} catch (final Exception e) {
			e.printStackTrace();
		}
		txtSvc.setText(appt.getSvcName());
		txtName.setText(appt.getCustomerName());
		return v;
	}

}
