package com.bookr.adapters;

import static com.bookr.utils.DataHolder.services;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ServiceAdapter extends BaseAdapter {

	Context ctx;

	public ServiceAdapter(Context ctx) {
		this.ctx = ctx;
	}

	@Override
	public int getCount() {
		return services.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@SuppressLint("ViewHolder")
	@Override
	public View getView(int position, View v, ViewGroup parent) {
		v = LayoutInflater.from(ctx).inflate(
				android.R.layout.simple_list_item_1, parent, false);
		TextView tv = (TextView) v.findViewById(android.R.id.text1);
		tv.setText(services.get(position).getName());
		tv.setTextColor(Color.BLACK);
		return v;
	}
}
