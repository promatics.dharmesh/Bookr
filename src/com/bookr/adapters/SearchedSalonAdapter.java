package com.bookr.adapters;

import static com.bookr.utils.DataHolder.searchSalons;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bookr.R;

public class SearchedSalonAdapter extends BaseAdapter{

	Context ctx;

	public SearchedSalonAdapter(Context ctx) {
		this.ctx = ctx;
	}
	@Override
	public int getCount() {
		return searchSalons.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@SuppressLint("ViewHolder")
	@Override
	public View getView(int position, View v, ViewGroup parent) {
		v = LayoutInflater.from(ctx).inflate(R.layout.searched_salon_list_item, parent, false);
		TextView tv1 = (TextView)v.findViewById(R.id.txtsalonName);
		TextView tv2 = (TextView)v.findViewById(R.id.txtssalonAddress);
		tv1.setText(searchSalons.get(position).getBusiness_name());
		tv2.setText(searchSalons.get(position).getAddress());
		return v;
	}
}
