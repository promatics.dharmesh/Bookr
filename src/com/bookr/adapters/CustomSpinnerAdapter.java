package com.bookr.adapters;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bookr.R;

public class CustomSpinnerAdapter extends ArrayAdapter<String> {
	List<String> arrayList;
	Context ctx;
	TextView txt;
	LayoutInflater inflater;

	public CustomSpinnerAdapter(Context context, ArrayList<String> objects) {
		super(context, android.R.layout.simple_spinner_dropdown_item, objects);
		ctx = context;
		arrayList = objects;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return arrayList.size();
	}

	@Override
	public View getView(int position, View v, ViewGroup parent) {
		return getCustomView(position, v, parent);
	}

	public View getCustomView(int position, View v, ViewGroup parent) {
		if (v == null) {
			v = inflater.inflate(R.layout.spinner_row_item, parent, false);
		}
		txt = (TextView) v.findViewById(R.id.text1);
		txt.setText(arrayList.get(position));
		return v;
	}

	@Override
	public View getDropDownView(int position, View v, ViewGroup parent) {
		return getCustomView(position, v, parent);
	}
}