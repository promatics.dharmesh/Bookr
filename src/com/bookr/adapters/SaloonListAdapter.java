package com.bookr.adapters;

import static com.bookr.utils.DataHolder.saloons;
import static com.bookr.utils.DataHolder.selectedSaloon;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.bookr.R;
import com.bookr.details.BookFrag;
import com.bookr.details.SaloonDetailActivity;
import com.bookr.models.Saloon;
import com.bookr.utils.CallService;
import com.bookr.utils.CommonUtils;
import com.bookr.utils.Constants;
import com.bookr.utils.DataHolder;
import com.bookr.utils.ServiceCallBack;
import com.bookr.utils.User;

@SuppressWarnings("deprecation")
public class SaloonListAdapter extends BaseAdapter implements ServiceCallBack {

	Context ctx;
	AQuery aq;
	String salonId;

	public SaloonListAdapter(Context ctx) {
		super();
		this.ctx = ctx;
		aq = new AQuery(ctx);
	}

	@Override
	public int getCount() {
		return saloons.size();
	}

	@Override
	public Saloon getItem(int position) {
		return saloons.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@SuppressLint({ "SimpleDateFormat", "ViewHolder", "InflateParams" })
	@Override
	public View getView(final int position, View v, ViewGroup parent) {
		v = LayoutInflater.from(ctx).inflate(R.layout.saloon_list_item, parent,
				false);
		final Saloon salon = saloons.get(position);
		LinearLayout base = (LinearLayout) v.findViewById(R.id.base);
		TextView txtName, txtAddr, txtDistance, txtPromo, txtReviews;
		ImageView img;
		RatingBar ratingBar;
		RelativeLayout rel;
		img = (ImageView) v.findViewById(R.id.img);
		txtName = (TextView) v.findViewById(R.id.txtName);
		txtAddr = (TextView) v.findViewById(R.id.txtAddress);
		txtDistance = (TextView) v.findViewById(R.id.txtDistance);
		txtPromo = (TextView) v.findViewById(R.id.txtRecomended);
		txtReviews = (TextView) v.findViewById(R.id.txtReviews);
		ratingBar = (RatingBar) v.findViewById(R.id.saloonRating);
		// rel = (RelativeLayout) v.findViewById(R.id.layRELATIVE);
		aq.id(img).image(salon.getImg());
		if (salon.getPromo() != null) {
			if (!salon.getPromo().equals("null")) {
				txtPromo.setVisibility(View.VISIBLE);
			} else {
				txtPromo.setVisibility(View.GONE);
			}
		}
		txtName.setText(salon.getBusiness_name());
		txtAddr.setText(salon.getAddress());
		if (salon.getDistance() != null && !salon.getDistance().equals("")) {
			txtDistance.setVisibility(View.VISIBLE);
			txtDistance.setText(salon.getDistance().toString());
			System.out.println(txtDistance.getText());
			DecimalFormat df = new DecimalFormat();
			df.setMaximumFractionDigits(3);
			txtDistance.setText(df.format(Double.valueOf(txtDistance.getText()
					.toString())) + " Miles");
			System.out.println(txtDistance.getText());
		}
		float ratings = Float.valueOf(salon.getRating());
		ratingBar.setRating(ratings);
		img.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				BookFrag.searchFlag = false;
				salonId = saloons.get(position).getId();
				callservice();

			}

			private void callservice() {
				List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
				values.add(new BasicNameValuePair("saloon_id", salonId));
				if (User.getInstance() != null) {
					values.add(new BasicNameValuePair("visitor_id", User
							.getInstance().getUserID()));
				}
				new CallService(SaloonListAdapter.this, ctx,
						Constants.REQ_GET_SALON, true, values)
						.execute(Constants.GET_SALOON);
			}
		});
		// img.setOnClickListener(new OnClickListener() {
		// @Override
		// public void onClick(View v) {
		// BookFrag.searchFlag = false;
		// Intent i = new Intent(ctx, SaloonDetailActivity.class)
		// .putExtra("salonId", saloons.get(position).getId());
		// ctx.startActivity(i);
		// }
		// });
		// View child = LayoutInflater.from(ctx).inflate(R.layout.ss_list_item,
		// null, false);
		// LinearLayout slotGrp = (LinearLayout) child
		// .findViewById(R.id.slotLayout);
		// for (int i = 0; i < salon.getSlots().length; i++) {
		// LinearLayout.LayoutParams buttonLayoutParams = new
		// LinearLayout.LayoutParams(
		// android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
		// android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
		// buttonLayoutParams.setMargins(0, 10, 15, 0);
		// final Button btn = new Button(v.getContext());
		// final TimeSlot slot = salon.getSlots()[i];
		// btn.setLayoutParams(buttonLayoutParams);
		// btn.setBackgroundColor(Color.BLACK);
		// String time = slot.getStartTime().substring(0, 5);
		// Log.v("24Hr", time);
		// try {
		// final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
		// final Date dateObj = sdf.parse(time);
		// btn.setText(new SimpleDateFormat("hh:mm a").format(dateObj));
		// } catch (final Exception e) {
		// e.printStackTrace();
		// }
		// btn.setTextColor(Color.WHITE);
		// btn.setTag(position + ":" + i);
		// btn.setOnClickListener(new OnClickListener() {
		// @Override
		// public void onClick(View v) {
		// if (User.getInstance() != null) {
		// Intent intent = new Intent(ctx, BookingReview.class);
		// SelectedSlot select = new SelectedSlot();
		// select.setStartTime(slot.getStartTime());
		// select.setEndTime(slot.getEndTime());
		// select.setSlotid(slot.getSlotID());
		// select.setSalonId(salon.getId());
		// select.setEmpId(slot.getEmpId());
		// select.setSalonName(salon.getBusiness_name());
		// DataHolder.selectedslot = select;
		// ctx.startActivity(intent);
		// } else {
		// final Dialog dialog = new Dialog(ctx);
		// dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// dialog.setContentView(R.layout.dialog_popup);
		// Button btnCancel = (Button) dialog
		// .findViewById(R.id.btnCancel);
		// Button btnLogin = (Button) dialog
		// .findViewById(R.id.btnlogins);
		// btnCancel.setOnClickListener(new OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// dialog.dismiss();
		// }
		// });
		// btnLogin.setOnClickListener(new OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// SelectedSlot select = new SelectedSlot();
		// select.setStartTime(slot.getStartTime());
		// select.setEndTime(slot.getEndTime());
		// select.setSlotid(slot.getSlotID());
		// select.setSalonId(salon.getId());
		// select.setEmpId(slot.getEmpId());
		// select.setSalonName(salon.getBusiness_name());
		// DataHolder.selectedslot = select;
		// ctx.startActivity(new Intent(ctx,
		// VisitorLogin.class));
		// }
		// });
		// dialog.show();
		// }
		//
		// }
		// });
		// slotGrp.addView(btn);
		// }
		// base.addView(child);
		return v;
	}

	@Override
	public void onServiceResponse(int requestCode, JSONObject data) {
		switch (requestCode) {
		case Constants.REQ_GET_SALON:
			try {
				int code = data.getInt("responseCode");
				if (code == 1) {
					JSONObject salonDetail = data.getJSONObject("Data")
							.getJSONObject("User");
					selectedSaloon = new Saloon();
					selectedSaloon.setId(salonDetail.getString("id"));
					selectedSaloon.setBusiness_name(salonDetail
							.getString("business_name"));
					// SaloonDetailActivity.title.setText(selectedSaloon
					// .getBusiness_name());
					selectedSaloon.setAddress(salonDetail.getString("address"));
					selectedSaloon.setAbout_us(salonDetail
							.getString("about_us"));
					selectedSaloon.setCity_id(salonDetail.getString("city_id"));
					selectedSaloon.setLat(salonDetail.getString("lat"));
					selectedSaloon.setLng(salonDetail.getString("lng"));
					selectedSaloon.setReview(salonDetail.getString("review"));
					selectedSaloon.setRating(salonDetail.getString("rating"));
					if (salonDetail.has("favourite_status")) {
						selectedSaloon.setFavourite(salonDetail.getString(
								"favourite_status").equals("1"));
					}
					JSONArray imgArr = data.getJSONObject("Data").getJSONArray(
							"BusinessImage");
					if (imgArr != null && imgArr.length() != 0) {
						String[] imgs = new String[imgArr.length()];
						for (int j = 0; j < imgArr.length(); j++) {
							imgs[j] = imgArr.getJSONObject(j).getString("url");
							if (j == 0) {
								selectedSaloon.setImg(imgs[0]);
							}
							imgs[j] = imgArr.getJSONObject(j).getString("url");
						}
						selectedSaloon.setImgs(imgs);
					}
					DataHolder.saloons.add(selectedSaloon);
					// DataHolder.salonList = true;
					Intent i = new Intent(ctx, SaloonDetailActivity.class);
					ctx.startActivity(i);

				} else {
					CommonUtils.showDialog(ctx,
							"Error occured!\nPlease try again.");
				}
			} catch (Exception e) {
				e.printStackTrace();
				CommonUtils.showDialog(ctx, "Invalid response from server.");
			}
			break;
		}
	}
}
