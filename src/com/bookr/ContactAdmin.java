package com.bookr;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ContactAdmin extends Activity implements OnClickListener {
	EditText subject_email, query_email;
	TextView emailTo;
	Button send, cancel;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contact_admin);
		emailTo = (TextView) findViewById(R.id.et_conatctTo);
		subject_email = (EditText) findViewById(R.id.et_conatctSubject);
		query_email = (EditText) findViewById(R.id.et_conatct_sendQuery);
		send = (Button) findViewById(R.id.btn_send_admin);
		cancel = (Button) findViewById(R.id.btn_cancel_admin);
		send.setOnClickListener(this);
		cancel.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_send_admin:
			if (subject_email.getText().toString().equals("")) {
				AlertDialog.Builder alert = new AlertDialog.Builder(
						new ContextThemeWrapper(this,
								android.R.style.Theme_Holo_Light));
				alert.setMessage("Subject should not be empty");
				alert.setTitle("Alert");
				alert.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
							}
						});
				alert.show();
				return;
			}

			Intent email = new Intent(Intent.ACTION_SEND);
			email.putExtra(Intent.EXTRA_EMAIL,
					new String[] { "info@letsbookr.com" });
			email.putExtra(Intent.EXTRA_SUBJECT, subject_email.getText()
					.toString());
			email.putExtra(Intent.EXTRA_TEXT, query_email.getText().toString());
			email.setType("message/rfc822");
			startActivity(Intent.createChooser(email,
					"Choose an Email client :"));
			subject_email.setText("");
			query_email.setText("");
			break;
		case R.id.btn_cancel_admin:
			finish();
		}
		try {
			InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
		} catch (Exception e) {
		}
	}

}
