package com.bookr;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ExpandableListView;

import com.bookr.adapters.StateCityAdapter;
import com.bookr.models.City;
import com.bookr.models.State;
import com.bookr.utils.CallService;
import com.bookr.utils.CommonUtils;
import com.bookr.utils.Constants;
import com.bookr.utils.DataHolder;
import com.bookr.utils.NewUser;
import com.bookr.utils.ServiceCallBack;
import com.bookr.utils.User;

@SuppressWarnings("deprecation")
public class SelectStateCity extends Activity implements ServiceCallBack,
		ExpandableListView.OnChildClickListener {
	ExpandableListView stateCityList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.select_state);
		stateCityList = (ExpandableListView) findViewById(R.id.ListState);
		stateCityList.setOnChildClickListener(this);
		try {
			InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
		} catch (Exception e) {
		}
		new CallService(this, this, Constants.REQ_GET_STATE_CITY, true, null)
				.execute(Constants.GET_STATE_CITY);
	}

	@Override
	public void onServiceResponse(int requestCode, JSONObject data) {
		try {
			switch (requestCode) {
			case Constants.REQ_GET_STATE_CITY: {
				int code = data.getInt("responseCode");
				if (code == 1) {
					JSONArray state = data.getJSONArray("Data");
					DataHolder.states.clear();
					for (int i = 0; i < state.length(); i++) {
						JSONObject servicestate = state.getJSONObject(i)
								.getJSONObject("Location");
						State statesvc = new State();
						statesvc.setId(servicestate.getString("id"));
						statesvc.setLocation_name(servicestate
								.getString("location_name"));
						statesvc.setLocation_address(servicestate
								.getString("location_address"));
						statesvc.setStatus(servicestate.getBoolean("status"));
						JSONArray cities = state.getJSONObject(i).getJSONArray(
								"City");
						if (cities != null && cities.length() > 0) {
							City[] cts = new City[cities.length()];
							for (int j = 0; j < cities.length(); j++) {
								JSONObject serviceCity = cities
										.getJSONObject(j);
								City citysvc = new City();
								citysvc.setId(serviceCity.getString("id"));
								citysvc.setCityname(serviceCity
										.getString("name"));
								citysvc.setLoaction_id(serviceCity
										.getString("location_id"));
								citysvc.setStatus(serviceCity
										.getString("status"));
								cts[j] = citysvc;
							}
							Log.v("add city", statesvc.getLocation_name()
									+ " : " + cts.length);
							statesvc.setCities(cts);
						} else {
							Log.e("no city", "no city");
							statesvc.setCities(new City[0]);
						}
						DataHolder.states.add(statesvc);
					}
					stateCityList.setAdapter(new StateCityAdapter(this));
					// stateCityList.setOnItemClickListener(this);
				} else {
					CommonUtils.showDialog(this, "error");
				}
				break;
			}

			case Constants.REQ_UPDATE_STATE_CITY:
				int code = data.getInt("responseCode");
				if (code == 1) {
					AlertDialog.Builder alert = new Builder(this);
					alert.setMessage("City updated Successfully");
					alert.setTitle("Alert");
					alert.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									DataHolder.check = true;
									// SalonsFragment.txtCity.setText(User.getInstance().getCityName());
									finish();
								}
							});
					alert.show();
				} else {
					CommonUtils.showDialog(this, data.getJSONObject("Message")
							.getString("error"));
				}
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			CommonUtils.showDialog(this, "Error Occurred:" + e.getMessage());
		}
	}

	@Override
	public boolean onChildClick(ExpandableListView exlv, View arg1, int grpPos,
			int childPos, long arg4) {
		if (User.getInstance() != null) {
			City city = DataHolder.states.get(grpPos).getCities()[childPos];
			System.out.println(city);
			User.saveCity(city);
			User.saveState(DataHolder.states.get(grpPos));
			List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
			if (User.getInstance() != null) {
				values.add(new BasicNameValuePair("user_id", User.getInstance()
						.getUserID()));
				values.add(new BasicNameValuePair("state_id", User
						.getInstance().getLocationId()));
				values.add(new BasicNameValuePair("city_id", User.getInstance()
						.getCityId()));
			}
			new CallService(this, this, Constants.REQ_UPDATE_STATE_CITY, true,
					values).execute(Constants.UPDATE_STATE_CITY);
		} else {
			DataHolder.selectedCity = DataHolder.states.get(grpPos).getCities()[childPos];
			System.out.println(DataHolder.selectedCity.getId()
					+ DataHolder.selectedCity.getCityname());
			String cityId = DataHolder.selectedCity.getId();
			String cityName = DataHolder.selectedCity.getCityname();
			NewUser.storeNewUser(cityId, cityName);
			AlertDialog.Builder alert = new Builder(this);
			alert.setMessage("City updated Successfully");
			alert.setTitle("Alert");
			alert.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							DataHolder.check = true;
							finish();
						}
					});
			alert.show();
		}
		return true;
	}
}
