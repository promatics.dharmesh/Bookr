package com.bookr;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.bookr.R;
import com.bookr.adapters.BookedSalonListAdapter;
import com.bookr.models.BookedSalonList;
import com.bookr.utils.CallService;
import com.bookr.utils.Constants;
import com.bookr.utils.DataHolder;
import com.bookr.utils.ServiceCallBack;
import com.bookr.utils.User;

@SuppressWarnings("deprecation")
public class BookedSalonListActivity extends Activity implements
		ServiceCallBack, OnItemClickListener {
	ListView listBookedSalon;
	ArrayList<BookedSalonList> bookedSalon = new ArrayList<BookedSalonList>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.booked_salon_list);
		listBookedSalon = (ListView) findViewById(R.id.listsalon);

	}

	public void callService() {
		List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
		values.add(new BasicNameValuePair("visitor_id", User.getInstance()
				.getUserID()));
		new CallService(this, this, Constants.REQ_GET_USER_SALON_LIST, true,
				values).execute(Constants.GET_USER_SALON_LIST);
	}

	@Override
	protected void onResume() {
		super.onResume();
		callService();
	}

	@Override
	public void onServiceResponse(int requestCode, JSONObject data) {
		switch (requestCode) {
		case Constants.REQ_GET_USER_SALON_LIST:
			try {
				int code = data.getInt("responseCode");
				if (code == 1) {
					JSONArray bookSalonArr = data.getJSONArray("Data");
					bookedSalon.clear();
					for (int i = 0; i < bookSalonArr.length(); i++) {
						BookedSalonList booked = new BookedSalonList();
						JSONObject bookobj = bookSalonArr.getJSONObject(i)
								.getJSONObject("Booking");
						booked.setBookingId(bookobj.getString("id"));
						JSONObject salonObj = bookSalonArr.getJSONObject(i)
								.getJSONObject("Saloon");
						booked.setSalonId(salonObj.getString("id"));
						booked.setSalonName(salonObj.getString("business_name"));
						bookedSalon.add(booked);
					}
					listBookedSalon.setAdapter(new BookedSalonListAdapter(this,
							bookedSalon));
					if (bookedSalon.size() == 0) {
						AlertDialog.Builder alert = new AlertDialog.Builder(
								new ContextThemeWrapper(this,
										android.R.style.Theme_Holo_Light));
						alert.setTitle("Salons");
						alert.setMessage("No Salon Available for review");
						alert.setPositiveButton("Ok",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										finish();
									}
								});
						alert.show();
					} else {
						listBookedSalon.setOnItemClickListener(this);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				Toast.makeText(this, "Error Occurred", Toast.LENGTH_LONG).show();
			}
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		DataHolder.salondetail = bookedSalon.get(position);
		startActivity(new Intent(this, AddReview.class));
	}
}
