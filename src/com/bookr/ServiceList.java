package com.bookr;

import static com.bookr.utils.DataHolder.categorySelected;
import static com.bookr.utils.DataHolder.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.bookr.R;
import com.bookr.adapters.ServiceAdapter;
import com.bookr.models.Service;
import com.bookr.salonlist.SalonListActivity;
import com.bookr.utils.CallService;
import com.bookr.utils.CommonUtils;
import com.bookr.utils.Constants;
import com.bookr.utils.DataHolder;
import com.bookr.utils.ServiceCallBack;

@SuppressWarnings("deprecation")
public class ServiceList extends Activity implements ServiceCallBack,
		OnItemClickListener {

	TextView txtService;
	ListView listView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.selectedservicelist);
		txtService = (TextView) findViewById(R.id.txtService);
		txtService.setText(categorySelected.getName());
		listView = (ListView) findViewById(R.id.listServiceSelect);

		List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
		values.add(new BasicNameValuePair("cat_id", categorySelected.getId()));
		Log.d("values are", values.toString());
		new CallService(this, this, Constants.REQ_GET_SERVICES, true, values)
				.execute(Constants.GET_SERVICES);
	}

	@Override
	public void onServiceResponse(int requestCode, JSONObject data) {
		try {
			int code = data.getInt("responseCode");
			if (code == 1) {
				JSONArray servicearr = data.getJSONArray("Data");
				DataHolder.services.clear();
				for (int i = 0; i < servicearr.length(); i++) {
					JSONObject service = servicearr.getJSONObject(i)
							.getJSONObject("ServiceName");
					Service catsvc = new Service();
					catsvc.setId(service.getString("id"));
					catsvc.setName(service.getString("name"));

					DataHolder.services.add(catsvc);
				}
				Log.v("svs", "total no=" + services.size());
				listView.setAdapter(new ServiceAdapter(this));
				listView.setOnItemClickListener(this);
			} else {
				CommonUtils.showDialog(this,
						"Error occurred!\nPlease try again.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			CommonUtils.showDialog(this, "Invalid response from server.");
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		DataHolder.selectedService = services.get(position);
		startActivity(new Intent(this, SalonListActivity.class));
	}
}
