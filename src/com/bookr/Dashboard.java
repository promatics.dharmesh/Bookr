package com.bookr;

import static com.bookr.utils.DataHolder.categories;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bookr.adapters.GridAdapter;
import com.bookr.gcm.GCMHelper;
import com.bookr.models.Category;
import com.bookr.utils.CallService;
import com.bookr.utils.CommonUtils;
import com.bookr.utils.Constants;
import com.bookr.utils.DataHolder;
import com.bookr.utils.NewUser;
import com.bookr.utils.ServiceCallBack;
import com.bookr.utils.User;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

public class Dashboard extends Activity implements ServiceCallBack,
		OnClickListener, OnItemClickListener {
	ImageView imgdrawer;
	TextView selectLocaton, searchServices, searchSalon, promotedSalon,
			myProfile, myBookings, myReviews, myFavourites, myrewards,
			myOffers, logout, facebook, twitter, instagram, youtube,
			contactAdmin, aboutBookme, name, txtNewSerchSvc, txtNewSerchName,
			txtNewProm, txtLogin, txtAboutBookr, txtContact;
	SlidingMenu menu, newMenu;
	GridView grid;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.servicecategory);
		if (User.getInstance() != null) {
			initDrawer();
		} else {
			newDrawer();
		}
		grid = (GridView) findViewById(R.id.grid);
		selectLocaton = (TextView) findViewById(R.id.txt_selectLoctaion);
		selectLocaton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Dashboard.this, SelectStateCity.class);
				startActivity(i);
			}
		});
		imgdrawer = (ImageView) findViewById(R.id.imgDrawer);
		imgdrawer.setOnClickListener(this);
		new CallService(this, this, Constants.REQ_GET_CAT, true, null)
				.execute(Constants.GET_CATEGORIES);
	}

	@Override
	public void onBackPressed() {
		if (User.getInstance() != null) {
			if (menu.isMenuShowing()) {
				menu.toggle(true);
			} else {
				AlertDialog.Builder alert = new AlertDialog.Builder(
						new ContextThemeWrapper(this,
								android.R.style.Theme_Holo_Light));
				alert.setMessage("Are you sure you want to exit?");
				alert.setTitle("Bookr!");
				alert.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								Intent startMain = new Intent(
										Intent.ACTION_MAIN);
								startMain.addCategory(Intent.CATEGORY_HOME);
								startMain
										.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
												| Intent.FLAG_ACTIVITY_CLEAR_TOP);
								startActivity(startMain);
								finish();
							}
						});
				alert.setNegativeButton("No",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
							}
						});
				alert.show();
			}
		} else {
			AlertDialog.Builder alert = new AlertDialog.Builder(
					new ContextThemeWrapper(this,
							android.R.style.Theme_Holo_Light));
			alert.setMessage("Are you sure you want to exit?");
			alert.setTitle("Bookr!");
			alert.setPositiveButton("Yes",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							Intent startMain = new Intent(Intent.ACTION_MAIN);
							startMain.addCategory(Intent.CATEGORY_HOME);
							startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							startActivity(startMain);
							finish();
						}
					});
			alert.setNegativeButton("No",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
						}
					});
			alert.show();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (User.getInstance() != null) {
			// check if device registered
			GCMHelper.registerIfNotAlreadyDone(this);
			Log.v("On Resume", "visitor Login");
		}
		if (User.getInstance() != null) {
			if (User.getInstance().getCityName().isEmpty())
				selectLocaton.setText(getString(R.string.select_location));
			else
				selectLocaton.setText(User.getInstance().getCityName());
		} else if (NewUser.getInstance() == null
				|| NewUser.getInstance().getCityName().isEmpty()) {
			selectLocaton.setText(getString(R.string.select_location));
		} else {
			selectLocaton.setText(NewUser.getInstance().getCityName());
		}
	}

	private void newDrawer() {
		newMenu = new SlidingMenu(this);
		newMenu.setMode(SlidingMenu.RIGHT);
		newMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
		newMenu.setBehindOffset(100);
		newMenu.setShadowDrawable(R.drawable.shadow);
		newMenu.setShadowWidthRes(R.dimen.shadow_width);
		newMenu.setFadeDegree(0.35f);
		newMenu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
		newMenu.setMenu(R.layout.newdrawer);
		txtNewSerchSvc = (TextView) newMenu
				.findViewById(R.id.txt_searchservices);
		txtNewSerchName = (TextView) newMenu
				.findViewById(R.id.txt_serachsalons);
		txtNewProm = (TextView) newMenu.findViewById(R.id.txt_promotedsalons);
		txtLogin = (TextView) newMenu.findViewById(R.id.txt_login);
		txtContact = (TextView) newMenu.findViewById(R.id.txtContactAdmin);
		txtAboutBookr = (TextView) newMenu.findViewById(R.id.txtAboutBookr);

		txtNewSerchSvc.setOnClickListener(this);
		txtNewSerchName.setOnClickListener(this);
		txtNewProm.setOnClickListener(this);
		txtLogin.setOnClickListener(this);
		txtContact.setOnClickListener(this);
		txtAboutBookr.setOnClickListener(this);

	}

	private void initDrawer() {
		menu = new SlidingMenu(this);
		menu.setMode(SlidingMenu.RIGHT);
		menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
		menu.setBehindOffset(100);
		menu.setShadowDrawable(R.drawable.shadow);
		menu.setShadowWidthRes(R.dimen.shadow_width);
		menu.setFadeDegree(0.35f);
		menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
		menu.setMenu(R.layout.drawer);
		searchServices = (TextView) menu.findViewById(R.id.txt_searchServices);
		searchSalon = (TextView) menu.findViewById(R.id.txt_serachSalons);
		promotedSalon = (TextView) menu.findViewById(R.id.txt_promotedSalons);
		myProfile = (TextView) menu.findViewById(R.id.txt_myProfile);
		myBookings = (TextView) menu.findViewById(R.id.txt_myBooking);
		myReviews = (TextView) menu.findViewById(R.id.txt_myReviews);
		myFavourites = (TextView) menu.findViewById(R.id.txtMyfavourites);
		myrewards = (TextView) menu.findViewById(R.id.txtMyrewards);
		myOffers = (TextView) menu.findViewById(R.id.txtMyOffers);
		logout = (TextView) menu.findViewById(R.id.txt_logout);
		// facebook = (TextView) menu.findViewById(R.id.txt_fb);
		// twitter = (TextView) menu.findViewById(R.id.txt_twitter);
		// instagram = (TextView) menu.findViewById(R.id.txt_instagram);
		contactAdmin = (TextView) menu.findViewById(R.id.txt_contactAdmin);
		aboutBookme = (TextView) menu.findViewById(R.id.txt_aboutBookme);
		name = (TextView) menu.findViewById(R.id.txt_username);
		if (User.getInstance().getName() != null) {
			name.setText(User.getInstance().getName());
		}
		searchServices.setOnClickListener(this);
		searchSalon.setOnClickListener(this);
		promotedSalon.setOnClickListener(this);
		myProfile.setOnClickListener(this);
		myBookings.setOnClickListener(this);
		myReviews.setOnClickListener(this);
		myFavourites.setOnClickListener(this);
		myrewards.setOnClickListener(this);
		myOffers.setOnClickListener(this);
		logout.setOnClickListener(this);
		// facebook.setOnClickListener(this);
		// twitter.setOnClickListener(this);
		// instagram.setOnClickListener(this);
		contactAdmin.setOnClickListener(this);
		aboutBookme.setOnClickListener(this);
	}

	@Override
	public void onServiceResponse(int requestCode, JSONObject data) {
		switch (requestCode) {
		case Constants.REQ_GET_CAT:
			try {
				int code = data.getInt("responseCode");
				if (code == 1) {
					JSONArray arr = data.getJSONArray("Data");
					DataHolder.categories.clear();
					for (int i = 0; i < arr.length(); i++) {
						JSONObject servicecat = arr.getJSONObject(i)
								.getJSONObject("ServiceCategory");
						Category appsvc = new Category();
						appsvc.setId(servicecat.getString("id"));
						appsvc.setName(servicecat.getString("name"));
						appsvc.setImage(servicecat.getString("image"));
						appsvc.setStatus(servicecat.getBoolean("status"));
						DataHolder.categories.add(appsvc);
					}
					Log.v("IMG", "total imgs:" + categories.size());
					grid.setAdapter(new GridAdapter(this));
					grid.setOnItemClickListener(this);
				} else {
					CommonUtils.showDialog(this, "Error occurred");
				}
			} catch (Exception e) {
				e.printStackTrace();
				CommonUtils.showDialog(this, "Invalid response from server");
			}
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		switch (parent.getId()) {
		case R.id.grid:
			DataHolder.categorySelected = DataHolder.categories.get(position);
			Intent i = new Intent(this, ServiceList.class);
			startActivity(i);
			break;
		}
	}

	public void onNothingSelected(AdapterView<?> parent) {
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.imgDrawer:
			if (User.getInstance() != null) {
				menu.toggle();
			} else {
				newMenu.toggle();
			}
			break;
		case R.id.txt_searchServices:
			menu.toggle();
			break;
		case R.id.txt_serachSalons:
			Intent searchSalon = new Intent(this, SearchSalon.class);
			startActivity(searchSalon);
			menu.toggle();
			break;
		case R.id.txt_promotedSalons:
			Intent promoted = new Intent(this, PromotedSalons.class);
			startActivity(promoted);
			menu.toggle();
			break;
		case R.id.txt_myProfile:
			Intent intent = new Intent(this, MyProfile.class);
			startActivity(intent);
			menu.toggle();
			break;
		case R.id.txt_myBooking:
			Intent i = new Intent(this, MyBookingActivity.class);
			startActivity(i);
			menu.toggle();
			break;
		case R.id.txt_myReviews:
			Intent review = new Intent(this, MyReviewsActivity.class);
			startActivity(review);
			menu.toggle();
			break;
		case R.id.txtMyfavourites:
			Intent MyFav = new Intent(this, MyFavouriteActivity.class);
			startActivity(MyFav);
			menu.toggle();
			break;
		case R.id.txtMyrewards:
			Intent rewards = new Intent(this, MyRewardsActivity.class);
			startActivity(rewards);
			menu.toggle();
			break;
		case R.id.txtMyOffers:
			Intent offers = new Intent(this, MyOffersActivity.class);
			startActivity(offers);
			menu.toggle();
			break;
		case R.id.txt_logout:
			menu.toggle();
			if (!GCMHelper.getSavedRegistrationId().equals("")) {
				GCMHelper.unregister();
			}
			User.getInstance().logout();
			Intent intentLog = new Intent(this, Splash.class);
			intentLog.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_CLEAR_TASK
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intentLog);
			Toast.makeText(this, "You have successfully logged out.",
					Toast.LENGTH_SHORT).show();
			finish();
			break;
		// case R.id.txt_fb:
		// Toast.makeText(this, "This module is pending", Toast.LENGTH_SHORT)
		// .show();
		// break;
		// case R.id.txt_twitter:
		// Toast.makeText(this, "This module is pending", Toast.LENGTH_SHORT)
		// .show();
		// break;
		// case R.id.txt_instagram:
		// Toast.makeText(this, "This module is pending", Toast.LENGTH_SHORT)
		// .show();
		// break;
		case R.id.txt_contactAdmin:
			menu.toggle();
			Intent email = new Intent(this, ContactAdmin.class);
			startActivity(email);
			break;
		case R.id.txt_aboutBookme:
			Intent about = new Intent(this, AboutBookMe.class);
			startActivity(about);
			menu.toggle();
			break;
		case R.id.txt_searchservices:
			newMenu.toggle();
			break;
		case R.id.txt_serachsalons:
			Intent salon = new Intent(this, SearchSalon.class);
			startActivity(salon);
			newMenu.toggle();
			break;
		case R.id.txt_promotedsalons:
			Intent promo = new Intent(this, PromotedSalons.class);
			startActivity(promo);
			newMenu.toggle();
			break;
		case R.id.txt_login:
			Intent login = new Intent(this, LoginView.class);
			startActivity(login);
			newMenu.toggle();
			break;
		case R.id.txtContactAdmin:
			newMenu.toggle();
			Intent emails = new Intent(this, ContactAdmin.class);
			startActivity(emails);
			break;
		case R.id.txtAboutBookr:
			Intent abouts = new Intent(this, AboutBookMe.class);
			startActivity(abouts);
			newMenu.toggle();
			break;

		}
	}

}
