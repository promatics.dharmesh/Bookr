package com.bookr.salonlist;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bookr.AboutBookMe;
import com.bookr.ContactAdmin;
import com.bookr.Dashboard;
import com.bookr.LoginView;
import com.bookr.MyBookingActivity;
import com.bookr.MyFavouriteActivity;
import com.bookr.MyOffersActivity;
import com.bookr.MyProfile;
import com.bookr.MyReviewsActivity;
import com.bookr.MyRewardsActivity;
import com.bookr.PromotedSalons;
import com.bookr.R;
import com.bookr.SearchSalon;
import com.bookr.Splash;
import com.bookr.gcm.GCMHelper;
import com.bookr.utils.DataHolder;
import com.bookr.utils.User;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationServices;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

public class SalonListActivity extends FragmentActivity implements
		OnClickListener, OnItemSelectedListener, ConnectionCallbacks,
		OnConnectionFailedListener {
	private static final long HRS3 = 1000L * 60L * 60L * 3L;
	private Location mLastLocation;
	FragmentTabHost host;
	ImageView imgDrawer, imgIcList, imgICmap;
	TextView selectLocaton, searchServices, searchSalon, promotedSalon,
			myProfile, myBookings, myReviews, myOffers, myFavourites,
			myrewards, logout, facebook, twitter, instagram, youtube,
			contactAdmin, aboutBookme, name, txtNewSerchSvc, txtNewSerchName,
			txtNewProm, txtLogin, txtAboutBookr, txtContact;
	SlidingMenu menu, newMenu;
	Spinner spinner;
	GoogleApiClient mGoogleApiClient;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.salonlist);
		if (User.getInstance() != null) {
			initDrawer();
		} else {
			newDrawer();
		}
		imgDrawer = (ImageView) findViewById(R.id.imgDrawer);
		spinner = (Spinner) findViewById(R.id.spinFilter);
		spinner.setAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, android.R.id.text1,
				getResources().getStringArray(R.array.spinItems)));
		spinner.setOnItemSelectedListener(this);
		imgDrawer.setOnClickListener(this);
		host = (FragmentTabHost) findViewById(android.R.id.tabhost);
		host.setup(this, getSupportFragmentManager(), R.id.realtabcontent);
		host.addTab(
				host.newTabSpec("1")
						.setIndicator(indicator(R.drawable.ic_list)),
				SalonsFragment.class, arg0);
		host.addTab(
				host.newTabSpec("2").setIndicator(indicator(R.drawable.ic_map)),
				SalonsMapFrag.class, arg0);
		host.setCurrentTab(0);
		if (isServiceConnected()) {
			buildGoogleApiClient();
			mGoogleApiClient.connect();
		}
	}

	private void newDrawer() {
		newMenu = new SlidingMenu(this);
		newMenu.setMode(SlidingMenu.RIGHT);
		newMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
		newMenu.setBehindOffset(100);
		newMenu.setShadowDrawable(R.drawable.shadow);
		newMenu.setShadowWidthRes(R.dimen.shadow_width);
		newMenu.setFadeDegree(0.35f);
		newMenu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
		newMenu.setMenu(R.layout.newdrawer);
		txtNewSerchSvc = (TextView) newMenu
				.findViewById(R.id.txt_searchservices);
		txtNewSerchName = (TextView) newMenu
				.findViewById(R.id.txt_serachsalons);
		txtNewProm = (TextView) newMenu.findViewById(R.id.txt_promotedsalons);
		txtLogin = (TextView) newMenu.findViewById(R.id.txt_login);
		txtContact = (TextView) newMenu.findViewById(R.id.txtContactAdmin);
		txtAboutBookr = (TextView) newMenu.findViewById(R.id.txtAboutBookr);

		txtNewSerchSvc.setOnClickListener(this);
		txtNewSerchName.setOnClickListener(this);
		txtNewProm.setOnClickListener(this);
		txtLogin.setOnClickListener(this);
		txtContact.setOnClickListener(this);
		txtAboutBookr.setOnClickListener(this);
	}

	public synchronized void buildGoogleApiClient() {
		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(LocationServices.API).build();

	}

	@SuppressLint("InflateParams")
	private View indicator(int id) {
		View v = getLayoutInflater().inflate(R.layout.map_list_tab_indicator,
				null, false);
		((ImageView) v.findViewById(R.id.imgTab)).setImageResource(id);
		return v;
	}

	private void initDrawer() {
		menu = new SlidingMenu(this);
		menu.setMode(SlidingMenu.RIGHT);
		menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
		menu.setBehindOffset(100);
		menu.setShadowDrawable(R.drawable.shadow);
		menu.setShadowWidthRes(R.dimen.shadow_width);
		menu.setFadeDegree(0.35f);
		menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
		menu.setMenu(R.layout.drawer);

		searchServices = (TextView) menu.findViewById(R.id.txt_searchServices);
		searchSalon = (TextView) menu.findViewById(R.id.txt_serachSalons);
		promotedSalon = (TextView) menu.findViewById(R.id.txt_promotedSalons);
		myProfile = (TextView) menu.findViewById(R.id.txt_myProfile);
		myBookings = (TextView) menu.findViewById(R.id.txt_myBooking);
		myReviews = (TextView) menu.findViewById(R.id.txt_myReviews);
		myFavourites = (TextView) menu.findViewById(R.id.txtMyfavourites);
		myrewards = (TextView) menu.findViewById(R.id.txtMyrewards);
		myOffers = (TextView) menu.findViewById(R.id.txtMyOffers);
		logout = (TextView) menu.findViewById(R.id.txt_logout);
		// facebook = (TextView) menu.findViewById(R.id.txt_fb);
		// twitter = (TextView) menu.findViewById(R.id.txt_twitter);
		// instagram = (TextView) menu.findViewById(R.id.txt_instagram);
		contactAdmin = (TextView) menu.findViewById(R.id.txt_contactAdmin);
		aboutBookme = (TextView) menu.findViewById(R.id.txt_aboutBookme);
		name = (TextView) menu.findViewById(R.id.txt_username);
		if (User.getInstance().getName() != null) {
			name.setText(User.getInstance().getName());
		}
		searchServices.setOnClickListener(this);
		searchSalon.setOnClickListener(this);
		promotedSalon.setOnClickListener(this);
		myProfile.setOnClickListener(this);
		myBookings.setOnClickListener(this);
		myReviews.setOnClickListener(this);
		myFavourites.setOnClickListener(this);
		myrewards.setOnClickListener(this);
		myOffers.setOnClickListener(this);
		logout.setOnClickListener(this);
		// facebook.setOnClickListener(this);
		// twitter.setOnClickListener(this);
		// instagram.setOnClickListener(this);
		contactAdmin.setOnClickListener(this);
		aboutBookme.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.imgDrawer:
			if (User.getInstance() != null) {
				menu.toggle();
			} else {
				newMenu.toggle();
			}
			break;
		case R.id.txt_searchServices:
			Intent services = new Intent(this, Dashboard.class);
			startActivity(services);
			menu.toggle();
			break;
		case R.id.txt_serachSalons:
			Intent searchSalon = new Intent(this, SearchSalon.class);
			startActivity(searchSalon);
			menu.toggle();
			break;
		case R.id.txt_promotedSalons:
			Intent promoted = new Intent(this, PromotedSalons.class);
			startActivity(promoted);
			menu.toggle();
			break;
		case R.id.txt_myProfile:
			Intent intent = new Intent(this, MyProfile.class);
			startActivity(intent);
			menu.toggle();
			break;
		case R.id.txt_myBooking:
			Intent i = new Intent(this, MyBookingActivity.class);
			startActivity(i);
			menu.toggle();
			break;
		case R.id.txt_myReviews:
			Intent review = new Intent(this, MyReviewsActivity.class);
			startActivity(review);
			menu.toggle();
			break;
		case R.id.txtMyfavourites:
			Intent MyFav = new Intent(this, MyFavouriteActivity.class);
			startActivity(MyFav);
			menu.toggle();
			break;
		case R.id.txtMyrewards:
			Intent rewards = new Intent(this, MyRewardsActivity.class);
			startActivity(rewards);
			menu.toggle();
			break;
		case R.id.txtMyOffers:
			Intent offers = new Intent(this, MyOffersActivity.class);
			startActivity(offers);
			menu.toggle();
			break;
		case R.id.txt_logout:
			menu.toggle();
			if (!GCMHelper.getSavedRegistrationId().equals("")) {
				GCMHelper.unregister();
			}
			User.getInstance().logout();
			Intent intentLog = new Intent(this, Splash.class);
			startActivity(intentLog);
			Toast.makeText(this, "You have successfully logged out.",
					Toast.LENGTH_SHORT).show();
			finish();
			break;
		// case R.id.txt_fb:
		// Toast.makeText(this, "This module is pending", Toast.LENGTH_SHORT)
		// .show();
		// break;
		// case R.id.txt_twitter:
		// Toast.makeText(this, "This module is pending", Toast.LENGTH_SHORT)
		// .show();
		// break;
		// case R.id.txt_instagram:
		// Toast.makeText(this, "This module is pending", Toast.LENGTH_SHORT)
		// .show();
		// break;
		case R.id.txt_contactAdmin:
			menu.toggle();
			Intent email = new Intent(this, ContactAdmin.class);
			startActivity(email);
			break;
		case R.id.txt_aboutBookme:
			Intent about = new Intent(this, AboutBookMe.class);
			startActivity(about);
			menu.toggle();
			break;
		case R.id.txt_searchservices:
			Intent service = new Intent(this, Dashboard.class);
			startActivity(service);
			newMenu.toggle();
			break;
		case R.id.txt_serachsalons:
			Intent salon = new Intent(this, SearchSalon.class);
			startActivity(salon);
			newMenu.toggle();
			break;
		case R.id.txt_promotedsalons:
			Intent promo = new Intent(this, PromotedSalons.class);
			startActivity(promo);
			newMenu.toggle();
			break;
		case R.id.txt_login:
			Intent login = new Intent(this, LoginView.class);
			startActivity(login);
			newMenu.toggle();
			break;
		case R.id.txtContactAdmin:
			newMenu.toggle();
			Intent emails = new Intent(this, ContactAdmin.class);
			startActivity(emails);
			break;
		case R.id.txtAboutBookr:
			Intent abouts = new Intent(this, AboutBookMe.class);
			startActivity(abouts);
			newMenu.toggle();
			break;

		}
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		switch (position) {
		case 0:
			if (host.getCurrentTab() != 0)
				host.setCurrentTab(0);
			try {
				if(DataHolder.check == false){
				for (Fragment frag : getSupportFragmentManager().getFragments()) {
					if (frag instanceof SalonsFragment && frag.isVisible()) {
						((SalonsFragment) frag).callService();
					}
				}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case 1:
			if (host.getCurrentTab() != 0) {
				host.setCurrentTab(0);
			}
			try {
				for (Fragment frag : getSupportFragmentManager().getFragments()) {
					if (frag instanceof SalonsFragment && frag.isVisible()) {
						((SalonsFragment) frag).soonestAvailable();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case 2:
			if (host.getCurrentTab() != 0)
				host.setCurrentTab(0);
			try {
				for (Fragment frag : getSupportFragmentManager().getFragments()) {
					if (frag instanceof SalonsFragment && frag.isVisible()) {
						((SalonsFragment) frag).HighestRatedcallService();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case 3:
			if (host.getCurrentTab() != 0)
				host.setCurrentTab(0);
			try {
				for (Fragment frag : getSupportFragmentManager().getFragments()) {
					if (frag instanceof SalonsFragment && frag.isVisible()) {
						((SalonsFragment) frag).mostBookedcallService();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case 4:
			if (!isServiceConnected() && mLastLocation == null) {
				Toast.makeText(
						this,
						"Your location might be inaccurate, please check your location settings",
						Toast.LENGTH_LONG).show();
				return;
			}
			if (host.getCurrentTab() != 0) {
				host.setCurrentTab(0);
			}
			host.setCurrentTab(0);
			try {
				for (Fragment frag : getSupportFragmentManager().getFragments()) {
					if (frag instanceof SalonsFragment && frag.isVisible()) {
						((SalonsFragment) frag).NearMeByEmployee();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		Toast.makeText(
				this,
				"Unable to get your location. Please check your location settings.",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		mLastLocation = LocationServices.FusedLocationApi
				.getLastLocation(mGoogleApiClient);
		mGoogleApiClient.disconnect();
	}

	@Override
	public void onConnectionSuspended(int arg0) {
	}

	public Location getLastLocation() {
		if (System.currentTimeMillis() - mLastLocation.getTime() >= HRS3) {
			// toast
			Toast.makeText(
					this,
					"Your location might be inaccurate, please check your location settings",
					Toast.LENGTH_LONG).show();
			mGoogleApiClient.connect();
		}
		return mLastLocation;
	}

	private boolean isServiceConnected() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);

		if (ConnectionResult.SUCCESS == resultCode) {
			return true;
		} else {
			return false;
		}
	}

}
