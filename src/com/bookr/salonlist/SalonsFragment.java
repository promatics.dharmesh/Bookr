package com.bookr.salonlist;

import static com.bookr.utils.DataHolder.selectedService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bookr.R;
import com.bookr.SelectStateCity;
import com.bookr.adapters.SaloonListAdapter;
import com.bookr.adapters.SoonestSalonAdapter;
import com.bookr.models.Saloon;
import com.bookr.utils.CallService;
import com.bookr.utils.CommonUtils;
import com.bookr.utils.Constants;
import com.bookr.utils.DataHolder;
import com.bookr.utils.NewUser;
import com.bookr.utils.ServiceCallBack;
import com.bookr.utils.User;

@SuppressWarnings("deprecation")
@SuppressLint("SimpleDateFormat")
public class SalonsFragment extends Fragment implements ServiceCallBack,
		OnClickListener {
	ListView listView;
	public static TextView txtCity;
	TextView txtDate, txtTime;
	int Hour, Minute, Year, Month, Day;
	boolean isTimeset = false, isDateset = false;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater
				.inflate(R.layout.saloon_list_layout, container, false);
		listView = (ListView) v.findViewById(R.id.list);
		txtCity = (TextView) v.findViewById(R.id.txtCity);
		txtDate = (TextView) v.findViewById(R.id.txtSetDate);
		Calendar ca = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		txtDate.setText(format.format(ca.getTime()));
		isDateset = true;
		txtDate.setOnClickListener(this);
		txtTime = (TextView) v.findViewById(R.id.txtSetTime);
		SimpleDateFormat format1 = new SimpleDateFormat("hh:mm a");
		txtTime.setText(format1.format(ca.getTime()));
		isTimeset = true;
		txtTime.setOnClickListener(this);
		if (User.getInstance() != null) {
			txtCity.setText(User.getInstance().getCityName());
		} else {
			txtCity.setText(R.string.select_location);
		}
		txtCity.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getActivity(), SelectStateCity.class);
				startActivity(i);
			}
		});
		if (((SalonListActivity) getActivity()).spinner
				.getSelectedItemPosition() != 0) {
			((SalonListActivity) getActivity()).spinner.setSelection(0);
		}
		return v;
	}

	@Override
	public void onResume() {
		super.onResume();
		if (DataHolder.check == true) {
			callService();
		}
	}

	public void callService() {
		if (isDateset && isTimeset) {
			try {
				if (User.getInstance() != null
						&& !User.getInstance().getCityName().equals("")) {
					txtCity.setText(User.getInstance().getCityName());
				} else {
					txtCity.setText(NewUser.getInstance().getCityName());
				}

				List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
				values.add(new BasicNameValuePair("service_name_id",
						selectedService.getId()));
				if (User.getInstance() != null) {
					values.add(new BasicNameValuePair("city_id", User
							.getInstance().getCityId()));
				} else {
					values.add(new BasicNameValuePair("city_id", NewUser
							.getInstance().getCityId()));
				}
				values.add(new BasicNameValuePair("date", txtDate.getText()
						.toString()));
				values.add(new BasicNameValuePair("time", txtTime.getText()
						.toString()));
				new CallService(this, getActivity(),
						Constants.REQ_DEFAULT_SALONLIST_BY_EMP, true, values)
						.execute(Constants.DEFAULT_SALONLIST_BY_EMP);
			} catch (Exception e) {
				e.printStackTrace();
				Toast.makeText(getActivity(),
						"Please Select Location to view Salons",
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	public void HighestRatedcallService() {
		if (isDateset && isTimeset) {
			try {
				if (User.getInstance() != null
						&& !User.getInstance().getCityName().equals("")) {
					txtCity.setText(User.getInstance().getCityName());
				} else {
					txtCity.setText(NewUser.getInstance().getCityName());
				}
				List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
				values.add(new BasicNameValuePair("service_name_id",
						selectedService.getId()));
				if (User.getInstance() != null) {
					values.add(new BasicNameValuePair("city_id", User
							.getInstance().getCityId()));
				} else {
					values.add(new BasicNameValuePair("city_id", NewUser
							.getInstance().getCityId()));
				}
				values.add(new BasicNameValuePair("date", txtDate.getText()
						.toString()));
				values.add(new BasicNameValuePair("time", txtTime.getText()
						.toString()));
				new CallService(this, getActivity(),
						Constants.REQ_DEFAULT_SALONLIST_BY_EMP, true, values)
						.execute(Constants.GET_MOST_RATED_SALON_BY_EMP);
			} catch (Exception e) {
				e.printStackTrace();
				Toast.makeText(getActivity(),
						"Please Select Location to view Salons",
						Toast.LENGTH_SHORT).show();
			}
		}

	}

	public void mostBookedcallService() {
		if (isDateset && isTimeset) {
			try {
				if (User.getInstance() != null
						&& !User.getInstance().getCityName().equals("")) {
					txtCity.setText(User.getInstance().getCityName());
				} else {
					txtCity.setText(NewUser.getInstance().getCityName());
				}
				List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
				values.add(new BasicNameValuePair("service_name_id",
						selectedService.getId()));
				if (User.getInstance() != null) {
					values.add(new BasicNameValuePair("city_id", User
							.getInstance().getCityId()));
				} else {
					values.add(new BasicNameValuePair("city_id", NewUser
							.getInstance().getCityId()));
				}
				values.add(new BasicNameValuePair("date", txtDate.getText()
						.toString()));
				values.add(new BasicNameValuePair("time", txtTime.getText()
						.toString()));
				new CallService(this, getActivity(),
						Constants.REQ_DEFAULT_SALONLIST_BY_EMP, true, values)
						.execute(Constants.GET_MOST_BOOKED_SALON_BY_EMP);
			} catch (Exception e) {
				e.printStackTrace();
				Toast.makeText(getActivity(),
						"Please Select Location to view Salons",
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	public void NearMeByEmployee() {
		if (isDateset && isTimeset) {
			try {
				if (User.getInstance() != null
						&& !User.getInstance().getCityName().equals("")) {
					txtCity.setText(User.getInstance().getCityName());
				} else {
					txtCity.setText(NewUser.getInstance().getCityName());
				}

				List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
				Location location = ((SalonListActivity) getActivity())
						.getLastLocation();
				if (location != null) {
					values.add(new BasicNameValuePair("lat", String
							.valueOf(location.getLatitude())));
					values.add(new BasicNameValuePair("lng", String
							.valueOf(location.getLongitude())));
				} else {
					Toast.makeText(getActivity(), "error occured",
							Toast.LENGTH_SHORT).show();
					return;
				}
				values.add(new BasicNameValuePair("service_name_id",
						selectedService.getId()));
				if (User.getInstance() != null) {
					values.add(new BasicNameValuePair("city_id", User
							.getInstance().getCityId()));
				} else {
					values.add(new BasicNameValuePair("city_id", NewUser
							.getInstance().getCityId()));
				}
				values.add(new BasicNameValuePair("date", txtDate.getText()
						.toString()));
				values.add(new BasicNameValuePair("time", txtTime.getText()
						.toString()));
				System.out.println(values);
				new CallService(this, getActivity(),
						Constants.REQ_DEFAULT_SALONLIST_BY_EMP, true, values)
						.execute(Constants.GET_NEAR_ME_BY_EMP);
			} catch (Exception e) {
				e.printStackTrace();
				Toast.makeText(getActivity(),
						"Please Select Location to view Salons",
						Toast.LENGTH_SHORT).show();
			}
		}

	}

	public void soonestAvailable() {
		if (isDateset && isTimeset) {
			try {
				if (User.getInstance() != null
						&& !User.getInstance().getCityName().equals("")) {
					txtCity.setText(User.getInstance().getCityName());
				} else {
					txtCity.setText(NewUser.getInstance().getCityName());
				}
				List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
				values.add(new BasicNameValuePair("service_name_id",
						selectedService.getId()));
				if (User.getInstance() != null) {
					values.add(new BasicNameValuePair("city_id", User
							.getInstance().getCityId()));
				} else {
					values.add(new BasicNameValuePair("city_id", NewUser
							.getInstance().getCityId()));
				}
				values.add(new BasicNameValuePair("date", txtDate.getText()
						.toString()));
				values.add(new BasicNameValuePair("time", txtTime.getText()
						.toString()));
				new CallService(this, getActivity(),
						Constants.REQ_GET_SOONEST_AVAILABLE_BY_EMP, true,
						values).execute(Constants.GET_SOONEST_AVAILABLE_BY_EMP);
			} catch (Exception e) {
				e.printStackTrace();
				Toast.makeText(getActivity(),
						"Please Select Location to view Salons",
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	@Override
	public void onServiceResponse(int requestCode, JSONObject data) {
		switch (requestCode) {
		case Constants.REQ_DEFAULT_SALONLIST_BY_EMP:
			try {
				int code = data.getInt("responseCode");
				if (code == 1) {
					DataHolder.check = false;
					JSONArray salonArr = data.getJSONArray("Data");
					DataHolder.saloons.clear();
					for (int i = 0; i < salonArr.length(); i++) {
						JSONObject user = salonArr.getJSONObject(i)
								.getJSONObject("User");
						Saloon saloon = new Saloon();
						saloon.setId(user.getString("id"));
						saloon.setBusiness_name(user.getString("business_name"));
						saloon.setAddress(user.getString("address"));
						saloon.setAbout_us(user.getString("about_us"));
						saloon.setLocation_id(user.getString("location_id"));
						saloon.setCity_id(user.getString("city_id"));
						saloon.setLat(user.getString("lat"));
						saloon.setLng(user.getString("lng"));
						saloon.setReview(user.getString("review"));
						saloon.setRating(user.getString("rating"));
						if (user.has("promo")) {
							saloon.setPromo(user.getString("promo"));
						}
						if (user.has("distance"))
							saloon.setDistance(user.getString("distance"));
						JSONArray imgArr = salonArr.getJSONObject(i)
								.getJSONArray("BusinessImage");
						if (imgArr != null && imgArr.length() != 0) {
							String[] imgs = new String[imgArr.length()];
							for (int j = 0; j < imgArr.length(); j++) {
								imgs[j] = imgArr.getJSONObject(j).getString(
										"url");
								if (j == 0) {
									saloon.setImg(imgs[0]);
								}
								imgs[j] = imgArr.getJSONObject(j).getString(
										"url");
							}
							saloon.setImgs(imgs);
						}
						// JSONArray scheduleArr = salonArr.getJSONObject(i)
						// .getJSONArray("Schedule");
						// if (scheduleArr.length() == 0)
						// salonArr.get(i).equals(null);
						//
						// TimeSlot[] slots = new
						// TimeSlot[scheduleArr.length()];
						// for (int j = 0; j < scheduleArr.length(); j++) {
						// JSONObject slotobj = scheduleArr.getJSONObject(j);
						// TimeSlot slot = new TimeSlot();
						// slot.setSlotID(slotobj.getString("id"));
						// slot.setStartTime(slotobj.getString("starttime"));
						// slot.setEndTime(slotobj.getString("endtime"));
						// slot.setEmpId(slotobj.getString("employee_id"));
						// slots[j] = slot;
						// }
						// saloon.setSlots(slots);
						DataHolder.selectedDate = txtDate.getText().toString();
						System.out.println(DataHolder.selectedDate);
						DataHolder.saloons.add(saloon);
					}
					listView.setAdapter(new SaloonListAdapter(getActivity()));
					// listView.setOnItemClickListener(new OnItemClickListener()
					// {
					//
					// @Override
					// public void onItemClick(AdapterView<?> parent,
					// View view, int position, long id) {
					// BookFrag.searchFlag = false;
					// Intent i = new Intent(getActivity(),
					// SaloonDetailActivity.class).putExtra(
					// "salonId", DataHolder.saloons.get(position).getId());
					// startActivity(i);
					// }
					// });
					if (DataHolder.saloons.size() == 0) {
						CommonUtils
								.showDialog(getActivity(),
										"No salons found under this category for this city.");
					}
				} else {
					CommonUtils.showDialog(getActivity(), "first Select city");
				}
			} catch (Exception e) {
				e.printStackTrace();
				CommonUtils.showDialog(getActivity(),
						"Invalid response from server.");
			}
			break;
		case Constants.REQ_GET_SOONEST_AVAILABLE_BY_EMP:
			try {
				int code = data.getInt("responseCode");
				if (code == 1) {
					JSONArray salonArr = data.getJSONArray("Data");
					DataHolder.saloons.clear();
					for (int i = 0; i < salonArr.length(); i++) {
						Saloon saloon = new Saloon();
						// JSONArray slotsArr = salonArr.getJSONObject(i)
						// .getJSONArray("Schedule");
						// if (slotsArr.length() == 0)
						// continue;
						// TimeSlot[] slots = new TimeSlot[slotsArr.length()];
						// for (int j = 0; j < slotsArr.length(); j++) {
						// JSONObject slotobj = slotsArr.getJSONObject(j);
						// TimeSlot slot = new TimeSlot();
						// slot.setSlotID(slotobj.getString("id"));
						// slot.setStartTime(slotobj.getString("starttime"));
						// slot.setEndTime(slotobj.getString("endtime"));
						// slot.setEmpId(slotobj.getString("employee_id"));
						// slots[j] = slot;
						// }
						// saloon.setSlots(slots);
						JSONObject user = salonArr.getJSONObject(i)
								.getJSONObject("User");

						saloon.setId(user.getString("id"));
						saloon.setBusiness_name(user.getString("business_name"));
						saloon.setAddress(user.getString("address"));
						saloon.setAbout_us(user.getString("about_us"));
						saloon.setLocation_id(user.getString("location_id"));
						saloon.setCity_id(user.getString("city_id"));
						saloon.setLat(user.getString("lat"));
						saloon.setLng(user.getString("lng"));
						saloon.setReview(user.getString("review"));
						saloon.setRating(user.getString("rating"));
						if (user.has("promo")) {
							saloon.setPromo(user.getString("promo"));
						}
						if (user.has("distance"))
							saloon.setDistance(user.getString("distance"));
						JSONArray imgArr = salonArr.getJSONObject(i)
								.getJSONArray("BusinessImage");
						if (imgArr != null && imgArr.length() != 0) {
							String[] imgs = new String[imgArr.length()];
							for (int j = 0; j < imgArr.length(); j++) {
								imgs[j] = imgArr.getJSONObject(j).getString(
										"url");
								if (j == 0) {
									saloon.setImg(imgs[0]);
								}
								imgs[j] = imgArr.getJSONObject(j).getString(
										"url");
							}
							saloon.setImgs(imgs);
						}
						DataHolder.selectedDate = txtDate.getText().toString();
						System.out.println(DataHolder.selectedDate);
						DataHolder.saloons.add(saloon);
					}
					listView.setAdapter(new SoonestSalonAdapter(getActivity()));
					// listView.setOnItemClickListener(new OnItemClickListener()
					// {
					//
					// @Override
					// public void onItemClick(AdapterView<?> parent,
					// View view, int position, long id) {
					// BookFrag.searchFlag = false;
					// Intent i = new Intent(getActivity(),
					// SaloonDetailActivity.class).putExtra(
					// "salonId", DataHolder.saloons.get(position).getId());
					// startActivity(i);
					// }
					// });
					if (DataHolder.saloons.size() == 0) {
						CommonUtils
								.showDialog(getActivity(),
										"No salons found under this category for this city.");
					}
				} else {
					CommonUtils.showDialog(getActivity(),
							"Error occured!\nPlease try again.");
				}
			} catch (Exception e) {
				e.printStackTrace();
				CommonUtils.showDialog(getActivity(),
						"Invalid response from server.");
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.txtSetDate:
			final Calendar c = Calendar.getInstance();
			Year = c.get(Calendar.YEAR);
			Month = c.get(Calendar.MONTH);
			Day = c.get(Calendar.DAY_OF_MONTH);
			DatePickerDialog dpd = new DatePickerDialog(getActivity(),
					new OnDateSetListener() {
						@Override
						public void onDateSet(DatePicker arg0, int Year,
								int Month, int Day) {
							txtDate.setText(Day + "-" + (Month + 1) + "-"
									+ Year);
							String date = txtDate.getText().toString();
							try {
								SimpleDateFormat sdf = new SimpleDateFormat(
										"dd-MM-yyyy");
								Date dateObj = sdf.parse(date);
								txtDate.setText(new SimpleDateFormat(
										"dd-MM-yyyy").format(dateObj));
							} catch (final Exception e) {
								e.printStackTrace();
							}
							isDateset = true;
							callService();
						}
					}, Year, Month, Day);
			long t = System.currentTimeMillis();
			dpd.getDatePicker().setMinDate(t - 1000);
			dpd.show();

			break;
		case R.id.txtSetTime:
			final Calendar c1 = Calendar.getInstance();
			Hour = c1.get(Calendar.HOUR_OF_DAY);
			Minute = c1.get(Calendar.MINUTE);
			TimePickerDialog tpd = new TimePickerDialog(getActivity(),
					new OnTimeSetListener() {
						@Override
						public void onTimeSet(TimePicker view, int hourOfDay,
								int minute) {
							txtTime.setText(hourOfDay + ":" + minute);
							String time = txtTime.getText().toString();
							Log.v("24Hr", time);
							try {
								SimpleDateFormat sdf = new SimpleDateFormat(
										"H:mm");
								Date dateObj = sdf.parse(time);
								txtTime.setText(new SimpleDateFormat("hh:mm a")
										.format(dateObj));
							} catch (final Exception e) {
								e.printStackTrace();
							}
							isTimeset = true;
							callService();
						}
					}, Hour, Minute, false);
			tpd.show();
			break;
		}
	}
}
