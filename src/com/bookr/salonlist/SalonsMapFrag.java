package com.bookr.salonlist;

import static com.bookr.utils.Constants.NE_LAT;
import static com.bookr.utils.Constants.NE_LNG;
import static com.bookr.utils.Constants.REQ_SALON_MAP;
import static com.bookr.utils.Constants.SALON_MAP;
import static com.bookr.utils.Constants.SW_LAT;
import static com.bookr.utils.Constants.SW_LNG;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Dialog;
import android.location.Location;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bookr.R;
import com.bookr.custom.MapStateListener;
import com.bookr.custom.TouchableMapFragment;
import com.bookr.models.Saloon;
import com.bookr.utils.CallService;
import com.bookr.utils.CommonUtils;
import com.bookr.utils.ServiceCallBack;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLoadedCallback;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

@SuppressWarnings("deprecation")
public class SalonsMapFrag extends Fragment implements ServiceCallBack,
		OnMapReadyCallback, OnMapLoadedCallback {

	private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

	private View v;
	private GoogleMap googleMap;
	private TouchableMapFragment mapFragment;
	private Set<Saloon> allSalons = new TreeSet<Saloon>();
	private CallService task;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		try {
			// if (((SalonListActivity) getActivity()).spinner
			// .getSelectedItemPosition() != 3) {
			// ((SalonListActivity) getActivity()).spinner.setSelection(3);
			// }
			if (v != null) {
				ViewGroup parent = (ViewGroup) v.getParent();
				if (parent != null)
					parent.removeView(v);
			}
			try {
				v = inflater.inflate(R.layout.map_frag, container, false);
			} catch (InflateException e) {
				return v;
			}
			initMap();
			return v;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private void getSalons() {
		if (task != null && task.getStatus() != Status.FINISHED)
			task.cancel(false);
		List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
		LatLngBounds bonds = googleMap.getProjection().getVisibleRegion().latLngBounds;
		values.add(new BasicNameValuePair(NE_LAT, String
				.valueOf(bonds.northeast.latitude)));
		values.add(new BasicNameValuePair(NE_LNG, String
				.valueOf(bonds.northeast.longitude)));
		values.add(new BasicNameValuePair(SW_LAT, String
				.valueOf(bonds.southwest.latitude)));
		values.add(new BasicNameValuePair(SW_LNG, String
				.valueOf(bonds.southwest.longitude)));
		for (BasicNameValuePair value : values) {
			Log.v(value.getName(), value.getValue());
		}
		Toast.makeText(getActivity().getApplicationContext(),
				"Fetching Salons", Toast.LENGTH_SHORT).show();
		task = new CallService(this, getActivity(), REQ_SALON_MAP, false,
				values);
		task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, SALON_MAP);
	}

	private void initMap() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(getActivity());
		if (ConnectionResult.SUCCESS == resultCode) {
			mapFragment = (TouchableMapFragment) getChildFragmentManager()
					.findFragmentById(R.id.myMap);
			mapFragment.getMapAsync(this);
		} else {
			Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
					resultCode, getActivity(),
					CONNECTION_FAILURE_RESOLUTION_REQUEST);
			// If Google Play services can provide an error dialog
			if (errorDialog != null) {
				errorDialog.show();
			} else {
				CommonUtils.showDialog(getActivity(),
						"Google Play Services not available");
			}
		}
	}

	@Override
	public void onServiceResponse(int requestCode, JSONObject data) {
		try {
			switch (requestCode) {
			case REQ_SALON_MAP:
				int code = data.getInt("responseCode");
				if (code == 1) {
					JSONArray salonArr = data.getJSONArray("Data");
					for (int i = 0; i < salonArr.length(); i++) {
						JSONObject user = salonArr.getJSONObject(i)
								.getJSONObject("User");
						Saloon saloon = new Saloon();
						saloon.setId(user.getString("id"));
						saloon.setBusiness_name(user.getString("business_name"));
						saloon.setAddress(user.getString("address"));
						saloon.setLat(user.getString("lat"));
						saloon.setLng(user.getString("lng"));
						allSalons.add(saloon);
					}
					showMarkers();

				} else {
					CommonUtils.showDialog(getActivity(),
							"Error occurred!\nPlease try again.");
				}
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			CommonUtils.showDialog(getActivity(), "Server Error!");
		}
	}

	@Override
	public void onMapReady(GoogleMap map) {
		if (googleMap == null) {
			googleMap = map;
			googleMap.setMyLocationEnabled(true);
			googleMap.getUiSettings().setZoomControlsEnabled(true);
			googleMap.setOnMapLoadedCallback(this);
		}
	}

	private void setUpMap() {
		try {
			Location myLocation = googleMap.getMyLocation();
			LatLng myLatLng = new LatLng(myLocation.getLatitude(),
					myLocation.getLongitude());
			googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLatLng,
					12.0f));

			new MapStateListener(googleMap, mapFragment, getActivity()) {

				@Override
				public void onMapUnsettled() {
				}

				@Override
				public void onMapTouched() {
				}

				@Override
				public void onMapSettled() {
					getSalons();
				}

				@Override
				public void onMapReleased() {
				}
			};
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onMapLoaded() {
		setUpMap();
	}

	private void showMarkers() {
		googleMap.clear();
		Location myLocation = googleMap.getMyLocation();
		googleMap.addMarker(new MarkerOptions()
				.position(
						new LatLng(myLocation.getLatitude(), myLocation
								.getLongitude()))
				.icon(BitmapDescriptorFactory.fromResource(R.drawable.map))
				.title("Its You"));
		for (Saloon salon : allSalons) {
			googleMap.addMarker(new MarkerOptions()
					.position(
							new LatLng(Double.valueOf(salon.getLat()), Double
									.valueOf(salon.getLng())))
					.title(salon.getBusiness_name())
					.snippet(salon.getAddress()));
		}
	}
}
