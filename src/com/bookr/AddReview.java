package com.bookr;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.bookr.utils.CallService;
import com.bookr.utils.CommonUtils;
import com.bookr.utils.Constants;
import com.bookr.utils.DataHolder;
import com.bookr.utils.ServiceCallBack;
import com.bookr.utils.User;

@SuppressWarnings("deprecation")
public class AddReview extends Activity implements OnClickListener,
		ServiceCallBack {
	EditText etSearchSalon, etReviewTitle, etdetail;
	Button btnSubmitReview;
	RatingBar ratingBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_review);
		etSearchSalon = (EditText) findViewById(R.id.etSearchSalon);
		etReviewTitle = (EditText) findViewById(R.id.etReviewTitle);
		etdetail = (EditText) findViewById(R.id.etdetail);
		btnSubmitReview = (Button) findViewById(R.id.btnSubmitReview);
		ratingBar = (RatingBar) findViewById(R.id.ratingBar);
		etSearchSalon.setText(DataHolder.salondetail.getSalonName());
		btnSubmitReview.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnSubmitReview:
			if (etReviewTitle.getText().toString().equals("")) {
				AlertDialog.Builder alert = new AlertDialog.Builder(
						new ContextThemeWrapper(this,
								android.R.style.Theme_Holo_Light));
				alert.setMessage("Please enter Review title");
				alert.setTitle("Review Title");
				alert.setPositiveButton("OK", null);
				alert.show();
				return;
			}
			if (ratingBar.getRating() == 0) {
				AlertDialog.Builder alert = new AlertDialog.Builder(
						new ContextThemeWrapper(this,
								android.R.style.Theme_Holo_Light));
				alert.setMessage("Please enter Rating");
				alert.setTitle("Salon Rating");
				alert.setPositiveButton("OK", null);
				alert.show();
				return;
			}
			if (etdetail.getText().toString().equals("")) {
				AlertDialog.Builder alert = new AlertDialog.Builder(
						new ContextThemeWrapper(this,
								android.R.style.Theme_Holo_Light));
				alert.setMessage("Please enter comment About salon");
				alert.setTitle("Comment");
				alert.setPositiveButton("OK", null);
				alert.show();
				return;
			}
			List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
			values.add(new BasicNameValuePair("booking_id",
					DataHolder.salondetail.getBookingId()));
			values.add(new BasicNameValuePair("saloon_id",
					DataHolder.salondetail.getSalonId()));
			values.add(new BasicNameValuePair("visitor_id", User.getInstance()
					.getUserID()));
			values.add(new BasicNameValuePair("title", etReviewTitle.getText()
					.toString()));
			values.add(new BasicNameValuePair("review", etdetail.getText()
					.toString()));
			String rating = String.valueOf(ratingBar.getRating());
			values.add(new BasicNameValuePair("rating", rating));

			new CallService(this, this,
					Constants.REQ_PROVIDE_REVIEW_AND_RATING, true, values)
					.execute(Constants.PROVIDE_REVIEW_AND_RATING);
			etSearchSalon.setText(null);
			etReviewTitle.setText(null);
			etdetail.setText(null);
			ratingBar.setRating(0);
			break;
		}
	}

	@Override
	public void onServiceResponse(int requestCode, JSONObject data) {
		switch (requestCode) {
		case Constants.REQ_PROVIDE_REVIEW_AND_RATING:
			try {
				int code = data.getInt("responseCode");
				if (code == 1) {
					AlertDialog.Builder alert = new AlertDialog.Builder(
							new ContextThemeWrapper(this,
									android.R.style.Theme_Holo_Light));
					alert.setMessage("Thanks for your Review");
					alert.setTitle("Thanks!");
					alert.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									finish();
								}
							});
					alert.show();
					return;
				} else {
					CommonUtils.showDialog(this, data.getJSONObject("Message")
							.getString("error"));
				}
			} catch (Exception e) {
				e.printStackTrace();
				Toast.makeText(this, "Error Occurred", Toast.LENGTH_LONG).show();
			}
			break;
		}
	}
}
