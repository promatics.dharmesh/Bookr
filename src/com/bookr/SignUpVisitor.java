package com.bookr;

//import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Patterns;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.bookr.utils.CallService;
import com.bookr.utils.CommonUtils;
import com.bookr.utils.Constants;
import com.bookr.utils.ServiceCallBack;

@SuppressWarnings("deprecation")
public class SignUpVisitor extends Activity implements OnClickListener,
		ServiceCallBack {
	EditText visitor_name, visitor_username, visitor_email, visitor_phone,
			visitor_password, visitor_cnfpassword;
	TextView visitor_dob;
	Button joinbtn;
	int Year, Month, Day;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.visitersignup);
		visitor_name = (EditText) findViewById(R.id.txt_signup_name);
		// visitor_username = (EditText) findViewById(R.id.txt_signup_username);
		visitor_email = (EditText) findViewById(R.id.txt_signup_email);
		visitor_phone = (EditText) findViewById(R.id.txt_signup_phone);
		// visitor_dob = (TextView) findViewById(R.id.txt_signup_DOB);
		visitor_password = (EditText) findViewById(R.id.txt_signup_password);
		// visitor_cnfpassword = (EditText)
		// findViewById(R.id.txt_signup_confirm_password);
		// visitor_dob.setOnClickListener(this);
		joinbtn = (Button) findViewById(R.id.btn_signup_visitor);
		joinbtn.setOnClickListener(this);

	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.slide_down);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		// case R.id.txt_signup_DOB:
		// final Calendar c = Calendar.getInstance();
		// Year = c.get(Calendar.YEAR);
		// Month = c.get(Calendar.MONTH);
		// Day = c.get(Calendar.DAY_OF_MONTH);
		// DatePickerDialog dpd = new DatePickerDialog(this,
		// new OnDateSetListener() {
		// @SuppressLint("SimpleDateFormat")
		// @Override
		// public void onDateSet(DatePicker arg0, int Year,
		// int Month, int Day) {
		// visitor_dob.setText(Day + "-" + (Month + 1) + "-"
		// + Year);
		// String date = visitor_dob.getText().toString();
		// try {
		// SimpleDateFormat sdf = new SimpleDateFormat(
		// "dd-MM-yyyy");
		// Date dateObj = sdf.parse(date);
		// visitor_dob.setText(new SimpleDateFormat(
		// "dd-MM-yyyy").format(dateObj));
		// } catch (final Exception e) {
		// e.printStackTrace();
		// }
		// }
		// }, Year, Month, Day);
		// long t = System.currentTimeMillis();
		// dpd.getDatePicker().setMaxDate(t - 1000);
		// dpd.show();
		// break;

		case R.id.btn_signup_visitor:
			if (visitor_name.getText().toString().equals("")) {
				AlertDialog.Builder alert = new AlertDialog.Builder(
						new ContextThemeWrapper(this,
								android.R.style.Theme_Holo_Light));
				alert.setMessage("Enter your name");
				alert.setTitle("Alert");
				alert.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
							}
						});
				alert.show();
				return;
			}
			// if (visitor_username.getText().toString().length() < 4) {
			// AlertDialog.Builder alert = new AlertDialog.Builder(new
			// ContextThemeWrapper(this,android.R.style.
			// Theme_Holo_Light));
			// alert.setMessage("Enter your User name (Min 4)");
			// alert.setTitle("Alert");
			// alert.setPositiveButton("OK",
			// new DialogInterface.OnClickListener() {
			// @Override
			// public void onClick(DialogInterface dialog, int which) {
			// }
			// });
			// alert.show();
			// return;
			// }
			if (!visitor_email.getText().toString()
					.matches(Patterns.EMAIL_ADDRESS.pattern())) {
				AlertDialog.Builder alert = new AlertDialog.Builder(
						new ContextThemeWrapper(this,
								android.R.style.Theme_Holo_Light));
				alert.setMessage("Enter valid email address");
				alert.setTitle("Alert");
				alert.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
							}
						});
				alert.show();
				return;
			}

			if (!visitor_phone.getText().toString()
					.matches(Patterns.PHONE.pattern())
					&& !visitor_phone.getText().toString().equals("")) {
				AlertDialog.Builder alert = new AlertDialog.Builder(
						new ContextThemeWrapper(this,
								android.R.style.Theme_Holo_Light));
				alert.setMessage("Enter valid Phone number");
				alert.setTitle("Alert");
				alert.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
							}
						});
				alert.show();
				return;
			}
			// if (visitor_dob.getText().toString().equals("")) {
			// AlertDialog.Builder alert = new AlertDialog.Builder(new
			// ContextThemeWrapper(this,android.R.style.
			// Theme_Holo_Light));
			// alert.setMessage("Enter your Date of Birth");
			// alert.setTitle("Alert");
			// alert.setPositiveButton("OK",
			// new DialogInterface.OnClickListener() {
			// @Override
			// public void onClick(DialogInterface dialog, int which) {
			// }
			// });
			// alert.show();
			// return;
			// }
			if (visitor_password.getText().toString().equals("")) {
				AlertDialog.Builder alert = new AlertDialog.Builder(
						new ContextThemeWrapper(this,
								android.R.style.Theme_Holo_Light));
				alert.setMessage("Enter your Password");
				alert.setTitle("Alert");
				alert.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {

							}
						});
				alert.show();
				return;
			}
			// if (visitor_cnfpassword.getText().toString().equals("")
			// || !visitor_cnfpassword.getText().toString()
			// .equals(visitor_password.getText().toString())) {
			// AlertDialog.Builder alert =new AlertDialog.Builder(new
			// ContextThemeWrapper(this,android.R.style.
			// Theme_Holo_Light));
			// alert.setMessage("Confirm pasword should be same as password");
			// alert.setTitle("Alert");
			// alert.setPositiveButton("OK",
			// new DialogInterface.OnClickListener() {
			// @Override
			// public void onClick(DialogInterface dialog, int which) {
			//
			// }
			// });
			// alert.show();
			// return;
			// }
			try {
				InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
						0);
			} catch (Exception e) {
			}
			List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
			values.add(new BasicNameValuePair("name", visitor_name.getText()
					.toString()));
			// values.add(new BasicNameValuePair("username", visitor_username
			// .getText().toString()));
			values.add(new BasicNameValuePair("email", visitor_email.getText()
					.toString()));
			values.add(new BasicNameValuePair("phone_no", visitor_phone
					.getText().toString()));
			values.add(new BasicNameValuePair("password", visitor_password
					.getText().toString()));
			// values.add(new BasicNameValuePair("dob", visitor_dob.getText().
			// toString()));
			new CallService(this, this, Constants.REQ_VISI_REG, true, values)
					.execute(Constants.VISITOR_REGISTRATION);
			break;
		}
	}

	@Override
	public void onServiceResponse(int requestCode, JSONObject data) {
		switch (requestCode) {
		case Constants.REQ_VISI_REG:
			try {
				int code = data.getInt("responseCode");
				if (code == 1) {

					AlertDialog.Builder alert = new AlertDialog.Builder(
							new ContextThemeWrapper(this,
									android.R.style.Theme_Holo_Light));
					alert.setMessage("You are successfully registered with Bookr! Please login to continue.");
					alert.setTitle("Sign Up!");
					alert.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									finish();
								}
							});
					alert.show();

				} else {
					CommonUtils.showDialog(this, data.getJSONObject("Message")
							.getString("error"));
				}
			} catch (Exception e) {
				e.printStackTrace();
				CommonUtils.showDialog(this, "Error Occurred:" + e.getMessage());
			}
			break;
		}

	}

}
