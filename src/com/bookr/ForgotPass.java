package com.bookr;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Patterns;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.bookr.R;
import com.bookr.utils.CallService;
import com.bookr.utils.CommonUtils;
import com.bookr.utils.Constants;
import com.bookr.utils.ServiceCallBack;

@SuppressWarnings("deprecation")
public class ForgotPass extends Activity implements OnClickListener,
		ServiceCallBack {
	Button submit;
	EditText et_email;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.visitor_frogot_pass);
		et_email = (EditText) findViewById(R.id.txt_forgot_pass);
		submit = (Button) findViewById(R.id.forgot_pass_btn);
		submit.setOnClickListener(this);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.slide_down);
	}

	@Override
	public void onClick(View v) {
		if (!et_email.getText().toString()
				.matches(Patterns.EMAIL_ADDRESS.pattern())) {
			AlertDialog.Builder alert = new AlertDialog.Builder(
					new ContextThemeWrapper(this,
							android.R.style.Theme_Holo_Light));
			alert.setMessage("Enter valid email address");
			alert.setTitle("Alert");
			alert.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
						}
					});
			alert.show();
			return;
		}
		try {
			InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
		} catch (Exception e) {
		}
		List<BasicNameValuePair> values = new ArrayList<BasicNameValuePair>();
		values.add(new BasicNameValuePair("email", et_email.getText()
				.toString()));
		new CallService(this, this, Constants.REQ_VISI_FORGOT_PASS, true,
				values).execute(Constants.VISI_FORGOT_PASS);
	}

	@Override
	public void onServiceResponse(int requestCode, JSONObject data) {
		switch (requestCode) {
		case Constants.REQ_VISI_FORGOT_PASS:
			try {
				int code = data.getInt("responseCode");
				if (code == 1) {
					AlertDialog.Builder alert = new AlertDialog.Builder(
							new ContextThemeWrapper(this,
									android.R.style.Theme_Holo_Light));
					alert.setMessage("Your Password has been sent to your email. Please check your email to continue.");
					alert.setTitle("Forgot Passowrd");
					alert.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									finish();
								}
							});
					alert.show();
				} else {
					CommonUtils.showDialog(this, data.getJSONObject("Message")
							.getString("error"));
				}
			} catch (Exception e) {
				e.printStackTrace();
				CommonUtils.showDialog(this, "Error Occurred" + e.getMessage());
			}
			break;
		}
	}
}
