package com.bookr;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class LoginView extends Activity implements OnClickListener {
	Button visitor, owner;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_view);
		visitor = (Button) findViewById(R.id.btnVisitor);
		owner = (Button) findViewById(R.id.btnworker);
		visitor.setOnClickListener(this);
		owner.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnVisitor:
			Intent i = new Intent(LoginView.this, VisitorLogin.class);
			startActivity(i);
			overridePendingTransition(R.anim.slide_up, -1);
			break;
		case R.id.btnworker:
			i = new Intent(LoginView.this, SalonEmpLogin.class);
			startActivity(i);
			overridePendingTransition(R.anim.slide_up, 0);
			break;
		}
	}

}
